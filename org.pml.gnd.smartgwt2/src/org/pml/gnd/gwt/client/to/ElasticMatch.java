package org.pml.gnd.gwt.client.to;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Dataset TO object
 * 
 * { "_index" : "gnd", "_type" : "datasets", "_id" :
 * "72b6efa2c581e7615bb765208312fd17_1", "_score" : 2.1410832, "_source" :
 * {"sensor"
 * :"Garmin-G400","platform":"plat_a","sensor_type":"speed","data_type":
 * ["time","lon","lat"],"platform_type":"van","time_bounds":{"start":
 * "2012-03-15T03:46:37+0000"
 * ,"end":"2012-03-15T16:35:58+0000"},"name":"Track 30"
 * ,"type":"track","geo_bounds"
 * :{"tl":[-7.784170959924891,57.944529113991734],"br"
 * :[-5.019334420312834,49.109100753386016]},"trial":"trial_d"} }
 * 
 * @author Yuri
 * 
 */
public class ElasticMatch extends JavaScriptObject
{

	protected ElasticMatch()
	{
	}

	public final native String getId() /*-{
		// special handling. it may contain underscore
		var res = this._id.split("_")[0];
		return res;
	}-*/;
	
	public final native String getPlatformType() /*-{
	return this.fields["metadata.platform_type"];
}-*/;


	public final native String getPlatform() /*-{
	return this.fields["metadata.platform"];
}-*/;


	public final native String getTrial() /*-{
	return this.fields["metadata.trial"];
}-*/;


	public final native String getType() /*-{
	return this.fields["metadata.type"];
}-*/;
	
	public final native String getSensor() /*-{
	return this.fields["metadata.sensor"];
}-*/;

	public final native String getSensorType() /*-{
	return this.fields["metadata.sensor_type"];
}-*/;

	public final native String getName() /*-{
	return this.fields["metadata.name"];
}-*/;

	public static ElasticMatch randomise()
	{
		ElasticMatch res = create();
		res.newPerm();
		return res;
	}

	private static final native ElasticMatch create() /*-{
		return new Object();
	}-*/;

	private final native void newPerm() /*-{
		this.id = "aabbss" + Math.ceil(Math.random() * 110);
	}-*/;

}
