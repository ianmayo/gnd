package org.pml.gnd.gwt.client.model;

import org.pml.gnd.gwt.client.to.ElasticResults;

import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.jsonp.client.JsonpRequestBuilder;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class BrowseModelImpl implements BrowseModel
{

	
	
	@Override
	public void getMatches(String query,
			final AsyncCallback<ElasticResults> callback)
	{
		// trim off the wrappers
		query = query.substring(1, query.length() - 1);
		
		JSONValue queryObj = JSONParser.parseStrict(query);
		fireSearch(queryObj, callback);
	}

	@Override
	public void getAll(AsyncCallback<ElasticResults> callback)
	{
		JSONObject queryObj = new JSONObject();
		queryObj.put("match_all", new JSONObject());

		fireSearch(queryObj, callback);
	}

	private void fireSearch(JSONValue queryObj,
			final AsyncCallback<ElasticResults> callback)
	{

		// sort out the facets
		JSONObject facets = new JSONObject();
		addFacetFor(facets, "platform");
		addFacetFor(facets, "platform_type");
		addFacetFor(facets, "trial");
		addFacetFor(facets, "sensor");
		addFacetFor(facets, "sensor_type");

		JSONObject qq = new JSONObject();
		qq.put("size", new JSONNumber(0));
		qq.put("query", queryObj);
		qq.put("facets", facets);

		String url = "http://0d9fd05438a44abe882139135eb01048.found.no:9200/data/dataset/_search?pretty=true&source="
				+ qq.toString();
		// Send request to server and catch any errors.
		System.out.println("url" + url);
		JsonpRequestBuilder builder = new JsonpRequestBuilder();
		builder.requestObject(url, callback);
	}

	/**
	 * create a term facet for the specified term
	 * 
	 * @param parent
	 * @param term
	 */
	private void addFacetFor(JSONObject parent, String term)
	{
		JSONObject platform = new JSONObject();
		JSONObject platTerm = new JSONObject();
		platTerm.put("field", new JSONString(term));
		platTerm.put("size", new JSONNumber(1000));
		platform.put("terms", platTerm);
		parent.put(term, platform);
	}

}
