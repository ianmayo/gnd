package org.pml.gnd.gwt.client.model;


import org.pml.gnd.gwt.client.to.WelcomeResults;

import com.google.gwt.jsonp.client.JsonpRequestBuilder;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class WelcomeModelImpl implements WelcomeModel
{

	@Override
	public void getRecentDatasets(AsyncCallback<WelcomeResults> callback)
	{
		String url = "http://gnd.iriscouch.com/tracks/_design/tracks/_view/track_listing?limit=10";
		// Send request to server and catch any errors.
		JsonpRequestBuilder builder = new JsonpRequestBuilder();
		builder.requestObject(url, callback);
	}
	
}
