package org.pml.gnd.gwt.client.model;

import org.pml.gnd.gwt.client.to.ElasticResults;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface BrowseModel
{
	public void getMatches(String query, AsyncCallback<ElasticResults> callback);

	public void getAll(AsyncCallback<ElasticResults> callback);
}
