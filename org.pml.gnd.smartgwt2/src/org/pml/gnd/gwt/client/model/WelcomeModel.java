package org.pml.gnd.gwt.client.model;


import org.pml.gnd.gwt.client.to.WelcomeResults;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface WelcomeModel
{
	/** get a list of recent datasets
	 * 
	 * @param callback
	 */
	public void getRecentDatasets(AsyncCallback<WelcomeResults> callback);

	/** find out how many datasets there are
	 * 
	 * @param callback
	 */	
}
