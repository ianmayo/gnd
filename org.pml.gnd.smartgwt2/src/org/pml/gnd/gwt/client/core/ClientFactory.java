package org.pml.gnd.gwt.client.core;


import org.pml.gnd.gwt.client.model.BrowseModel;
import org.pml.gnd.gwt.client.model.WelcomeModel;
import org.pml.gnd.gwt.client.view.BrowseView;
import org.pml.gnd.gwt.client.view.WelcomeView;

import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

public interface ClientFactory
{
	EventBus getEventBus();
	PlaceController getPlaceController();
	WelcomeView getWelcomeView();	
	WelcomeModel getWelcomeModel();
	BrowseView getBrowseView();
	BrowseModel getBrowseModel();
}
