package org.pml.gnd.gwt.client.core;


import org.pml.gnd.gwt.client.place.BrowsePlace;
import org.pml.gnd.gwt.client.place.WelcomePlace;
import org.pml.gnd.gwt.client.activity.BrowseActivity;
import org.pml.gnd.gwt.client.activity.WelcomeActivity;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

public class AppActivityMapper implements ActivityMapper {

	private ClientFactory clientFactory;

	/**
	 * AppActivityMapper associates each Place with its corresponding
	 * {@link Activity}
	 * 
	 * @param clientFactory
	 *            Factory to be passed to activities
	 */
	public AppActivityMapper(ClientFactory clientFactory) {
		super();
		this.clientFactory = clientFactory;
	}

	/**
	 * Map each Place to its corresponding Activity. 
	 */
	@Override
	public Activity getActivity(Place place) {
		
		if (place instanceof WelcomePlace)
			return new WelcomeActivity((WelcomePlace) place, clientFactory);		
		if (place instanceof BrowsePlace)
			return new BrowseActivity((BrowsePlace) place, clientFactory);
		return null;
	}

}
