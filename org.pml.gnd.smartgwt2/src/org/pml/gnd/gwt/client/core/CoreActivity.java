package org.pml.gnd.gwt.client.core;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.place.shared.Place;

public abstract class CoreActivity extends AbstractActivity
{
	
	// Used to obtain views, eventBus, placeController
	// Alternatively, could be injected via GIN
	private ClientFactory clientFactory;

	protected CoreActivity(ClientFactory clientFactory)
	{
		this.clientFactory = clientFactory;
	}
	
	protected ClientFactory getFactory()
	{
		return clientFactory;
	}

	/**
	 * Navigate to a new Place in the browser
	 */
	protected void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}

}
