package org.pml.gnd.gwt.client.core;


import org.pml.gnd.gwt.client.model.BrowseModel;
import org.pml.gnd.gwt.client.model.BrowseModelImpl;
import org.pml.gnd.gwt.client.model.WelcomeModelImpl;
import org.pml.gnd.gwt.client.model.WelcomeModel;
import org.pml.gnd.gwt.client.view.BrowseView;
import org.pml.gnd.gwt.client.view.BrowseViewImpl;
import org.pml.gnd.gwt.client.view.WelcomeView;
import org.pml.gnd.gwt.client.view.WelcomeViewImpl;

import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

public class ClientFactoryImpl implements ClientFactory
{
	

	private static final EventBus eventBus = new SimpleEventBus();
	private static final PlaceController placeController = new PlaceController(
			eventBus);
	private static final WelcomeView welcomeView = new WelcomeViewImpl();
	private static final BrowseView browseView = new BrowseViewImpl();
	private WelcomeModel welcomeModel;
	private BrowseModel browseModel;

	public ClientFactoryImpl()
	{
		
	}

	@Override
	public EventBus getEventBus()
	{
		return eventBus;
	}

	@Override
	public WelcomeView getWelcomeView()
	{
		return welcomeView;
	}

	@Override
	public PlaceController getPlaceController()
	{
		return placeController;
	}

	
	@Override
	public WelcomeModel getWelcomeModel()
	{
		if (welcomeModel == null)
			welcomeModel = new WelcomeModelImpl();
			
		return welcomeModel;
	}
	
	@Override
	public BrowseView getBrowseView(){		
			
		return browseView;
	}
	
	@Override
	public BrowseModel getBrowseModel()
	{
		if (browseModel == null)
			browseModel = new BrowseModelImpl();
			
		return browseModel;
	}
}
