package org.pml.gnd.gwt.client.activity;


import java.util.LinkedHashMap;


import org.pml.gnd.gwt.client.core.ClientFactory;
import org.pml.gnd.gwt.client.core.CoreActivity;
import org.pml.gnd.gwt.client.model.BrowseModel;
import org.pml.gnd.gwt.client.place.BrowsePlace;
import org.pml.gnd.gwt.client.place.WelcomePlace;
import org.pml.gnd.gwt.client.to.ElasticFacet;
import org.pml.gnd.gwt.client.to.ElasticResults;
import org.pml.gnd.gwt.client.view.BrowseView;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

public class BrowseActivity extends CoreActivity implements
	BrowseView.Presenter
{
	//private BrowsePlace browsePlace;

	// Name that will be appended to "Hello,"

	public BrowseActivity(BrowsePlace place, ClientFactory clientFactory)
	{
		super(clientFactory);
		//browsePlace = place;
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus)
	{
		final BrowseView browseView = getFactory().getBrowseView();
		browseView.setPresenter(this);
		containerWidget.setWidget(browseView.asWidget());
		BrowseModel browseModel = getFactory().getBrowseModel();
		AsyncCallback<ElasticResults> handler = new FacetsHandler(browseView);
		browseModel.getAll(handler);
	}
	
	private static class FacetsHandler implements AsyncCallback<ElasticResults>	{
		
		private BrowseView browse;

		private FacetsHandler(BrowseView browse )
		{
			this.browse = browse;
		}

		@Override
		public void onSuccess(ElasticResults result)
		{			
			// we also wish to put the facets in the search window
			//System.out.println(result.getNumHits());
			System.out.println("-----" + result.getNumFacets() ); 
			System.out.println("-----" + result.getFacet("platform_type")); 
			
			if(result.getNumFacets() > 0)
			{
				populateList(result, browse, "platform");
				populateList(result, browse, "platform_type");
				populateList(result, browse, "trial");
				populateList(result, browse, "sensor");
				populateList(result, browse, "sensor_type");
			}
		}

		@Override
		public void onFailure(Throwable caught)
		{
			caught.printStackTrace();
		}
		
		/** produce a list of the termsin the named facet, put it into the view
		 * 
		 * @param result the ES results object we're going to process
		 * @param view the UI where we're going to put the terms
		 * @param facet_name the facet in question
		 */
		private final void populateList(ElasticResults result, BrowseView view, final String facet_name)
		{
			LinkedHashMap<String, String> theList = getTermsFor(result, facet_name);
			view.populateList(theList, facet_name);
		}
		
		/** produce a hashmap of the terms in the named facet
		 * 
		 * @param result the ElasticSearch results object
		 * @param facet_name the facet we're looking at
		 * @return
		 */
		private final LinkedHashMap<String, String> getTermsFor(final ElasticResults result, final String facet_name)
		{
			LinkedHashMap<String, String> destination = new LinkedHashMap<String, String>();

			ElasticFacet thisFacet = result.getFacet(facet_name);
			if(thisFacet != null)
			{
				final int numTerms = thisFacet.getNumTerms();
				for(int i=0;i<numTerms;i++)
				{
					destination.put(thisFacet.getTermName(i), thisFacet.getTermName(i));					
				}
			}
			return destination;
		}
		
	};

	
	@Override
	public void operateBasket()
	{
		//super.goTo(new OperatePlace());
	}

	@Override
	public void reset()
	{
		this.goTo(new BrowsePlace());
	}

	@Override
	public void welcome() {
		// TODO Auto-generated method stub
		 super.goTo(new WelcomePlace());
	}

	@Override
	public void browse() {
		// TODO Auto-generated method stub
		this.goTo(new BrowsePlace());
	}

}
