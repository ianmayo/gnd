package org.pml.gnd.gwt.client.activity;

import org.pml.gnd.gwt.client.core.ClientFactory;
import org.pml.gnd.gwt.client.core.CoreActivity;
import org.pml.gnd.gwt.client.view.WelcomeView;
import org.pml.gnd.gwt.client.model.WelcomeModel;
import org.pml.gnd.gwt.client.place.BrowsePlace;
import org.pml.gnd.gwt.client.place.WelcomePlace;
import org.pml.gnd.gwt.client.to.WelcomeResults;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

public class WelcomeActivity extends CoreActivity implements
		WelcomeView.Presenter
{

	public WelcomeActivity(WelcomePlace place, ClientFactory clientFactory)
	{
		super(clientFactory);
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus)
	{
		final WelcomeView welcomeView = getFactory().getWelcomeView();
		welcomeView.setPresenter(this);
		containerWidget.setWidget(welcomeView.asWidget());

		// initialise the listing
		welcomeView.setRecentItems(null);
		
		WelcomeModel model = getFactory().getWelcomeModel();
		model.getRecentDatasets(new AsyncCallback<WelcomeResults>()
		{

			@Override
			public void onSuccess(WelcomeResults result)
			{
				welcomeView.setRecentItems(result);
			}

			@Override
			public void onFailure(Throwable caught)
			{
			}
		});
		
		// also have a go at specifying the record count
		/*	model.getDatasetCount(new AsyncCallback<ElasticCount>()
		{

			@Override
			public void onSuccess(ElasticCount result)
			{
				//helloView.setCount(result);
			}

			@Override
			public void onFailure(Throwable caught)
			{
			}
		});*/
	}

	@Override
	public void browse()
	{
		System.out.println("in the browse place");
		super.goTo(new BrowsePlace());
	}

	@Override
	public void view(String datasetId)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void add()
	{
		//super.goTo(new AddPlace());
	}

	@Override
	public void maintain()
	{
		//super.goTo(new MaintainPlace());
	}
}
