package org.pml.gnd.gwt.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class BrowsePlace extends Place {

	public BrowsePlace() {

	}

	public static class Tokenizer implements PlaceTokenizer<BrowsePlace> {
		final static String NAME = "BROWSE";

		@Override
		public String getToken(BrowsePlace place) {
			return NAME;
		}

		@Override
		public BrowsePlace getPlace(String token) {

			return new BrowsePlace();
		}

	}
}
