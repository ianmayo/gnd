/**
 * 
 */
package org.pml.gnd.gwt.client.widgets;

import com.smartgwt.client.widgets.Button;

/**
 * @author Kashif
 *
 */
public class Link extends Button {
	
	private final String DEFAULT_CSS = "link";

	public Link(){
		setShowFocused(false);
		setShowFocusedAsOver(false);
		setAutoFit(true);
		setBaseStyle(DEFAULT_CSS);
	}
	
	public Link(String title){
		this();
		setTitle(title);
	}
	
	@Override
	public void setBaseStyle(String baseStyle) {
		super.setBaseStyle(baseStyle);
	}
}
