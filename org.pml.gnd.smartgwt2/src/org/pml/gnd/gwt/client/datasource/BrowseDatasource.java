/**
 * 
 */
package org.pml.gnd.gwt.client.datasource;

import org.pml.gnd.gwt.client.view.FilterOption;

import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.XJSONDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;

/**
 * @author MUNAWAR
 *
 */
public class BrowseDatasource extends XJSONDataSource {  
	  
    private static BrowseDatasource instance = null;  
      
    public static BrowseDatasource getInstance(String id, FilterOption filterOption) {  
      
          instance = new BrowseDatasource(id, filterOption);  
      
        return instance;  
    }  

    public BrowseDatasource(String id, FilterOption filterOption) {  
    	setID(id);
    	setClientOnly(true);
    	DataSourceField ref = new DataSourceTextField("trial");
        DataSourceField name = new DataSourceTextField("sensor_type");
        DataSourceField source = new DataSourceTextField("sensor");
        DataSourceField source1 = new DataSourceTextField("platform_type");
        DataSourceField source2 = new DataSourceTextField("name");
        DataSourceField source3 = new DataSourceTextField("platform");
        DataSourceField source4 = new DataSourceTextField("type");
        setDataFormat(DSDataFormat.JSON);
        setRecordXPath("hits/hits/_source/metadata");
        String filterString = "";
        if(filterOption.getPlatform() != null && !filterOption.getPlatform().equals(""))
        	filterString = "q=platform:"+filterOption;
        //else if(filterOption.getPlatform() != null && !filterOption.getPlatform().equals(""))
        //	filterString = "platform:"+filterOption;       
        
        
      //  setDataURL(/*"ds/test.json"*/"http://0d9fd05438a44abe882139135eb01048.found.no:9200/gnd2/datasets/_search?"+filterString +"&pretty=true");
       // setRecordXPath("_score/metadata");
        setFields(ref, name, source, source1, source2, source3, source4);
    }
}