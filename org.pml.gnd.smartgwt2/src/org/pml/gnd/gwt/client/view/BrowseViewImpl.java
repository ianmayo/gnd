package org.pml.gnd.gwt.client.view;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.pml.gnd.gwt.client.datasource.BrowseDatasource;
import org.pml.gnd.gwt.client.widgets.Link;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class BrowseViewImpl extends SimplePanel implements BrowseView
{

	private Presenter presenter;
	private FilterOption filter = new FilterOption();
	private PickerIcon searchIcon = new PickerIcon(PickerIcon.SEARCH);
	private VLayout vLayout;
	FilterOption filterOption; 
	Button btnNewButton = new Button("Search");
	
	SelectItem  platform;
	SelectItem  platformType;
	SelectItem  sensor;
	SelectItem  sensorType;
	SelectItem  trial;
	BrowseListGrid datasetsTable;
	public BrowseViewImpl()
	{
		vLayout = new VLayout();
		vLayout.setWidth100();
		vLayout.setHeight100();
		HLayout mainLayout = new HLayout();
		filterOption = new FilterOption();
		 
		Label liveFilterLbl = new Label("Live filter");
		liveFilterLbl.setHeight(40);
		liveFilterLbl.setStyleName("live-filter-label");
		
		VLayout leftItemsLayout = new VLayout();
		leftItemsLayout.setWidth(280);
		leftItemsLayout.addMember(liveFilterLbl);
		
		VLayout liveFilterLayout = new VLayout();
		liveFilterLayout.setWidth(280);
		liveFilterLayout.setHeight(300);
		liveFilterLayout.setLayoutTopMargin(10);
		liveFilterLayout.setStyleName("liveFilterBox");
		
		TextItem searchBox = new TextItem("searchBox", "Search");  
		searchBox.setIcons(searchIcon); 
		searchBox.setWidth(195);
		
		platform = new SelectItem();  
        platform.setTitle("Platform");
        platform.setWidth(195);
        
        platform.addChangedHandler(new ChangedHandler() {
			
			@Override
			public void onChanged(ChangedEvent event) {
				// TODO Auto-generated method stub
				
				if (filter != null)
					filter.getPlatform();
				if (presenter != null)
					presenter.browse();	
			}
		});
        //platform.setValueMap(valueMap);
        platformType = new SelectItem();  
        platformType.setTitle("Platform type");
        platformType.setWidth(195);
        
        sensor = new SelectItem();  
        sensor.setTitle("Sensor");
        sensor.setWidth(195);
        
        sensorType = new SelectItem();  
        sensorType.setTitle("Sensor type");
        sensorType.setWidth(195);

        trial = new SelectItem();  
        trial.setTitle("Trial");
        trial.setWidth(195);

        DateItem after = new DateItem();
        after.setTitle("After");
        after.setUseTextField(true); 
        after.setWidth(188);
        
        DateItem before = new DateItem();
        before.setTitle("Before");
        before.setUseTextField(true);
        before.setWidth(188);
        
        
        HLayout searchBtn = getIntroLayout(btnNewButton);
         
        searchBtn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				filterOption.setPlatform(platform.getDisplayValue());
				datasetsTable.loadData(filterOption);
			}
		});
        
        
        DynamicForm leftItemsForm = new DynamicForm();
        leftItemsForm.setWidth(300);
        leftItemsForm.setCellPadding(5);
        
        leftItemsForm.setFields(searchBox, platform, platformType, sensor, sensorType, trial, after, before);
       
		liveFilterLayout.addMember(leftItemsForm);
		liveFilterLayout.addMember(searchBtn);
		leftItemsLayout.addMember(liveFilterLayout);
		
		VLayout datasetsLayout = new VLayout();
		datasetsLayout.setWidth100();
		datasetsLayout.setHeight(620);
		datasetsLayout.setLayoutTopMargin(10);
		datasetsLayout.setLayoutLeftMargin(20);
		
		Label datasetsLbl = new Label("Matching datasets");
		datasetsLbl.setHeight(40);
		datasetsLbl.setAlign(Alignment.LEFT);
		datasetsLbl.setStyleName("datasets-label");
		
		datasetsTable = new BrowseListGrid();
		
		datasetsLayout.addMember(datasetsLbl);
		datasetsLayout.addMember(datasetsTable);
		 
		mainLayout.addMember(leftItemsLayout);
		mainLayout.addMember(datasetsLayout);
		
		vLayout.addMember(mainLayout);

	 	 
	}
	
	
	private HLayout getIntroLayout(Button link){
		HLayout layout = new HLayout();
		layout.setWidth(322);
		layout.setHeight(30);
		layout.setAlign(Alignment.RIGHT);
		layout.addMember(link);
		
		return layout;
	}		
	
	@Override
	public void populateList(LinkedHashMap<String, String> theItems, String item)
	{		
		if(item.equals("platform"))
			platform.setValueMap(theItems);
		if(item.equals("platform_type"))
			platformType.setValueMap(theItems);
		if(item.equals("trial"))
			trial.setValueMap(theItems);
		if(item.equals("sensor"))
			sensor.setValueMap(theItems);
		if(item.equals("sensor_type"))
			sensorType.setValueMap(theItems);
	}
	
	

	@Override
	public void setPresenter(Presenter listener)
	{
		presenter = listener;		
	}
	
	@Override
	public Widget  asWidget(){
		return vLayout;
	}
	
	
	

}
