package org.pml.gnd.gwt.client.view;

import java.util.LinkedHashMap;

import com.google.gwt.user.client.ui.IsWidget;

/**
 *
 * @author kashif
 */
public interface BrowseView extends IsWidget
{
	void setPresenter(Presenter listener);	
	void populateList(LinkedHashMap<String, String> theItems, String item);

	public interface Presenter
	{
		
		void operateBasket();
		void reset();
		void welcome();
		void browse();
	}


}
 