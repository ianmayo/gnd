/**
 * 
 */
package org.pml.gnd.gwt.client.view;

import java.util.LinkedHashMap;


import org.pml.gnd.gwt.client.datasource.BrowseDatasource;
import org.pml.gnd.gwt.client.to.ElasticResults;
import org.pml.gnd.gwt.client.view.FilterOption;

import com.google.gwt.jsonp.client.JsonpRequestBuilder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.XJSONDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 * @author Kashif
 *
 */
public class BrowseListGrid extends ListGrid {  
	  
     

    public BrowseListGrid() {  
    	ListGridField ref = new ListGridField("trial");
    	ListGridField name = new ListGridField("sensor_type");
    	ListGridField source = new ListGridField("sensor");
    	ListGridField source1 = new ListGridField("platform_type");
    	ListGridField source2 = new ListGridField("name");
    	ListGridField source3 = new ListGridField("platform");
    	ListGridField source4 = new ListGridField("type");
    	setWidth100();
		setStyleName("datasets-table");
		setShowAllRecords(true);  
		setAutoFetchData(false); 
		setData(new ListGridRecord[0]);
        setFields(ref, name, source, source1, source2, source3, source4);
    }
    
    public void loadData( FilterOption filterOption) {
    	String filterString = "";
        if(filterOption.getPlatform() != null && !filterOption.getPlatform().equals(""))
          	filterString = "q=platform:"+filterOption.platform+"&";
        setData(new ListGridRecord[0]);
    	AsyncCallback<ElasticResults> handler = new SearchHandler(this);
    	String url = "http://0d9fd05438a44abe882139135eb01048.found.no:9200/data/dataset/_search?"+filterString +"fields=name,platform,platform_type,sensor,sensor_type,trial&pretty=true";
				
		// Send request to server and catch any errors.
		System.out.println("url" + url);
		JsonpRequestBuilder builder = new JsonpRequestBuilder();
		builder.requestObject(url, handler);
    	
    	}
    private static class SearchHandler implements AsyncCallback<ElasticResults>	{
    	private ListGrid listGrid;
    	private SearchHandler(ListGrid grid)
		{
			listGrid = grid;
		}

		@Override
		public void onSuccess(ElasticResults result)
		{			
			// we also wish to put the facets in the search window
			//System.out.println(result.getNumHits());
			ListGridRecord[] recordList = new ListGridRecord[result.getNumHits()];
			int count = 0;
			for(int i=0;i<result.getNumHits();i++){
				ListGridRecord record = new ListGridRecord();
				if(result.getHit(i).getPlatform() != null)
					record.setAttribute("platform", result.getHit(i).getPlatform());
				if(result.getHit(i).getPlatformType() != null)
					record.setAttribute("platform_type", result.getHit(i).getPlatformType());
				if(result.getHit(i).getTrial() != null)
					record.setAttribute("trial", result.getHit(i).getTrial());
				if(result.getHit(i).getSensor() != null)
					record.setAttribute("sensor", result.getHit(i).getSensor());
				if(result.getHit(i).getSensorType() != null)
					record.setAttribute("sensor_type", result.getHit(i).getSensorType());	
				if(result.getHit(i).getName() != null)
					record.setAttribute("name", result.getHit(i).getName());	
				if(result.getHit(i).getType() != null)
					record.setAttribute("sensor_type", result.getHit(i).getType());	
				recordList[count] = record;
		    	count++;
			}
			listGrid.setData(recordList);
			
		}

		@Override
		public void onFailure(Throwable caught)
		{
			caught.printStackTrace();
		}
	};
}