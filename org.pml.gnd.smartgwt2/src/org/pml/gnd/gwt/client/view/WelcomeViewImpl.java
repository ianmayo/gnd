package org.pml.gnd.gwt.client.view;
import org.pml.gnd.gwt.client.to.WelcomeItem;
import org.pml.gnd.gwt.client.to.WelcomeResults;
import org.pml.gnd.gwt.client.widgets.Link;



import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class WelcomeViewImpl  extends SimplePanel  implements WelcomeView
{

	private Presenter presenter;	
	private Link browseLink = new Link("Browse");
	private Link addDataLink = new Link("Add data");
	private Link maintainLink = new Link("Maintain");	
	private VLayout mainLayout;
	private VLayout rightLayout;
	public WelcomeViewImpl(){
		load();
	}
	
	private void load(){
		mainLayout = new VLayout();
		mainLayout.setWidth100();
		mainLayout.setHeight100();
		HLayout headingContainer = new HLayout();
		headingContainer.setWidth100();
		headingContainer.setHeight(50);
		headingContainer.setLayoutTopMargin(50);
		
		Label welcomeText = new Label("Welcome to the Geospatial Network Database");
		welcomeText.setWidth100();
		welcomeText.setStyleName("welcome");
		headingContainer.addMember(welcomeText);
		
		VLayout leftLayout = new VLayout();
		leftLayout.setWidth(400);
		leftLayout.setHeight(100);
		leftLayout.setMembersMargin(5);
		leftLayout.setStyleName("welcomeLeftPanel box");
		
		HLayout whatForLayout = getIntroLayout("What it's for", browseLink);
		HLayout whoForLayout = getIntroLayout("Who it's for", addDataLink);
		HLayout supportLayout = getIntroLayout("How to get support", maintainLink);
		
		leftLayout.addMember(whatForLayout);
		leftLayout.addMember(whoForLayout);
		leftLayout.addMember(supportLayout);
		
		Label dummy = new Label();
		dummy.setHeight(40);
		leftLayout.addMember(dummy);
		leftLayout.addMember(new Link("How to find out more"));
		
		rightLayout = new VLayout();
		rightLayout.setWidth(400);
		rightLayout.setHeight(100);
		rightLayout.setMembersMargin(5);
		rightLayout.setStyleName("welcomeRightPanel box");
		Label whatsNewLabel = new Label("What's new");
		whatsNewLabel.setStyleName("text16Bold");
		whatsNewLabel.setAlign(Alignment.CENTER);
		whatsNewLabel.setHeight(20);
		
		rightLayout.addMember(whatsNewLabel);
		
		
		HLayout welcomeContainer = new HLayout();
		welcomeContainer.setWidth100();
		welcomeContainer.setMembersMargin(50);
		welcomeContainer.setLayoutTopMargin(80);
		welcomeContainer.setAlign(Alignment.CENTER);
		
		welcomeContainer.addMember(leftLayout);
		welcomeContainer.addMember(rightLayout);
		
		mainLayout.addMember(headingContainer);
		mainLayout.addMember(welcomeContainer);
		
		browseLink.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (presenter != null)
					presenter.browse();			
			}
		});
		addDataLink.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (presenter != null)
					presenter.add();			
			}
		});
		maintainLink.addClickHandler(new ClickHandler() {
	
			@Override
		public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (presenter != null)
					presenter.maintain();			
		}
		});
	}
	
	private HLayout getIntroLayout(String labelText, Link link){
		HLayout layout = new HLayout();
		layout.setHeight(30);
		layout.setMembersMargin(40);
		Label whatForLabel = new Label(labelText);
		whatForLabel.setStyleName("text14");
		whatForLabel.setHeight(20);
		whatForLabel.setWidth(250);
		
		layout.addMember(whatForLabel);
		layout.addMember(link);
		
		return layout;
	}		
	
	@Override
	public Widget  asWidget(){
		return mainLayout;
	}
	@Override
	public void setPresenter(Presenter presenter)
	{
		this.presenter = presenter;
	}

	@Override
	public void setRecentItems(WelcomeResults items)
	{
		if (items != null)
		{	
			
			for (int i = 0; i < items.getNumItems(); i++)
			{
				WelcomeItem item = items.getHit(i);
				
				rightLayout.addMember(createLink(item.getValue()));
			}
		}else{
			int index =1;
			while(rightLayout.getMembers().length>1){
				rightLayout.removeChild(rightLayout.getMember(index));
				
			}
		}

	}
	/** Method to move the browse page **/
	private Link createLink(String name) {
		
		Link link = new Link(name);
		link.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (presenter != null)
					presenter.browse();			
			}
		});
		return link;
	}
	
}
