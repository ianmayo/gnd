package org.pml.gnd.smartgwt.client.mvp;

import org.pml.gnd.smartgwt.client.place.ViewWelcomeScreenPlace;

import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;

/*
 * PlaceHistoryMapper interface is used to attach all places which the
 * PlaceHistoryHandler should be aware of. This is done via the @WithTokenizers
 * annotation or by extending {@link PlaceHistoryMapperWithFactory} and creating
 * a separate TokenizerFactory.
 */
@WithTokenizers({ ViewWelcomeScreenPlace.Tokenizer.class })
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper {
}
