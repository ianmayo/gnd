package org.pml.gnd.smartgwt.client.mvp;

import org.pml.gnd.smartgwt.client.ClientFactory;
import org.pml.gnd.smartgwt.client.activity.ViewAddActivity;
import org.pml.gnd.smartgwt.client.activity.ViewBrowseActivity;
import org.pml.gnd.smartgwt.client.activity.ViewExportActivity;
import org.pml.gnd.smartgwt.client.activity.ViewMaintainActivity;
import org.pml.gnd.smartgwt.client.activity.ViewOperateActivity;
import org.pml.gnd.smartgwt.client.activity.ViewWelcomeScreenActivity;
import org.pml.gnd.smartgwt.client.place.ViewAddPlace;
import org.pml.gnd.smartgwt.client.place.ViewBrowsePlace;
import org.pml.gnd.smartgwt.client.place.ViewExportPlace;
import org.pml.gnd.smartgwt.client.place.ViewMaintainPlace;
import org.pml.gnd.smartgwt.client.place.ViewOperatePlace;
import org.pml.gnd.smartgwt.client.place.ViewWelcomeScreenPlace;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

/**
 * ActivityMapper associates each {@link Place} with its corresponding
 * {@link Activity}.
 */
public class AppActivityMapper implements ActivityMapper {

	/**
	 * Provided for {@link Activitie}s.
	 */
	private ClientFactory clientFactory;

	public AppActivityMapper(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public Activity getActivity(Place place) {

		if (place instanceof ViewWelcomeScreenPlace) {
			return new ViewWelcomeScreenActivity(
					(ViewWelcomeScreenPlace) place, clientFactory);

		} else if (place instanceof ViewBrowsePlace) {
			return new ViewBrowseActivity((ViewBrowsePlace) place,
					clientFactory);

		} else if (place instanceof ViewAddPlace) {
			return new ViewAddActivity((ViewAddPlace) place, clientFactory);

		} else if (place instanceof ViewMaintainPlace) {
			return new ViewMaintainActivity((ViewMaintainPlace) place,
					clientFactory);

		} else if (place instanceof ViewExportPlace) {
			return new ViewExportActivity((ViewExportPlace) place,
					clientFactory);

		} else if (place instanceof ViewOperatePlace) {
			return new ViewOperateActivity((ViewOperatePlace) place,
					clientFactory);
		}

		return null;
	}

}
