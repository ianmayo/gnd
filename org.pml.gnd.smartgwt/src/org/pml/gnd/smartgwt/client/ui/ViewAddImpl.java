package org.pml.gnd.smartgwt.client.ui;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.types.LayoutPolicy;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Progressbar;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.TimeItem;
import com.smartgwt.client.widgets.form.fields.UploadItem;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * Sample implementation of {@link ViewAdd}.
 */
public class ViewAddImpl extends FlowPanel implements ViewAdd {
	private Presenter listener;

	public ViewAddImpl() {

		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setStyleName("gwt-PanelBG2");
		add(verticalPanel);
		verticalPanel.setSize("777px", "314px");

		Label lblAddingFile = new Label("Adding a file");
		verticalPanel.add(lblAddingFile);
		lblAddingFile.setStyleName("gwt-LabelBig");

		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel.add(horizontalPanel);
		horizontalPanel.setWidth("100%");

		VerticalPanel verticalPanel_1 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_1);
		verticalPanel_1.setWidth("320px");

		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		horizontalPanel_1.setSpacing(10);
		verticalPanel_1.add(horizontalPanel_1);
		horizontalPanel_1.setWidth("100%");

		VerticalPanel verticalPanel_2 = new VerticalPanel();
		verticalPanel_2.setStyleName("gwt-PanelBorder");
		verticalPanel_2.setSpacing(10);
		horizontalPanel_1.add(verticalPanel_2);
		verticalPanel_2.setWidth("100%");

		Label lblSelectFile = new Label("1. Select file");
		lblSelectFile.setStyleName("gwt-LabelBig3");
		verticalPanel_2.add(lblSelectFile);

		VLayout layout = new VLayout();
		layout.setWidth("280px");

		DynamicForm dynamicForm = new DynamicForm();
		dynamicForm.setHeight("");
		dynamicForm.setStyleName("gwt-LabelBig3");
		dynamicForm.setFields(new FormItem[] { new UploadItem(
				"newUploadItem_1", "File") });
		layout.addMember(dynamicForm);

		Label lblOrDrag = new Label("Or drag/drop files into this area");
		lblOrDrag.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		lblOrDrag.setStyleName("gwt-LabelBig4_with_border");
		layout.addMember(lblOrDrag);
		lblOrDrag.setHeight("60px");
		verticalPanel_2.add(layout);

		HorizontalPanel horizontalPanel_3 = new HorizontalPanel();
		horizontalPanel_3.setSpacing(10);
		verticalPanel_1.add(horizontalPanel_3);
		horizontalPanel_3.setWidth("100%");

		VerticalPanel verticalPanel_3 = new VerticalPanel();
		verticalPanel_3.setStyleName("gwt-PanelBorder");
		verticalPanel_3.setSpacing(10);
		horizontalPanel_3.add(verticalPanel_3);
		verticalPanel_3.setWidth("100%");

		Label lblSelectFile_1 = new Label("2. Entry metadata");
		lblSelectFile_1.setStyleName("gwt-LabelBig3");
		verticalPanel_3.add(lblSelectFile_1);

		VLayout layout_2 = new VLayout();
		layout_2.setWidth("280px");

		DynamicForm dynamicForm_1 = new DynamicForm();
		dynamicForm_1.setWidth("250px");
		dynamicForm_1.setStyleName("gwt-LabelBig3");
		dynamicForm_1.setFields(new FormItem[] {
				new ComboBoxItem("cbxPlatform", "Platform"),
				new SelectItem("newSelectItem_6", "Sensor"),
				new ComboBoxItem("cbxPlatformType", "Exercise"),
				new TextItem("newTextItem_6", "Media ref"),
				new TextAreaItem("newTextAreaItem_5", "Notes") });

		layout_2.addMember(dynamicForm_1);
		verticalPanel_3.add(layout_2);

		VerticalPanel verticalPanel_4 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_4);
		verticalPanel_4.setWidth("320px");

		HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		horizontalPanel_2.setSpacing(10);
		verticalPanel_4.add(horizontalPanel_2);
		horizontalPanel_2.setWidth("100%");

		VerticalPanel verticalPanel_5 = new VerticalPanel();
		verticalPanel_5.setStyleName("gwt-PanelBorder");
		verticalPanel_5.setSpacing(10);
		horizontalPanel_2.add(verticalPanel_5);
		verticalPanel_5.setWidth("100%");

		Label lblSpecifyFiletype = new Label("3. Specify file-type");
		lblSpecifyFiletype.setStyleName("gwt-LabelBig3");
		verticalPanel_5.add(lblSpecifyFiletype);

		VLayout layout_1 = new VLayout();
		layout_1.setSize("280px", "55px");

		DynamicForm dynamicForm_2 = new DynamicForm();
		dynamicForm_2.setStyleName("gwt-LabelBig3");
		dynamicForm_2.setFields(new FormItem[] { new ComboBoxItem(
				"cbxPlatform", "Format") });
		layout_1.addMember(dynamicForm_2);
		verticalPanel_5.add(layout_1);

		HorizontalPanel horizontalPanel_4 = new HorizontalPanel();
		horizontalPanel_4.setSpacing(10);
		verticalPanel_4.add(horizontalPanel_4);
		horizontalPanel_4.setWidth("100%");

		VerticalPanel verticalPanel_6 = new VerticalPanel();
		verticalPanel_6.setStyleName("gwt-PanelBorder");
		verticalPanel_6.setSpacing(10);
		horizontalPanel_4.add(verticalPanel_6);
		verticalPanel_6.setWidth("100%");

		Label lblProvideSupplemental = new Label("4. Provide supplemental data");
		lblProvideSupplemental.setStyleName("gwt-LabelBig3");
		verticalPanel_6.add(lblProvideSupplemental);

		VLayout layout_3 = new VLayout();
		layout_3.setSize("280px", "72px");

		DynamicForm dynamicForm_3 = new DynamicForm();
		dynamicForm_3.setStyleName("gwt-LabelBig3");
		dynamicForm_3.setFields(new FormItem[] {
				new DateItem("newDateItem_2", "Start date"),
				new TimeItem("newTimeItem_3", "Start time") });
		layout_3.addMember(dynamicForm_3);
		verticalPanel_6.add(layout_3);

		HorizontalPanel horizontalPanel_5 = new HorizontalPanel();
		horizontalPanel_5.setSpacing(10);
		verticalPanel_4.add(horizontalPanel_5);
		horizontalPanel_5.setWidth("100%");

		VerticalPanel verticalPanel_7 = new VerticalPanel();
		verticalPanel_7.setStyleName("gwt-PanelBorder");
		verticalPanel_7.setSpacing(10);
		horizontalPanel_5.add(verticalPanel_7);
		verticalPanel_7.setWidth("100%");

		Label lblImport = new Label("5. Import");
		lblImport.setStyleName("gwt-LabelBig3");
		verticalPanel_7.add(lblImport);

		VLayout layout_4 = new VLayout();
		layout_4.setWidth("280px");
		layout_4.setVPolicy(LayoutPolicy.NONE);

		Button btnImport = new Button("Import");
		layout_4.addMember(btnImport);

		Progressbar progressbar = new Progressbar();
		progressbar.setHeight("30");
		layout_4.addMember(progressbar);
		progressbar.setPercentDone(30);
		verticalPanel_7.add(layout_4);
		verticalPanel_7.setCellHorizontalAlignment(layout_4,
				HasHorizontalAlignment.ALIGN_RIGHT);

	}

	@Override
	public void setName(String name) {
	}

	@Override
	public void setPresenter(Presenter listener) {
		this.listener = listener;
	}

}
