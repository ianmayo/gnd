package org.pml.gnd.smartgwt.client.ui;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * Sample implementation of {@link ViewOperate}.
 */
public class ViewOperateImpl extends FlowPanel implements ViewOperate {
	private Presenter listener;
	private StaticTextItem lblExplanation;
	private SelectItem cbxOperation;
	private SelectItem cbxFrequency;
	private CheckboxItem chxStartAtNextInterval;

	public ViewOperateImpl() {

		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setStyleName("gwt-PanelBG2");
		add(verticalPanel);
		verticalPanel.setSize("777px", "314px");

		Label lblOperateOnFiles = new Label("Operate on file(s)");
		lblOperateOnFiles.setStyleName("gwt-LabelBig");
		verticalPanel.add(lblOperateOnFiles);

		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel.add(horizontalPanel);
		horizontalPanel.setSize("100%", "326px");

		VerticalPanel verticalPanel_1 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_1);
		verticalPanel_1.setSize("320px", "186px");

		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		horizontalPanel_1.setSpacing(10);
		verticalPanel_1.add(horizontalPanel_1);
		horizontalPanel_1.setSize("100%", "100%");

		VerticalPanel verticalPanel_2 = new VerticalPanel();
		verticalPanel_2.setStyleName("gwt-PanelBorder");
		verticalPanel_2.setSpacing(10);
		horizontalPanel_1.add(verticalPanel_2);
		verticalPanel_2.setSize("100%", "100%");

		Label lblSelectFiles = new Label("1. Select file(s)");
		lblSelectFiles.setStyleName("gwt-LabelBig3");
		verticalPanel_2.add(lblSelectFiles);

		VLayout layout = new VLayout();
		layout.setWidth("280px");

		DynamicForm dynamicForm = new DynamicForm();
		dynamicForm.setHeight("");
		dynamicForm.setStyleName("gwt-LabelBig3");
		CanvasItem canvasItem = new CanvasItem("newCanvasItem_1", "Files");

		ListGrid listGrid = new ListGrid();
		listGrid.setWidth("220px");
		ListGridField listGridField = new ListGridField("select", "Select");
		listGridField.setWidth(60);
		listGrid.setFields(new ListGridField[] { listGridField,
				new ListGridField("filename", "File name") });
		canvasItem.setCanvas(listGrid);
		dynamicForm.setFields(new FormItem[] { canvasItem });
		layout.addMember(dynamicForm);
		verticalPanel_2.add(layout);

		VerticalPanel verticalPanel_3 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_3);
		verticalPanel_3.setSize("300px", "186px");

		HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		horizontalPanel_2.setSpacing(10);
		verticalPanel_3.add(horizontalPanel_2);
		horizontalPanel_2.setSize("100%", "100%");

		VerticalPanel verticalPanel_4 = new VerticalPanel();
		verticalPanel_4.setStyleName("gwt-PanelBorder");
		verticalPanel_4.setSpacing(10);
		horizontalPanel_2.add(verticalPanel_4);
		verticalPanel_4.setSize("100%", "100%");

		Label lblSpecifyOperation = new Label("2. Specify operation");
		lblSpecifyOperation.setStyleName("gwt-LabelBig3");
		verticalPanel_4.add(lblSpecifyOperation);

		VLayout layout_1 = new VLayout();
		layout_1.setWidth("");

		DynamicForm dynamicForm_1 = new DynamicForm();
		dynamicForm_1.setSize("250px", "");
		dynamicForm_1.setStyleName("gwt-LabelBig3");
		lblExplanation = new StaticTextItem("lblExplanation", "Explanation");
		cbxOperation = new SelectItem("cbxOperation", "Operation");
		cbxOperation.addChangedHandler(new CbxOperationChangedHandler());
		dynamicForm_1
				.setFields(new FormItem[] { cbxOperation, lblExplanation });
		layout_1.addMember(dynamicForm_1);
		verticalPanel_4.add(layout_1);

		VerticalPanel verticalPanel_5 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_5);
		verticalPanel_5.setSize("292px", "186px");

		HorizontalPanel horizontalPanel_5 = new HorizontalPanel();
		horizontalPanel_5.setSpacing(10);
		verticalPanel_5.add(horizontalPanel_5);
		horizontalPanel_5.setSize("100%", "186px");

		VerticalPanel verticalPanel_7 = new VerticalPanel();
		verticalPanel_7.setStyleName("gwt-PanelBorder");
		verticalPanel_7.setSpacing(10);
		horizontalPanel_5.add(verticalPanel_7);
		verticalPanel_7.setSize("100%", "100%");

		Label lblEnterSupplemental = new Label("3. Enter supplemental details");
		lblEnterSupplemental.setStyleName("gwt-LabelBig3");
		verticalPanel_7.add(lblEnterSupplemental);
		lblEnterSupplemental.setWidth("272px");

		VLayout layout_2 = new VLayout();
		layout_2.setWidth("");

		DynamicForm dynamicForm_2 = new DynamicForm();
		dynamicForm_2.setSize("250px", "");
		dynamicForm_2.setStyleName("gwt-LabelBig3");
		cbxFrequency = new SelectItem("cbxFrequency", "Frequency");
		chxStartAtNextInterval = new CheckboxItem("newCheckboxItem_2",
				"Start at next interval");
		chxStartAtNextInterval.setValue(true);
		dynamicForm_2.setFields(new FormItem[] { cbxFrequency,
				chxStartAtNextInterval });
		layout_2.addMember(dynamicForm_2);
		verticalPanel_7.add(layout_2);

		HorizontalPanel horizontalPanel_3 = new HorizontalPanel();
		horizontalPanel_3.setSpacing(10);
		verticalPanel_5.add(horizontalPanel_3);
		horizontalPanel_3.setSize("100%", "100%");

		VerticalPanel verticalPanel_6 = new VerticalPanel();
		verticalPanel_6.setStyleName("gwt-PanelBorder");
		verticalPanel_6.setSpacing(10);
		horizontalPanel_3.add(verticalPanel_6);
		verticalPanel_6.setSize("100%", "100%");

		Label label_4 = new Label("3. Ready to export");
		label_4.setStyleName("gwt-LabelBig3");
		verticalPanel_6.add(label_4);

		HorizontalPanel horizontalPanel_4 = new HorizontalPanel();
		horizontalPanel_4.setSpacing(10);
		horizontalPanel_4
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		verticalPanel_6.add(horizontalPanel_4);

		Button btnRun = new Button("New button");
		btnRun.setText("Run");
		horizontalPanel_4.add(btnRun);
		btnRun.setWidth("70px");

	}

	@Override
	public void setName(String name) {
	}

	@Override
	public void setPresenter(Presenter listener) {
		this.listener = listener;
	}

	private class CbxOperationChangedHandler implements ChangedHandler {
		public void onChanged(ChangedEvent event) {

			// Test
			lblExplanation.setValue("Resample positions");
		}
	}
}
