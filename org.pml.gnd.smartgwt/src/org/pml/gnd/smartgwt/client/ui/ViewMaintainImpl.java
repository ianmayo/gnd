package org.pml.gnd.smartgwt.client.ui;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Sample implementation of {@link ViewMaintain}.
 */
public class ViewMaintainImpl extends FlowPanel implements ViewMaintain {
	private Presenter listener;

	public ViewMaintainImpl() {

		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setStyleName("gwt-PanelBG2");
		add(verticalPanel);
		verticalPanel.setSize("777px", "314px");

		Label lblMaintainGnd = new Label("Maintain GND");
		lblMaintainGnd.setStyleName("gwt-LabelBig");
		verticalPanel.add(lblMaintainGnd);

		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel.add(horizontalPanel);
		horizontalPanel.setWidth("100%");

		VerticalPanel verticalPanel_1 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_1);
		verticalPanel_1.setWidth("320px");

		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		horizontalPanel_1.setSpacing(10);
		verticalPanel_1.add(horizontalPanel_1);
		horizontalPanel_1.setWidth("100%");

		VerticalPanel verticalPanel_2 = new VerticalPanel();
		verticalPanel_2.setStyleName("gwt-PanelBorder");
		verticalPanel_2.setSpacing(10);
		horizontalPanel_1.add(verticalPanel_2);
		verticalPanel_2.setWidth("100%");

		Label lblMaintainMeta = new Label("Maintain metadata");
		lblMaintainMeta.setStyleName("gwt-LabelBig3");
		verticalPanel_2.add(lblMaintainMeta);

		Label lblUiSufficient = new Label(
				"UI sufficient to allow addition of new sensors, trials, platforms");
		lblUiSufficient.setStyleName("gwt-LabelBig5");
		verticalPanel_2.add(lblUiSufficient);

		VerticalPanel verticalPanel_3 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_3);
		verticalPanel_3.setWidth("320px");

		HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		horizontalPanel_2.setSpacing(10);
		verticalPanel_3.add(horizontalPanel_2);
		horizontalPanel_2.setWidth("100%");

		VerticalPanel verticalPanel_4 = new VerticalPanel();
		verticalPanel_4.setStyleName("gwt-PanelBorder");
		verticalPanel_4.setSpacing(10);
		horizontalPanel_2.add(verticalPanel_4);
		verticalPanel_4.setWidth("100%");

		Label lblMaintainData = new Label("Maintain data");
		lblMaintainData.setStyleName("gwt-LabelBig3");
		verticalPanel_4.add(lblMaintainData);

		Label lblUiSufficientTo = new Label(
				"UI sufficient to allow deletion of datasets, or particular versions of datasets");
		lblUiSufficientTo.setStyleName("gwt-LabelBig5");
		verticalPanel_4.add(lblUiSufficientTo);

		VerticalPanel verticalPanel_5 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_5);
		verticalPanel_5.setWidth("320px");

		HorizontalPanel horizontalPanel_3 = new HorizontalPanel();
		horizontalPanel_3.setSpacing(10);
		verticalPanel_5.add(horizontalPanel_3);
		horizontalPanel_3.setWidth("100%");

		VerticalPanel verticalPanel_6 = new VerticalPanel();
		verticalPanel_6.setStyleName("gwt-PanelBorder");
		verticalPanel_6.setSpacing(10);
		horizontalPanel_3.add(verticalPanel_6);
		verticalPanel_6.setWidth("100%");

		Label lblMaintainUsers = new Label("Maintain users");
		lblMaintainUsers.setStyleName("gwt-LabelBig3");
		verticalPanel_6.add(lblMaintainUsers);

		Label lblUiTo = new Label(
				"UI to maintain list of users with edited privileges for :");
		lblUiTo.setStyleName("gwt-LabelBig5");
		verticalPanel_6.add(lblUiTo);

		VerticalPanel verticalPanel_7 = new VerticalPanel();
		verticalPanel_6.add(verticalPanel_7);

		Label lblImport = new Label("import");
		lblImport.setStyleName("gwt-LabelBig5");
		verticalPanel_7.add(lblImport);

		Label lblTransform = new Label("transform");
		lblTransform.setStyleName("gwt-LabelBig5");
		verticalPanel_7.add(lblTransform);

		Label lblCreate = new Label("create");
		lblCreate.setStyleName("gwt-LabelBig5");
		verticalPanel_7.add(lblCreate);

		Label lblDelete = new Label("delete");
		lblDelete.setStyleName("gwt-LabelBig5");
		verticalPanel_7.add(lblDelete);

	}

	@Override
	public void setName(String name) {
	}

	@Override
	public void setPresenter(Presenter listener) {
		this.listener = listener;
	}

}
