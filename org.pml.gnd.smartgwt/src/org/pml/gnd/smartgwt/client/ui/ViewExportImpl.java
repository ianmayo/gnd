package org.pml.gnd.smartgwt.client.ui;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.UploadItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * Sample implementation of {@link ViewExport}.
 */
public class ViewExportImpl extends FlowPanel implements ViewExport {
	private Presenter listener;
	private ListGrid grdExportFotmat;
	private UploadItem uploadItem;
	private SelectItem selectItem;
	private Button btnExport;

	public ViewExportImpl() {

		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setStyleName("gwt-PanelBG2");
		add(verticalPanel);
		verticalPanel.setSize("777px", "314px");

		Label lblExport = new Label("Export");
		lblExport.setStyleName("gwt-LabelBig");
		verticalPanel.add(lblExport);

		Label lblTitleTestData = new Label("Title: Test_Data");
		lblTitleTestData.setStyleName("gwt-LabelBig");
		verticalPanel.add(lblTitleTestData);

		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel.add(horizontalPanel);
		horizontalPanel.setWidth("100%");

		VerticalPanel verticalPanel_1 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_1);
		verticalPanel_1.setSize("320px", "186px");

		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		horizontalPanel_1.setSpacing(10);
		verticalPanel_1.add(horizontalPanel_1);
		horizontalPanel_1.setSize("100%", "100%");

		VerticalPanel verticalPanel_2 = new VerticalPanel();
		verticalPanel_2.setStyleName("gwt-PanelBorder");
		verticalPanel_2.setSpacing(10);
		horizontalPanel_1.add(verticalPanel_2);
		verticalPanel_2.setSize("100%", "100%");

		Label lblSelectExport = new Label("1. Select export format");
		lblSelectExport.setStyleName("gwt-LabelBig3");
		verticalPanel_2.add(lblSelectExport);

		VLayout layout = new VLayout();
		layout.setWidth("280px");

		DynamicForm dynamicForm = new DynamicForm();
		dynamicForm.setHeight("");
		dynamicForm.setStyleName("gwt-LabelBig3");
		CanvasItem canvasItem = new CanvasItem("newCanvasItem_1", "Format");

		grdExportFotmat = new ListGrid();
		grdExportFotmat.setFields(new ListGridField[] { new ListGridField(
				"newField", "Export format") });
		canvasItem.setCanvas(grdExportFotmat);
		dynamicForm.setFields(new FormItem[] { canvasItem });
		layout.addMember(dynamicForm);
		verticalPanel_2.add(layout);

		VerticalPanel verticalPanel_3 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_3);
		verticalPanel_3.setSize("340px", "186px");

		HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		horizontalPanel_2.setSpacing(10);
		verticalPanel_3.add(horizontalPanel_2);
		horizontalPanel_2.setSize("100%", "100%");

		VerticalPanel verticalPanel_4 = new VerticalPanel();
		verticalPanel_4.setStyleName("gwt-PanelBorder");
		verticalPanel_4.setSpacing(10);
		horizontalPanel_2.add(verticalPanel_4);
		verticalPanel_4.setSize("100%", "100%");

		Label lblEnterSupplemental = new Label("2. Enter supplemental info");
		lblEnterSupplemental.setStyleName("gwt-LabelBig3");
		verticalPanel_4.add(lblEnterSupplemental);

		VLayout layout_1 = new VLayout();
		layout_1.setWidth("320px");

		DynamicForm dynamicForm_1 = new DynamicForm();
		dynamicForm_1.setHeight("");
		dynamicForm_1.setStyleName("gwt-LabelBig3");
		uploadItem = new UploadItem("fileExportTarget", "Target file");
		selectItem = new SelectItem("cbxFont", "Font");
		dynamicForm_1.setFields(new FormItem[] { uploadItem, selectItem });
		layout_1.addMember(dynamicForm_1);
		verticalPanel_4.add(layout_1);

		VerticalPanel verticalPanel_5 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_5);
		verticalPanel_5.setSize("220px", "186px");

		HorizontalPanel horizontalPanel_3 = new HorizontalPanel();
		horizontalPanel_3.setSpacing(10);
		verticalPanel_5.add(horizontalPanel_3);
		horizontalPanel_3.setSize("100%", "100%");

		VerticalPanel verticalPanel_6 = new VerticalPanel();
		verticalPanel_6.setStyleName("gwt-PanelBorder");
		verticalPanel_6.setSpacing(10);
		horizontalPanel_3.add(verticalPanel_6);
		verticalPanel_6.setSize("100%", "100%");

		Label lblReadyTo = new Label("3. Ready to export");
		lblReadyTo.setStyleName("gwt-LabelBig3");
		verticalPanel_6.add(lblReadyTo);

		HorizontalPanel horizontalPanel_4 = new HorizontalPanel();
		horizontalPanel_4
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		horizontalPanel_4.setSpacing(10);
		verticalPanel_6.add(horizontalPanel_4);
		verticalPanel_6.setCellHorizontalAlignment(horizontalPanel_4,
				HasHorizontalAlignment.ALIGN_RIGHT);

		Button btnCancel = new Button("New button");
		btnCancel.setText("Cancel");
		horizontalPanel_4.add(btnCancel);
		btnCancel.setWidth("70px");

		btnExport = new Button("New button");
		btnExport.setText("Export");
		horizontalPanel_4.add(btnExport);
		btnExport.setWidth("70px");

	}

	@Override
	public void setName(String name) {
	}

	@Override
	public void setPresenter(Presenter listener) {
		this.listener = listener;
	}

}
