package org.pml.gnd.smartgwt.client.ui;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.widgets.Label;

/**
 * Sample implementation of {@link ViewWelcomeScreen}.
 */
public class ViewWelcomeScreenImpl extends FlowPanel implements
		ViewWelcomeScreen {

	private Presenter listener;
	/**
	 * @wbp.nonvisual location=65,417
	 */
	private final DataSource dsTracks = new DataSource();
	private Button btnMaintain;
	private Button btnAddData;
	private Button btnBrowse;
	private VerticalPanel vtpanWhatsNew;
	private Button btnExport;

	// private Presenter presenter;

	public ViewWelcomeScreenImpl() {

		setWidth("900px");

		dsTracks.setDataURL("http://localhost:9090/data/data_tracks2.js");
		dsTracks.setDataFormat(DSDataFormat.JSON);

		DataSourceTextField dataSourceTextField = new DataSourceTextField(
				"value", "value");
		dsTracks.addField(dataSourceTextField);

		VerticalPanel verticalPanel = new VerticalPanel();
		add(verticalPanel);
		verticalPanel.setSize("900px", "100%");

		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel.add(horizontalPanel);
		horizontalPanel.setWidth("100%");

		Label lblNewLabel = new Label(
				"Welcome to the Geospatial Network Database");
		lblNewLabel.setWidth("900px");
		lblNewLabel.setStyleName("gwt-LabelHeader");
		lblNewLabel.setAlign(Alignment.CENTER);
		horizontalPanel.add(lblNewLabel);

		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		verticalPanel.add(horizontalPanel_1);
		horizontalPanel_1.setWidth("100%");

		VerticalPanel verticalPanel_1 = new VerticalPanel();
		horizontalPanel_1.add(verticalPanel_1);
		horizontalPanel_1.setCellWidth(verticalPanel_1, "350px");

		HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		horizontalPanel_2.setSpacing(10);
		verticalPanel_1.add(horizontalPanel_2);
		verticalPanel_1.setCellWidth(horizontalPanel_2, "350px");

		VerticalPanel verticalPanel_3 = new VerticalPanel();
		verticalPanel_3.setSpacing(10);
		horizontalPanel_2.add(verticalPanel_3);

		com.google.gwt.user.client.ui.Label lblNewLabel_1 = new com.google.gwt.user.client.ui.Label(
				"Welcome,");
		lblNewLabel_1.setStyleName("gwt-LabelBig");
		verticalPanel_3.add(lblNewLabel_1);

		com.google.gwt.user.client.ui.Label lblWhatIts = new com.google.gwt.user.client.ui.Label(
				"-What it's for");
		lblWhatIts.setStyleName("gwt-LabelBig2");
		verticalPanel_3.add(lblWhatIts);

		com.google.gwt.user.client.ui.Label lblwhoItsFor = new com.google.gwt.user.client.ui.Label(
				"-Who it's for");
		lblwhoItsFor.setStyleName("gwt-LabelBig2");
		verticalPanel_3.add(lblwhoItsFor);

		com.google.gwt.user.client.ui.Label lblhowToGet = new com.google.gwt.user.client.ui.Label(
				"-How to get support");
		lblhowToGet.setStyleName("gwt-LabelBig2");
		verticalPanel_3.add(lblhowToGet);

		com.google.gwt.user.client.ui.Label lblhowToFind = new com.google.gwt.user.client.ui.Label(
				"-How to find out more");
		lblhowToFind.setStyleName("gwt-LabelBig2");
		verticalPanel_3.add(lblhowToFind);

		VerticalPanel verticalPanel_4 = new VerticalPanel();
		verticalPanel_4.setSpacing(10);
		horizontalPanel_2.add(verticalPanel_4);

		btnBrowse = new Button("New button");
		btnBrowse.addClickHandler(new BtnBrowseClickHandler());
		btnBrowse.setText("Browse");
		verticalPanel_4.add(btnBrowse);

		btnAddData = new Button("New button");
		btnAddData.addClickHandler(new BtnAddDataClickHandler());
		btnAddData.setText("Add Data");
		verticalPanel_4.add(btnAddData);
		btnAddData.setWidth("80px");

		btnMaintain = new Button("New button");
		btnMaintain.addClickHandler(new BtnMaintainClickHandler());
		btnMaintain.setText("Maintain");
		verticalPanel_4.add(btnMaintain);

		btnExport = new Button("Export");
		btnExport.addClickHandler(new BtnExportClickHandler());
		verticalPanel_4.add(btnExport);

		Button btnOperation = new Button("Export");
		btnOperation.addClickHandler(new BtnOperationClickHandler());
		btnOperation.setText("Operation");
		verticalPanel_4.add(btnOperation);

		VerticalPanel verticalPanel_2 = new VerticalPanel();
		verticalPanel_2.setSpacing(10);
		horizontalPanel_1.add(verticalPanel_2);

		VerticalPanel verticalPanel_5 = new VerticalPanel();
		verticalPanel_5.setSpacing(10);
		verticalPanel_2.add(verticalPanel_5);

		com.google.gwt.user.client.ui.Label lblWhatsNew = new com.google.gwt.user.client.ui.Label(
				"What's new");
		lblWhatsNew.setStyleName("gwt-LabelBig");
		verticalPanel_5.add(lblWhatsNew);

		vtpanWhatsNew = new VerticalPanel();
		verticalPanel_5.add(vtpanWhatsNew);

		com.google.gwt.user.client.ui.Label lblCdVehicle = new com.google.gwt.user.client.ui.Label(
				"CD-12/273 Vehicle 23");
		lblCdVehicle.setStyleName("gwt-LabelWhatsNewItem");
		vtpanWhatsNew.add(lblCdVehicle);

		com.google.gwt.user.client.ui.Label label = new com.google.gwt.user.client.ui.Label(
				"CD-12/273 Vehicle 23");
		vtpanWhatsNew.add(label);
		label.setStyleName("gwt-LabelWhatsNewItem");

		com.google.gwt.user.client.ui.Label label_1 = new com.google.gwt.user.client.ui.Label(
				"CD-12/273 Vehicle 23");
		label_1.setStyleName("gwt-LabelWhatsNewItem");
		vtpanWhatsNew.add(label_1);

		com.google.gwt.user.client.ui.Label label_2 = new com.google.gwt.user.client.ui.Label(
				"CD-12/273 Vehicle 23");
		label_2.setStyleName("gwt-LabelWhatsNewItem");
		vtpanWhatsNew.add(label_2);

		com.google.gwt.user.client.ui.Label label_3 = new com.google.gwt.user.client.ui.Label(
				"CD-12/273 Vehicle 23");
		label_3.setStyleName("gwt-LabelWhatsNewItem");
		vtpanWhatsNew.add(label_3);

	}

	@Override
	public void setName(String name) {
	}

	@Override
	public void setPresenter(Presenter listener) {
		this.listener = listener;
	}

	private class BtnBrowseClickHandler implements ClickHandler {
		public void onClick(ClickEvent event) {

			// presenter.goTo(new ViewBrowsePlace("place"));

			History.newItem("browse");

		}
	}

	private class BtnAddDataClickHandler implements ClickHandler {
		public void onClick(ClickEvent event) {

			History.newItem("add");

		}
	}

	private class BtnMaintainClickHandler implements ClickHandler {
		public void onClick(ClickEvent event) {

			History.newItem("maintain");

		}
	}

	private class BtnExportClickHandler implements ClickHandler {
		public void onClick(ClickEvent event) {

			History.newItem("export");

		}
	}

	private class BtnOperationClickHandler implements ClickHandler {
		public void onClick(ClickEvent event) {

			History.newItem("operation");
		}
	}
}
