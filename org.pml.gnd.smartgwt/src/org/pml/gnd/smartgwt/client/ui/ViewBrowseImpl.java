package org.pml.gnd.smartgwt.client.ui;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.MiniDateRangeItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * Sample implementation of {@link ViewBrowse}.
 */
public class ViewBrowseImpl extends FlowPanel implements ViewBrowse {

	private Presenter listener;
	private TextItem tctSearch;
	private ComboBoxItem cbxPlatform;
	private ComboBoxItem cbxPlatformType;
	private ComboBoxItem cbxSensor;
	private ComboBoxItem cbxSensorType;
	private DynamicForm dynamicForm;
	private SelectItem cbxTrial;
	private MiniDateRangeItem drimDate;
	private ListGridField listGridFieldRef;
	private ListGridField listGridFieldName;
	private ListGridField listGridFieldSource;
	private ListGridField listGridFieldSensor;
	private ListGridField listGridFieldView;
	private ListGridField listGridFieldAddToBasket;
	private HLayout hLayout;
	private Button btnPrevious;
	private Button btnNext;

	public ViewBrowseImpl() {

		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setStyleName("gwt-PanelBG1");
		add(verticalPanel);
		verticalPanel.setSize("777px", "314px");

		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		verticalPanel.add(horizontalPanel_1);
		horizontalPanel_1.setWidth("100%");

		VerticalPanel verticalPanel_1 = new VerticalPanel();
		horizontalPanel_1.add(verticalPanel_1);
		horizontalPanel_1.setCellWidth(verticalPanel_1, "300px");

		HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		horizontalPanel_2.setSpacing(10);
		verticalPanel_1.add(horizontalPanel_2);
		horizontalPanel_2.setWidth("250px");

		VerticalPanel verticalPanel_2 = new VerticalPanel();
		verticalPanel_2.setSpacing(10);
		horizontalPanel_2.add(verticalPanel_2);
		verticalPanel_2.setWidth("234px");

		com.google.gwt.user.client.ui.Label lblLiveFilter = new com.google.gwt.user.client.ui.Label(
				"Live filter");
		lblLiveFilter.setStyleName("gwt-LabelBig");
		verticalPanel_2.add(lblLiveFilter);

		VLayout layout = new VLayout();
		layout.setWidth("250px");

		dynamicForm = new DynamicForm();
		dynamicForm.setWidth("250px");
		dynamicForm.setStyleName("gwt-LabelBig3");
		tctSearch = new TextItem("tctSearch", "Search");
		cbxPlatform = new ComboBoxItem("cbxPlatform", "Platform");
		cbxPlatformType = new ComboBoxItem("cbxPlatformType", " Platform Type");
		cbxSensor = new ComboBoxItem("cbxPlatformType", "Sensor");
		cbxSensorType = new ComboBoxItem("cbxPlatformType", "Sensor Type");
		cbxTrial = new SelectItem("newSelectItem_6", "New SelectItem");
		drimDate = new MiniDateRangeItem("newMiniDateRangeItem_7",
				"Date Between");
		drimDate.setWidth("100%");

		dynamicForm
				.setFields(new FormItem[] { tctSearch, cbxPlatform,
						cbxPlatformType, cbxSensor, cbxSensorType, cbxTrial,
						drimDate });
		layout.addMember(dynamicForm);
		dynamicForm.moveTo(0, 10);
		verticalPanel_2.add(layout);

		VerticalPanel verticalPanel_4 = new VerticalPanel();
		verticalPanel_4.setSpacing(10);
		horizontalPanel_1.add(verticalPanel_4);
		verticalPanel_4.setHeight("259px");

		VerticalPanel verticalPanel_5 = new VerticalPanel();
		verticalPanel_5.setSpacing(10);
		verticalPanel_4.add(verticalPanel_5);
		verticalPanel_4.setCellHeight(verticalPanel_5, "44px]");

		com.google.gwt.user.client.ui.Label lblMatchingDatasets = new com.google.gwt.user.client.ui.Label(
				"Matching datasets");
		lblMatchingDatasets.setStyleName("gwt-LabelBig");
		verticalPanel_5.add(lblMatchingDatasets);

		VLayout layout_1 = new VLayout();
		verticalPanel_5.add(layout_1);
		layout_1.setSize("450px", "194px");

		ListGrid listGrid = new ListGrid();
		listGrid.setHeight("166px");
		listGridFieldRef = new ListGridField("ref", "Ref");
		listGridFieldName = new ListGridField("name", "Name");
		listGridFieldSource = new ListGridField("source", "Source");
		listGridFieldSensor = new ListGridField("sensor", "Sensor");
		listGridFieldView = new ListGridField("view", "View");
		listGridFieldAddToBasket = new ListGridField("addtobasket",
				"Add To Basket");
		listGridFieldAddToBasket.setWidth(90);
		listGrid.setFields(new ListGridField[] { listGridFieldRef,
				listGridFieldName, listGridFieldSource, listGridFieldSensor,
				listGridFieldView, listGridFieldAddToBasket });
		layout_1.addMember(listGrid);

		hLayout = new HLayout();

		btnPrevious = new Button("Previous");
		btnPrevious.setSize("70px", "22px");
		hLayout.addMember(btnPrevious);

		btnNext = new Button("Next");
		btnNext.setSize("70px", "22px");
		hLayout.addMember(btnNext);
		layout_1.addMember(hLayout);

	}

	@Override
	public void setName(String name) {
		// button.setHTML(name);
	}

	@Override
	public void setPresenter(Presenter listener) {
		this.listener = listener;
	}

}
