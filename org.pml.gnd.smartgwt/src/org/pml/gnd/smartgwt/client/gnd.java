package org.pml.gnd.smartgwt.client;

import org.pml.gnd.smartgwt.client.mvp.AppActivityMapper;
import org.pml.gnd.smartgwt.client.mvp.AppPlaceHistoryMapper;
import org.pml.gnd.smartgwt.client.place.ViewAddPlace;
import org.pml.gnd.smartgwt.client.place.ViewBrowsePlace;
import org.pml.gnd.smartgwt.client.place.ViewExportPlace;
import org.pml.gnd.smartgwt.client.place.ViewMaintainPlace;
import org.pml.gnd.smartgwt.client.place.ViewOperatePlace;
import org.pml.gnd.smartgwt.client.place.ViewWelcomeScreenPlace;

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.HistoryListener;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;

public class gnd implements EntryPoint, HistoryListener {

	private SimplePanel appWidget = new SimplePanel();
	private Place defaultPlace = new ViewWelcomeScreenPlace("a");

	AppPlaceHistoryMapper _historyMapper;
	private ClientFactory clientFactory;
	private PlaceController placeController;

	public void onModuleLoad() {

		clientFactory = GWT.create(ClientFactory.class);

		EventBus eventBus = clientFactory.getEventBus();
		placeController = clientFactory.getPlaceController();

		// Start ActivityManager for the main widget with our ActivityMapper
		ActivityMapper activityMapper = new AppActivityMapper(clientFactory);
		ActivityManager activityManager = new ActivityManager(activityMapper,
				eventBus);
		activityManager.setDisplay(appWidget);

		// Start PlaceHistoryHandler with our PlaceHistoryMapper
		_historyMapper = GWT.create(AppPlaceHistoryMapper.class);

		PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(
				_historyMapper);

		historyHandler.register(placeController, eventBus, defaultPlace);
		RootPanel.get().add(appWidget);

		// Goes to place represented on URL or default place
		historyHandler.handleCurrentHistory();

		History.addHistoryListener(this);

		// History.fireCurrentHistoryState();
	}

	@Override
	public void onHistoryChanged(String historyToken) {
		// TODO Auto-generated method stub

		// Window.alert("History " + historyToken);

		if (historyToken.equals("browse")) {

			placeController.goTo(new ViewBrowsePlace("browse"));

		} else if (historyToken.equals("add")) {

			placeController.goTo(new ViewAddPlace("add"));

		} else if (historyToken.equals("maintain")) {

			placeController.goTo(new ViewMaintainPlace("maintain"));

		} else if (historyToken.equals("export")) {

			placeController.goTo(new ViewExportPlace("export"));

		} else if (historyToken.equals("operation")) {

			placeController.goTo(new ViewOperatePlace("operation"));

		}

	}

}
