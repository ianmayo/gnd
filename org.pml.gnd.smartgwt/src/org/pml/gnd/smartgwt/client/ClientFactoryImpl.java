package org.pml.gnd.smartgwt.client;

import org.pml.gnd.smartgwt.client.ui.ViewAdd;
import org.pml.gnd.smartgwt.client.ui.ViewAddImpl;
import org.pml.gnd.smartgwt.client.ui.ViewBrowse;
import org.pml.gnd.smartgwt.client.ui.ViewBrowseImpl;
import org.pml.gnd.smartgwt.client.ui.ViewExport;
import org.pml.gnd.smartgwt.client.ui.ViewExportImpl;
import org.pml.gnd.smartgwt.client.ui.ViewMaintain;
import org.pml.gnd.smartgwt.client.ui.ViewMaintainImpl;
import org.pml.gnd.smartgwt.client.ui.ViewOperate;
import org.pml.gnd.smartgwt.client.ui.ViewOperateImpl;
import org.pml.gnd.smartgwt.client.ui.ViewWelcomeScreen;
import org.pml.gnd.smartgwt.client.ui.ViewWelcomeScreenImpl;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.place.shared.PlaceController;

/**
 * Sample implementation of {@link ClientFactory}.
 */
public class ClientFactoryImpl implements ClientFactory {

	private static final EventBus eventBus = new SimpleEventBus();
	private static final PlaceController placeController = new PlaceController(
			eventBus);

	private static final ViewWelcomeScreen viewWelcomeScreen = new ViewWelcomeScreenImpl();
	private static final ViewBrowse viewBrowse = new ViewBrowseImpl();
	private static final ViewAdd viewAdd = new ViewAddImpl();
	private static final ViewMaintain viewMaintain = new ViewMaintainImpl();
	private static final ViewExport viewExport = new ViewExportImpl();
	private static final ViewOperate viewOperate = new ViewOperateImpl();

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public PlaceController getPlaceController() {
		return placeController;
	}

	@Override
	public ViewWelcomeScreen getViewWelcomeScreen() {

		return viewWelcomeScreen;
	}

	@Override
	public ViewBrowse getViewBrowse() {

		return viewBrowse;
	}

	@Override
	public ViewAdd getViewAdd() {

		return viewAdd;
	}

	@Override
	public ViewMaintain getViewMaintain() {

		return viewMaintain;
	}

	@Override
	public ViewExport getViewExport() {

		return viewExport;
	}

	@Override
	public ViewOperate getViewOperate() {

		return viewOperate;
	}

}
