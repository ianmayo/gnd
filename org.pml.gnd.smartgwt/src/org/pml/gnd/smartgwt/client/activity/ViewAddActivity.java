package org.pml.gnd.smartgwt.client.activity;

import org.pml.gnd.smartgwt.client.ClientFactory;
import org.pml.gnd.smartgwt.client.place.ViewAddPlace;
import org.pml.gnd.smartgwt.client.ui.ViewAdd;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

/**
 * Activities are started and stopped by an ActivityManager associated with a
 * container Widget.
 */
public class ViewAddActivity extends AbstractActivity implements
		ViewAdd.Presenter {
	/**
	 * Used to obtain views, eventBus, placeController. Alternatively, could be
	 * injected via GIN.
	 */
	private ClientFactory clientFactory;

	/**
	 * Sample property.
	 */
	private String name;

	public ViewAddActivity(ViewAddPlace place, ClientFactory clientFactory) {
		this.name = place.getName();
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		ViewAdd view = clientFactory.getViewAdd();
		view.setName(name);
		view.setPresenter(this);
		containerWidget.setWidget(view.asWidget());
	}

	/*
	 * @Override public String mayStop() { return
	 * "Please hold on. This activity is stopping."; }
	 */
	/**
	 * @see ViewAdd.Presenter#goTo(Place)
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}
}
