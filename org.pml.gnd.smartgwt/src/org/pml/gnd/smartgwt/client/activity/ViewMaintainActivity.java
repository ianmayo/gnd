package org.pml.gnd.smartgwt.client.activity;

import org.pml.gnd.smartgwt.client.ClientFactory;
import org.pml.gnd.smartgwt.client.place.ViewMaintainPlace;
import org.pml.gnd.smartgwt.client.ui.ViewMaintain;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

/**
 * Activities are started and stopped by an ActivityManager associated with a
 * container Widget.
 */
public class ViewMaintainActivity extends AbstractActivity implements
		ViewMaintain.Presenter {
	/**
	 * Used to obtain views, eventBus, placeController. Alternatively, could be
	 * injected via GIN.
	 */
	private ClientFactory clientFactory;

	/**
	 * Sample property.
	 */
	private String name;

	public ViewMaintainActivity(ViewMaintainPlace place,
			ClientFactory clientFactory) {
		this.name = place.getName();
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		ViewMaintain view = clientFactory.getViewMaintain();
		view.setName(name);
		view.setPresenter(this);
		containerWidget.setWidget(view.asWidget());
	}

	/*
	 * @Override public String mayStop() { return
	 * "Please hold on. This activity is stopping."; }
	 */
	/**
	 * @see ViewMaintain.Presenter#goTo(Place)
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}
}
