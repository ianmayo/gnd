package org.pml.gnd.smartgwt.client;

import org.pml.gnd.smartgwt.client.ui.ViewAdd;
import org.pml.gnd.smartgwt.client.ui.ViewBrowse;
import org.pml.gnd.smartgwt.client.ui.ViewExport;
import org.pml.gnd.smartgwt.client.ui.ViewMaintain;
import org.pml.gnd.smartgwt.client.ui.ViewWelcomeScreen;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import org.pml.gnd.smartgwt.client.ui.ViewOperate;

/**
 * ClientFactory helpful to use a factory or dependency injection framework like
 * GIN to obtain references to objects needed throughout your application like
 * the {@link EventBus}, {@link PlaceController} and views.
 */
public interface ClientFactory {

	EventBus getEventBus();

	PlaceController getPlaceController();

	public ViewWelcomeScreen getViewWelcomeScreen();

	public ViewBrowse getViewBrowse();

	public ViewAdd getViewAdd();

	public ViewMaintain getViewMaintain();

	public ViewExport getViewExport();

	public ViewOperate getViewOperate();
}
