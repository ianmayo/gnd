package org.pml.gnd.smartgwt.model;

import com.smartgwt.client.types.DSDataFormat;

// This is one row of given JSON Data array
public class GndRecordCoords {

	// Data Source
	private GndRestDataSource dataSource;

	// Fields
	// Key Field
	private GndDataSourceTextField key;

	private GndDataSourceTextField lat;
	private GndDataSourceTextField lon;
	private GndDataSourceTextField alt;
	private GndDataSourceTextField acc;
	private GndDataSourceTextField time;

	public GndRecordCoords(String dataURL) {

		setDataSource(new GndRestDataSource());
		getDataSource().setDataFormat(DSDataFormat.JSON);
		getDataSource().setDataURL(dataURL);

		key = new GndDataSourceTextField("key", "Key");

		lat = new GndDataSourceTextField("lat", "Latitude");
		lon = new GndDataSourceTextField("lon", "Longitude");
		alt = new GndDataSourceTextField("alt", "Altitude");
		acc = new GndDataSourceTextField("acc", "Accuracy");
		time = new GndDataSourceTextField("time", "Time");

		getDataSource().setFields(key, lat, lon, alt, acc, time);

	}

	// Getter & Setter
	public GndRestDataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(GndRestDataSource dataSource) {
		this.dataSource = dataSource;
	}

}
