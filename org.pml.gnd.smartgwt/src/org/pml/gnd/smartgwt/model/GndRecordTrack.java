package org.pml.gnd.smartgwt.model;

import com.smartgwt.client.types.DSDataFormat;

// This is one row of given JSON Data array
public class GndRecordTrack {

	// Data Source
	private GndRestDataSource dataSource;

	// Fields
	// Key Field
	private GndDataSourceTextField key;

	private GndDataSourceTextField id;
	private GndDataSourceTextField value;

	public GndRecordTrack(String dataURL) {

		setDataSource(new GndRestDataSource());
		getDataSource().setDataFormat(DSDataFormat.JSON);
		getDataSource().setDataURL(dataURL);

		key = new GndDataSourceTextField("key", "Key");

		id = new GndDataSourceTextField("id", "ID");
		value = new GndDataSourceTextField("value", "Value");

		getDataSource().setFields(key, id, value);

	}

	// Getter & Setter
	public GndRestDataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(GndRestDataSource dataSource) {
		this.dataSource = dataSource;
	}

}
