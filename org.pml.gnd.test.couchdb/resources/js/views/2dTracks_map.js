function(doc) { 
	if (doc.metadata.type == 'track') { 
		if (doc.metadata.data_type.indexOf('lat') != -1 && doc.metadata.data_type.indexOf('lon') != -1 ) { 
			emit(
				doc.metadata.sensor == null ? '' : doc.metadata.sensor, 
				{'_id' : doc._id, '_rev' : doc._rev, 'name' : doc.metadata.name, 'trial' : doc.metadata.trial, 
					'platform' : doc.metadata.platform, 'platform_type' : doc.metadata.platform_type, 'sensor' : doc.metadata.sensor, 
					'sensor_type' : doc.metadata.sensor_type, 'lat' : doc.lat, 'lon' : doc.lon
				}
			); 
		} 
	} 
}