function (doc) {
	if (doc.metadata.type == "track") {
		emit(doc._id, doc);
	}
}