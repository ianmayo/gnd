function (doc) {
	if(doc.lat != null)
	{
		emit(doc._id, doc.metadata.name + " - " + doc.metadata.platform + " (" + doc.lat.length + ") pts");
	}
}