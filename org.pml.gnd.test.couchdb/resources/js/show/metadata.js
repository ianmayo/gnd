/*
 * @param callback - jsonp callback to specify
 */
function(doc, req) { 

	var res;
	var error = true;
	if(doc != null)
	{
		// convert our object structure to JSON		
		res = { "headers" : {"Content-Type" : "application/json"}, "body" : require("lib/gndCommon").toJSON(doc.metadata, req) };
		error = false;
	}
	
	if (error) {
		res = {"code": 500, "headers" : {"Content-Type" : "application/json"}, "body" : require("lib/gndCommon").toJSON(doc.metadata, res)}
	}

	
	return res;
	
}