/*
 * @param maxRows how many rows to output, or all rows if less than this value
 */
function(doc, req) { 
	var res;
	var error = true;
	if(doc == null)
		res = "doc not found";
	if(doc != null)
	{
		res = "metadata not found";
		var metadata = doc.metadata;
		if(metadata != null && metadata != undefined)
			{
			res = "document is not track";
			if (metadata.type == 'track') { 
				res = "document does not have lat/lon data";
				if (metadata.data_type.indexOf('lat') != -1 && metadata.data_type.indexOf('lon') != -1 ) 
					{
						var geo = { "type" : "Feature" };
						var properties = { "name" : metadata.name, "sensor" : metadata.sensor, "plaform" : metadata.platform,
							"sensor_type" : metadata.sensor_type, "platform_type" : metadata.platform_type, "trial" : metadata.trial,
							"id" : doc._id};
		
						// has a max num of rows been specified?
						var maxRows = doc.lat.length;
						if(req.query.maxRows != null)
							maxRows = Math.min(maxRows, req.query.maxRows);
		
						var time = [];
						var coords = [];
						// does the track have 3d data?
						var is3d = metadata.data_type.indexOf('z') != -1;
						
						// does the track have time?
						var hasTime = metadata.data_type.indexOf("time") != -1;
						
						// work out sampling frequency, for if we're requesting
						// fewer
						// rows than there are present
						var interval = doc.lat.length / ( maxRows * 1.0);
						
						// index for working through source arrays
						var i=0;
						
						// work out which target index to use
						var ctr = 0;
						
						// loop through to generate output data
						for (i=0;i<doc.lat.length;i+= interval) {
							
							// convert the floating point calculated index to an
							// integer
							var thisIndex = Math.round(i);
								
							// create a GeoJSON coordinate for this point
							var point = [ doc.lon[thisIndex], doc.lat[thisIndex] ];
							if (is3d) {
								point[2] = doc.z[thisIndex];
							}
							coords.push(point);				
							
							// also manage the time in here, so we adhere to the
							// row
							// counter
							if (hasTime)
								time[ctr] = doc.time[thisIndex];
							
							// increment our target counter
							ctr++;
						}
		
						// store the time array, if one was needed
						if(hasTime)
							properties["time"] = time;
						
						var geometry = { "type" : "LineString", "coordinates" : coords };
						geo["geometry"] = geometry;
						geo["properties"] = properties;
						error = false;
						
						// convert our object structure to JSON
						res = { "headers" : {"Content-Type" : "application/json"}, "body" : require("lib/gndCommon").toJSON(geo, req) };
						
					} 
			 	}
		} 
	}
	if (error) {
		res = {"code": 500, "headers" : {"Content-Type" : "application/json"}, "body" : "{\"error\" : \"" + res +"\"}"}
	}
	return  res
}