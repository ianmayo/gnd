exports.name = "Gnd common lib 1.0";

exports.toJSON = function(obj, req) {
	var json = JSON.stringify(obj);
	if (req == null || req.query == null || req.query.callback == null) {
		return json;
	} else {
		return req.query.callback + "(" + json + ")";
	}
};