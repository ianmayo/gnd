function (newDoc, oldDoc, userCtx) {
    if (newDoc.metadata != undefined && newDoc.metadata != null) if (newDoc.metadata.type == "track") {
        var i;
        var allowedFields = ["metadata"];
        var metaFields = ["type", "data_type", "name", "platform", "platform_type", "sensor", "sensor_type", "trial"];
        var optionalFields = ["geo_bounds", "time_bounds"];
        var metadata = newDoc.metadata;
        if (newDoc.metadata == undefined || newDoc.metadata == null) {
            throw ({
                forbidden: "metadata is missing in track document"
            });
        }
        if (newDoc.metadata.data_type == undefined || newDoc.metadata.data_type == null) {
            throw ({
                forbidden: "data_type is undefined in track document"
            });
        }
        for (i in newDoc.metadata.data_type) {
            var type = newDoc.metadata.data_type[i];
            allowedFields.push(type);
        } /** check we only have the necessary fields    *     */
        for (i in newDoc) {
            if (i[0] == '_') {
                continue;
            }
            var index = allowedFields.indexOf(i);
            if (index == -1) {
                throw ({
                    forbidden: "field " + i + " isn't allowed in track"
                });
            }
            allowedFields.splice(index, 1);
        }
        if (allowedFields.length != 0) {
            throw ({
                forbidden: "field " + allowedFields[0] + " is required in track of type: " + newDoc.metadata.data_type
            });
        }
        var meta = newDoc.metadata;
        for (i in newDoc.metadata) {
            var index = metaFields.indexOf(i);
            if (index == -1) {
                index = optionalFields.indexOf(i);
                if (index == -1) {
                    throw ({
                        forbidden: "field " + i + " isn't allowed in metatata"
                    });
                }
                optionalFields.splice(index, 1);
            } else {
                metaFields.splice(index, 1);
            }
        }
        if (metaFields.length != 0) {
            throw ({
                forbidden: "field " + metaFields[0] + " is required in metaData for track of type: " + newDoc.data_type
            });
        }
        /*check geobound format satisfy ES requirements*/
        
        var chechGeoFormat = function(data, dataType, type){
        	if(data){
        	    if(!(data.type === type && 
        			Object.prototype.toString.call( data.coordinates ) === '[object Array]')){
        				throw({ forbidden: "field " + dataType +" has wrong format"});
                }
            }
        };
        
        chechGeoFormat(newDoc.metadata.geo_bounds, 'geo_bounds', 'envelope');
        chechGeoFormat(newDoc.location, 'location', 'MultiPoint');
       
    }
}