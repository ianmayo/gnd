{
	 "type" : "couchdb",
    "couchdb" : {
        "host" : "gnd.iriscouch.com",
        "port" : 5984,
        "db" : "tracks",
        "filter" : null
    },
    "index" : {
        "index" : "gnd2",
        "type" : "datasets",
        "bulk_size" : "100",
        "bulk_timeout" : "10ms"
    }
}