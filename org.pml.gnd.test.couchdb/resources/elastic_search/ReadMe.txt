The way of enabling 'normal' CouchDb support in ElasticSearch:
bin/plugin -install elasticsearch/elasticsearch-river-couchdb/1.1.0

But, we're relying on the extension from:
https://github.com/downloads/dadoonet/elasticsearch-river-couchdb/elasticsearch-river-couchdb-1.2.0-SNAPSHOT.zip

Once you've downloaded the zip, extract it, and put the contents into plugins/elasticsearch-river-couchdb (create dirs as necessary)

To configure a CouchDb river into ElasticSearch:
curl -d @create_river.js -XPUT http://localhost:9200/_river/gnd/_meta

Note: 
- you run the command from this resources/elastic_search folder, 
- gnd is the name of the ES index we create  (the same name is used in side the .js file)