{
        "query" : 
        {
        	"bool" : 
        		{
        			"must" : 
        				[
	        				{
	        				    "range" : {
	        				        "start" : { 
	        				            "lte" : "2012-03-14T11:40:00+0000" 
	        				        }
	        				    }
	        				},
	        				{
	        				    "range" : {
	        				        "end" : { 
	        				            "gte" : "2012-03-14T00:00:00+0000" 
	        				        }
	        				    }
	        				}
        				]
        		}
        }
}
