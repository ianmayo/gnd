{
	"size" : 0,
    "query" : { "match_all" : {} },

    "facets" : {
        "platform_type" : { 
           "terms" : {
              "field" : "platform_type", "size" : 10000
              } 
         },
        "platform" : { 
           "terms" : {
              "field" : "platform", "size" : 10000
              } 
         },
        "sensor" : { 
           "terms" : {
              "field" : "sensor", "size" : 10000
              } 
         },
        "sensor_type" : { 
           "terms" : {
              "field" : "sensor_type", "size" : 10000
              } 
         },
        "trial" : { 
           "terms" : {
              "field" : "trial", "size" : 10000
              } 
         },
        "type" : { 
           "terms" : {
              "field" : "type", "size" : 10000
              } 
         }
    }
}