{
  "dataset" : {
    "dynamic" : "false",
    "_source" : {
      "enabled" : false
    },
    "properties" : {
      "metadata" : {
        "properties" : {
          "data_type" : {
            "type" : "string",
            "store" : "yes"
          },
          "name" : {
            "type" : "string",
            "store" : "yes"
          },
          "platform" : {
            "type" : "string",
            "index" : "not_analyzed",
            "store" : "yes"
          },
          "platform_type" : {
            "type" : "string",
            "index" : "not_analyzed",
            "store" : "yes"
          },
          "sensor" : {
            "type" : "string",
            "index" : "not_analyzed",
            "store" : "yes"
          },
          "sensor_type" : {
            "type" : "string",
            "index" : "not_analyzed",
            "store" : "yes"
          },
          "time_bounds" : {
            "properties" : {
              "end" : {
                "type" : "date",
                "store" : "yes",
                "format" : "dateOptionalTime"
              },
              "start" : {
                "type" : "date",
                "store" : "yes",
                "format" : "dateOptionalTime"
              }
            }
          },
          "trial" : {
            "type" : "string",
            "index" : "not_analyzed",
            "store" : "yes"
          },
          "type" : {
            "type" : "string",
            "store" : "yes"
          }
        }
      }
    }
  }
}