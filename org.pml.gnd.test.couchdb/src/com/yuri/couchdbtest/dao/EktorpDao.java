package com.yuri.couchdbtest.dao;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.node.TextNode;
import org.ektorp.BulkDeleteDocument;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.ViewQuery;
import org.ektorp.ViewResult;
import org.ektorp.ViewResult.Row;
import org.ektorp.http.HttpClient;
import org.ektorp.http.HttpResponse;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbInstance;

public class EktorpDao implements TestDao
{

	private HttpClient httpClient;
	private CouchDbInstance dbInstance;
	private CouchDbConnector db;

	private void updateResourceJS(JsonNode node)
	{
		Map<String, String> updateMap = new HashMap<String, String>();
		Iterator<Entry<String, JsonNode>> childs = node.getFields();
		while (childs.hasNext())
		{
			Entry<String, JsonNode> child = childs.next();
			if ((child.getValue() instanceof TextNode)
					&& (node instanceof ObjectNode))
			{
				String data = child.getValue().asText();
				if (data.startsWith("resource_js:"))
				{
					try
					{
						List<String> lines = IOUtils.readLines(EktorpDao.class
								.getResourceAsStream(data.substring("resource_js:".length())));
						StringBuilder builder = new StringBuilder();
						for (String line : lines)
						{
							int indexComment = line.indexOf("//");
							if (indexComment != -1)
							{
								line = line.substring(0, indexComment);
							}
							builder.append(line.replace('\t', ' '));
						}
						updateMap.put(child.getKey(), builder.toString());
					}
					catch (IOException ex)
					{
						System.err.println("Can't replace resource javascript: " + data);
					}
				}
			}
			updateResourceJS(child.getValue());
		}
		for (Entry<String, String> update : updateMap.entrySet())
		{
			((ObjectNode) node).put(update.getKey(), update.getValue());
		}
	}

	public static void main(String[] args)
	{
		if (args.length != 4)
		{
			System.err.println("Need the database url, plus name of database. E.g http://127.0.0.1:5984 tracks");
			System.exit(1);
		}
		else
		{
			EktorpDao dao = new EktorpDao();
			dao.connect(args[0], args[1], args[2], args[3]);
		}
	}

	@Override
	public void connect(String url, String database, String user, String password)
	{
		try
		{
			httpClient = new StdHttpClient.Builder().url(url).username(user)
					.password(password).connectionTimeout(600000).socketTimeout(600000)
					.build();
		}
		catch (MalformedURLException ex)
		{
			throw new RuntimeException(ex);
		}
		dbInstance = new StdCouchDbInstance(httpClient);
		db = dbInstance.createConnector(database, true);
		if (db.contains("_design/tracks"))
		{
			JsonNode node = db.get(JsonNode.class, "_design/tracks");
			db.delete("_design/tracks", node.get("_rev").asText());
		}
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			JsonNode node = mapper.readValue(
					EktorpDao.class.getResourceAsStream("/tracks_design_doc.json"),
					JsonNode.class);
			updateResourceJS(node);
			db.create(node);
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
	}

	@Override
	public boolean isConnected()
	{
		return db != null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void disableValidation()
	{
		Map<String, Object> node = db.get(Map.class, "_design/tracks");
		node.remove("validate_doc_update");
		db.update(node);
	}

	@Override
	public void cleanTrackClasses()
	{
		ViewResult result = db.queryView(new ViewQuery().designDocId(
				"_design/tracks").viewName("allTrackIds"));
		for (Row row : result.getRows())
		{
			JsonNode node = row.getKeyAsNode();
			db.addToBulkBuffer(BulkDeleteDocument.of(node));
		}
		db.flushBulkBuffer();
	}

	@Override
	public List<JsonNode> getAll2DWithTimeTracks(String sensor)
	{
		ViewQuery query = new ViewQuery().designDocId("_design/tracks").viewName(
				"2dTracks");
		if (sensor != null)
		{
			query.key(sensor);
		}
		return db.queryView(query, JsonNode.class);
	}

	@Override
	public List<JsonNode> getInGeoJson()
	{
		ViewQuery query = new ViewQuery().designDocId("_design/tracks").viewName(
				"allTrackIds");
		ViewResult result = db.queryView(query);
		ArrayList<JsonNode> geoJson = new ArrayList<JsonNode>(result.getSize());
		ObjectMapper mapper = new ObjectMapper();		
		for (Row row : result.getRows())
		{
			String id = row.getKeyAsNode().get("_id").asText();
			String show = "/" + db.getDatabaseName() + "/_design/tracks/_show/geoJson/" + id;
			HttpResponse response = httpClient.get(show);
			try
			{
				if (response.getCode() < 300) {
					JsonNode node = mapper.readValue(
							response.getContent(),
							JsonNode.class
							);
					geoJson.add(node);
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			} finally {
				response.releaseConnection();
			}			
		}
		return geoJson;
	}

	@Override
	public void save(Map<String, Object> document)
	{
		db.create(document);
	}

	@Override
	public void close()
	{
		httpClient.shutdown();
	}

	@Override
	public void bulkSave(ArrayList<Map<String, Object>> tracks)
	{
		db.executeBulk(tracks);
	}
}
