package com.yuri.couchdbtest.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonNode;

public interface TestDao {

	void connect(String url, String database, String user, String password);
	
	boolean isConnected();
	
	void disableValidation();
	
	void cleanTrackClasses();
	
	List<JsonNode> getAll2DWithTimeTracks(String sensorName);
	
	List<JsonNode> getInGeoJson();
	
//	List<ODocument> getTrackMetadata(Date from, Date to, String... platforms);

	void save(Map<String, Object> document);
	
	void close();

	void bulkSave(ArrayList<Map<String, Object>> tracks);
}
