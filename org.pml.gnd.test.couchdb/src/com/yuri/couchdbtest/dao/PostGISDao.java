package com.yuri.couchdbtest.dao;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonNode;

public class PostGISDao implements TestDao
{
	private java.sql.Connection conn;

	@Override
	public void connect(String url, String database, String user, String password)
	{
		// String url = "jdbc:postgresql://localhost:5432/postgis";
		try
		{
			conn = DriverManager.getConnection(url, user, password);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public boolean isConnected()
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void disableValidation()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void cleanTrackClasses()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public List<JsonNode> getAll2DWithTimeTracks(String sensorName)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<JsonNode> getInGeoJson()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Map<String, Object> document)
	{
		if (conn == null)
			return;

		// parse the document, to retrieve the positions.
		String name = (String) document.get("name");
		@SuppressWarnings("unchecked")
		ArrayList<Double> latArr = (ArrayList<Double>) document.get("lat");
		@SuppressWarnings("unchecked")
		ArrayList<Double> lonArr = (ArrayList<Double>) document.get("lon");
		String theQuery = "insert into tracks (id, track, lat, lon) VALUES (?, ?,?,?);";
		PreparedStatement st;
		try
		{
			st = conn.prepareStatement(theQuery);

			for (int i = 0; i < latArr.size(); i++)
			{
				final String id = name + "_" + i + "_" + System.currentTimeMillis();
				
				double lat = latArr.get(i);
				double lon = lonArr.get(i);
				st.setString(1, id);
				st.setString(2, name);
				st.setFloat(3, (float) lat);
				st.setFloat(4, (float) lon);
				st.executeUpdate();
			}

			st.close();
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void close()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void bulkSave(ArrayList<Map<String, Object>> tracks)
	{
		// TODO Auto-generated method stub

	}

}
