package com.yuri.couchdbtest;

import static com.yuri.couchdbtest.BasicTestUtils.addConnectionOptions;
import static com.yuri.couchdbtest.BasicTestUtils.checkConnectionOptionsPresence;
import static com.yuri.couchdbtest.BasicTestUtils.connectWithCommandLine;
import static com.yuri.couchdbtest.utils.MapUtils.put;
import static com.yuri.couchdbtest.utils.MapUtils.putToMetadata;
import static com.yuri.couchdbtest.utils.MapUtils.remove;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.ektorp.DbAccessException;

import com.yuri.couchdbtest.dao.EktorpDao;
import com.yuri.couchdbtest.dao.TestDao;
import com.yuri.couchdbtest.utils.HierarchicalMap;

public class ValidationTest
{

	private static Options createOptions()
	{
		Options options = new Options();
		addConnectionOptions(options);
		return options;
	}

	private static void printHelp(Options options)
	{
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("java -jar validation.jar <options>", options);
		System.out.println("Examples: ");
		System.out
				.println(" java -jar validation.jar -url http://localhost:5984/ -d temp -u admin -p admin");
	}

	private static void printMode(String url, String database)
	{
		System.out.println("Test parameters:");
		System.out.println(" Url       : " + url);
		System.out.println(" Database  : " + database);
		System.out.println("---------------------------");
	}

	private static void doTest(TestDao mode)
	{
		mode.cleanTrackClasses();

		HierarchicalMap track = new HierarchicalMap();

		track.putHierarchical("metadata.time_bounds.start", new Date());
		track.putHierarchical("metadata.time_bounds.end", new Date());
		track.putHierarchical("metadata.type", "track");
		track.putHierarchical("metadata.data_type", Arrays.asList("lat", "lon", "time"));
		track.putHierarchical("metadata.name", "track1");
		track.putHierarchical("metadata.platform", "platfrom");
		track.putHierarchical("metadata.platform_type", "platform_type");
		track.putHierarchical("metadata.sensor", "sensor");
		track.putHierarchical("metadata.sensor_type", "sensor_type");
		track.putHierarchical("metadata.trial", "aaaa");
		track.putHierarchical("lat", Arrays.asList(1, 2, 3));
		track.putHierarchical("lon", Arrays.asList(4, 5, 6));
		track.putHierarchical("time", Arrays.asList(11, 12, 13));

		// try to create track which doesn't have required metadata attributes
		String[] metadataFields =
		{ "data_type", "name", "platform", "platform_type", "sensor", "sensor_type",
				"trial"};
		for (String metadataField : metadataFields)
		{
			Map<String, Object> badTrack = new HashMap<String, Object>(track);
			
			// take a deep copy of the metadata object			
			@SuppressWarnings("unchecked")
			Map<String, Object> meta = new HashMap<String, Object>(
					(Map<String, Object>) badTrack.get("metadata"));
			
			// now store this metadata object in our working track
			badTrack.put("metadata", meta);
			
			// and remove the current metadata field
			meta.remove(metadataField);
			try
			{
				mode.save(badTrack);
				throw new RuntimeException("Track without " + metadataField
						+ " has been saved successfully!");
			}
			catch (DbAccessException ex)
			{
				System.out.println("Track without " + metadataField
						+ " can't be saved - OK!");
			}
		}

		// try to create track which trackType isn't array
		System.out.println();
		try
		{
			mode.save(putToMetadata(new HashMap<String, Object>(track), "data_type", "sssss"));
			throw new RuntimeException(
					"Track with bad data_type has been saved successfully!");
		}
		catch (DbAccessException ex)
		{
			System.out.println("data_type MUST be an array - OK!");
		}

		// try to create track with unsupported trackType
		System.out.println();
		try
		{
			mode.save(putToMetadata(new HashMap<String, Object>(track), "data_type",
					Arrays.asList("lat", "lon", "iii")));
			throw new RuntimeException(
					"Track with data_type=['lat', 'lon', 'iii'] has been saved successfully!");
		}
		catch (DbAccessException ex)
		{
			System.out.println("data_type MUST contain only allowed types - OK!");
		}

		// try to create track which doesn't have required by data_type data
		// attributes
		System.out.println();
		String[] time2DFields =
		{ "lat", "lon", "time" };
		for (String time2DField : time2DFields)
		{
			try
			{
				mode.save(remove(new HashMap<String, Object>(track), time2DField));
				throw new RuntimeException("2d+time track without field "
						+ time2DFields + " has been saved successfully!");
			}
			catch (DbAccessException ex)
			{
				System.out.println("2d+time without " + time2DField
						+ " can't be saved - OK!");
			}
		}

		// try to create track which has unallowed by data_type field
		System.out.println();
		try
		{
			mode.save(put(new HashMap<String, Object>(track), "z",
					Arrays.asList(0, 8, 7)));
			throw new RuntimeException(
					"2d+time track with 'z' field has been saved successfully!");
		}
		catch (DbAccessException ex)
		{
			System.out.println("2d+time must not contain 'z' field - OK!");
		}

		// check that initial track can be saved successfully
		track.putHierarchical("metadata.data_type", Arrays.asList("lat", "lon", "time"));		
		mode.save(track);
		System.out.println();
		System.out.println("Initial track was saved successfully - OK!");
	}

	public static void main(String[] args)
	{
		Options options = createOptions();
		CommandLineParser parser = new GnuParser();
		TestDao mode = null;
		try
		{
			CommandLine commandLine = parser.parse(options, args);
			if (!checkConnectionOptionsPresence(commandLine))
			{
				printHelp(options);
				return;
			}
			mode = connectWithCommandLine(new EktorpDao(), commandLine);
			printMode(commandLine.getOptionValue("url"),
					commandLine.getOptionValue("d"));
			doTest(mode);
		}
		catch (ParseException ex)
		{
			printHelp(options);
		}
		finally
		{
			if (mode != null && mode.isConnected())
			{
				mode.cleanTrackClasses();
				mode.close();
			}
		}
	}

}
