package com.yuri.couchdbtest.track;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.yuri.couchdbtest.utils.HierarchicalMap;

public class Track2DGenerator implements TrackAttributesGenerator
{

	protected Random random = new Random(System.currentTimeMillis());

	@Override
	public void fillDocument(HierarchicalMap document, int points)
	{
		List<Double> lat = new ArrayList<Double>(points);
		List<Double> lon = new ArrayList<Double>(points);
		
		double minLat=0, minLong=0, maxLat=0, maxLong=0;
		
		for (int i = 0; i < points; i++)
		{
			double latVal = 49d + random.nextDouble() * (1 + random.nextInt(9));
			double lonVal = -5d - random.nextDouble() * (1 + random.nextInt(3));

			if(i==0)
			{
				minLat = maxLat = latVal;
				minLong = maxLong = lonVal;
			}
			else
			{
				minLat = Math.min(minLat, latVal);
				maxLat = Math.max(maxLat, latVal);
				minLong = Math.min(minLong,  lonVal);
				maxLong = Math.max(maxLong,  lonVal);
			}
			
			lat.add(latVal);
			lon.add(lonVal);

		}
		document.putHierarchical("lat", lat);
		document.putHierarchical("lon", lon);
		TrackUtils.extendTrackType(document, "lat", "lon");
		
		// and write the metadata
		List<Double> tl = new ArrayList<Double>(2);
		List<Double> br = new ArrayList<Double>(2);
		tl.add(minLong);
		tl.add(maxLat);
		br.add(maxLong);
		br.add(minLat);
		
		document.putHierarchical("metadata.geo_bounds.tl", tl);
		document.putHierarchical("metadata.geo_bounds.br", br);
		
	}
}
