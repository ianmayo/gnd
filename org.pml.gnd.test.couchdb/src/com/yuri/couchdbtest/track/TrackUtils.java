package com.yuri.couchdbtest.track;

import java.util.HashSet;
import java.util.Set;

import com.yuri.couchdbtest.utils.HierarchicalMap;

public class TrackUtils {

	@SuppressWarnings("unchecked")
	public static void extendTrackType(HierarchicalMap document, String... newTypes) {
		if (newTypes == null || newTypes.length == 0) {
			return;
		}
		Set<String> types = (Set<String>) document.getHierarchial("metadata.data_type");
		if (types == null) {
			types = new HashSet<String>();			
		}
		for (String newType : newTypes) {			
			types.add(newType);
		}
		document.putHierarchical("metadata.data_type", types);
	}
}
