package com.yuri.couchdbtest.track;

import com.yuri.couchdbtest.utils.HierarchicalMap;

public interface TrackAttributesGenerator {
	
	void fillDocument(HierarchicalMap document, int points);
}
