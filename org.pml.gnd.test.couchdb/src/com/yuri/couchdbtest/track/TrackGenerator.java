package com.yuri.couchdbtest.track;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.yuri.couchdbtest.utils.HierarchicalMap;

public class TrackGenerator {

	protected final List<TrackAttributesGenerator> attributesGenerators;

	public TrackGenerator(List<TrackAttributesGenerator> attributesGenerators) {
		this.attributesGenerators = attributesGenerators;
	}

	public TrackGenerator(TrackAttributesGenerator... attributesGenerators) {
		this(Arrays.asList(attributesGenerators));
	}	

	public Map<String, Object> generate(int points) {
		HierarchicalMap document = new HierarchicalMap();
		document.putHierarchical("metadata.type", "track");
		for (TrackAttributesGenerator generator : attributesGenerators) {
			generator.fillDocument(document, points);
		}		
		return document;
	}
}
