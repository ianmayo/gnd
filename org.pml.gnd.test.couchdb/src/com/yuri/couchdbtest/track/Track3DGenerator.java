package com.yuri.couchdbtest.track;

import java.util.ArrayList;
import java.util.List;

import com.yuri.couchdbtest.utils.HierarchicalMap;

public class Track3DGenerator extends Track2DGenerator
{

	@Override
	public void fillDocument(HierarchicalMap document, int points)
	{
		super.fillDocument(document, points);
		List<Double> z = new ArrayList<Double>(points);
		for (int i = 0; i < points; i++)
		{
			z.add(random.nextDouble() * random.nextInt(10000));
		}
		document.putHierarchical("z", z);
		TrackUtils.extendTrackType(document, "z");
	}

}
