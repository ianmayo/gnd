package com.yuri.couchdbtest.track;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.yuri.couchdbtest.utils.HierarchicalMap;

public class CommontAttributeGenerator implements TrackAttributesGenerator
{

	private int count = 1;

	private Random rnd = new Random();

	private List<String> platforms;

	private List<String> platformTypes;

	private List<String> sensors;

	private List<String> sensorTypes;

	private List<String> trials;

	public CommontAttributeGenerator()
	{
		platforms = genFor("plat", 20);
		platformTypes = genFor("platformType", 10);
		sensors = genFor("sensor", 40);
		sensorTypes = genFor("sensorType", 20);
		trials = genFor("trial", 40);
	}

	private final List<String> genFor(String leader, int num)
	{
		final List<String> list = new ArrayList<String>();
		for (int i = 0; i < num; i++)
		{
			list.add(leader + "_" + i);
		}
		return list;
	}

	@Override
	public void fillDocument(HierarchicalMap document, int points)
	{
		document.putHierarchical("metadata.name", "track_" + count++);
		document.putHierarchical("metadata.platform", randFrom(platforms));
		document.putHierarchical("metadata.platform_type", randFrom(platformTypes));
		document.putHierarchical("metadata.sensor", randFrom(sensors));
		document.putHierarchical("metadata.sensor_type", randFrom(sensorTypes));
		document.putHierarchical("metadata.trial", randFrom(trials));
	}

	private String randFrom(List<String> list)
	{
		return list.get(rnd.nextInt(list.size()));
	}
}
