package com.yuri.couchdbtest.track;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import com.yuri.couchdbtest.utils.HierarchicalMap;

public class TimeStampedTrackGenerator implements TrackAttributesGenerator
{

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ",
			Locale.UK);

	@Override
	public void fillDocument(HierarchicalMap document, int points)
	{
		Random random = new Random(System.currentTimeMillis());
		List<String> time = new ArrayList<String>(points);
		long startTime = (System.currentTimeMillis() + (long)random.nextInt(1000 * 60 * 60 * 24));
		String startStr=null, endStr=null;
		for (int i = 0; i < points; i++)
		{
			String dateStr = sdf.format(new Date(startTime));
			time.add(dateStr);
			startTime += random.nextInt(1000 * 60 * 30);
			
			if(startStr == null)
				startStr = dateStr;
			endStr = dateStr;
		}
		document.putHierarchical("time", time);
		TrackUtils.extendTrackType(document, "time");
		
		// also stick in the metadata
		document.putHierarchical("metadata.time_bounds.start", startStr);
		document.putHierarchical("metadata.time_bounds.end", endStr);
	}

}
