package com.yuri.couchdbtest.utils;

import java.util.Map;

public class MapUtils {
	
	public static <F, S> Map<F, S> put(Map<F, S> map, F key, S value) {
		map.put(key, value);
		return map;
	}
	
	public static Map<String, Object> putToMetadata(Map<String, Object> object, String key, Object value) {
		Object metadata = object.get("metadata");
		if (metadata != null && (metadata instanceof Map)) {			
			@SuppressWarnings("unchecked")
			Map<String, Object> metadataMap = (Map<String, Object>) metadata;
			metadataMap.put(key, value);			
		}
		return object;
	}	
	
	public static <F, S> Map<F, S> remove(Map<F, S> map, F key) {
		map.remove(key);
		return map;
	}
}
