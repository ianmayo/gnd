package com.yuri.couchdbtest.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

@SuppressWarnings("unchecked")
public class HierarchicalMap extends LinkedHashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	private boolean exists = false;
	
	private Object getFromMap(Map<String, Object> map, String key, boolean fillExists) {
		if (key.matches("[\\d+]$")) {
			int index = key.lastIndexOf('[');
			int arrayIndex = Integer.parseInt(key.substring(index + 1, key.length() - 1));
			key = key.substring(0, index);
			ArrayList<Object> list = (ArrayList<Object>) map.get(key);
			if (list == null || list.size() <= arrayIndex) {
				return null;
			}
			if (fillExists) {
				exists = true;
			}
			return list.get(arrayIndex);
		}
		if (fillExists) {
			exists = map.containsKey(key);
		}
		return map.get(key);
	}
	
	private Object putToMap(Map<String, Object> map, String key, boolean last, Object value) {
		if (! last) {
			value = new LinkedHashMap<String, Object>();
		}
		if (key.matches(".*\\[\\d+\\]")) {
			int index = key.lastIndexOf('[');
			int arrayIndex = Integer.parseInt(key.substring(index + 1, key.length() - 1));
			key = key.substring(0, index);
			ArrayList<Object> list = (ArrayList<Object>) map.get(key);
			if (list == null) {
				list = new ArrayList<Object>();
				map.put(key, list);
			}
			while (list.size() <= arrayIndex) {
				list.add(null);
			}
			Object result = last ? list.get(arrayIndex) : value;
			list.set(arrayIndex, value);
			return result;
		}
		Object result;
		if (! last && map.containsKey(key)) {
			result = map.get(key);
			if (result instanceof Map) {
				return result;
			}
		}
		result = map.put(key, value);
		return last ? result : value;
	}	
	
	public Object getHierarchial(Object key) {
		if (! (key instanceof String)) {
			return null;
		}
		exists = false;
		String[] parts = ((String) key).split("\\.");
		Map<String, Object> next = this;
		if (parts.length > 1) {
			for (int i = 0; i < parts.length - 1; i++) {
				next = (Map<String, Object>) getFromMap(next, parts[i], false);
				if (next == null) {
					return null;
				}
			}
		} else {
			parts = new String[] {(String) key};
		}
		return getFromMap(next, parts[parts.length - 1], true);
	}

	public boolean containsKeyHierarchial(Object key) {
		if (! (key instanceof String)) {
			return false;
		}
		getHierarchial(key);
		return exists;
	}

	public Object putHierarchical(String key, Object value) {
		if (! (key instanceof String)) {
			throw new IllegalArgumentException();
		}
		String[] parts = ((String) key).split("\\.");
		Map<String, Object> next = this;
		if (parts.length > 1) {
			for (int i = 0; i < parts.length - 1; i++) {
				next = (Map<String, Object>) putToMap(next, parts[i], false, null);
			}
		} else {
			parts = new String[] {(String) key};
		}
		return putToMap(next, parts[parts.length - 1], true, value);
	}
	
	public static void main(String[] args) {
		HierarchicalMap map = new HierarchicalMap();
		map.putHierarchical("x", "2");
		map.putHierarchical("metadata.y", "1");
		map.putHierarchical("metadata.z[0]", "3");
		map.putHierarchical("metadata.u.i", "4");
	}
}
