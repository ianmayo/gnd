package com.yuri.couchdbtest.utils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Set;

public class TimeProxy implements InvocationHandler {
	private Object object;
	private Set<String> computeMethods;

	private TimeProxy(Object object, Set<String> computeMethods) {
		this.object = object;
		this.computeMethods = computeMethods;
	}
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		if (computeMethods != null && ! computeMethods.contains(method.getName())) {
			return method.invoke(object, args);
		}
		long timeMark = System.currentTimeMillis();
		Object result = method.invoke(object, args);
		System.out.println(object.getClass().getSimpleName() + "." + method.getName() + " takes: " + (System.currentTimeMillis() - timeMark));
		System.out.println("---------------------------");
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static <I, T extends I> I timeProxy(Class<I> i, T impl, Set<String> computeMethods) {
		return (I) Proxy.newProxyInstance(
				Thread.currentThread().getContextClassLoader(),
				new Class<?>[] {i}, 
				new TimeProxy(impl, computeMethods)
		);
	}
	
	public static <I, T extends I> I timeProxy(Class<I> i, T impl) {
		return timeProxy(i, impl, null);
	}
}