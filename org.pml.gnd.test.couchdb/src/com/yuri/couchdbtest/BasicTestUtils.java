package com.yuri.couchdbtest;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import com.yuri.couchdbtest.dao.TestDao;
import com.yuri.couchdbtest.track.CommontAttributeGenerator;
import com.yuri.couchdbtest.track.TimeStampedTrackGenerator;
import com.yuri.couchdbtest.track.Track2DGenerator;
import com.yuri.couchdbtest.track.Track3DGenerator;
import com.yuri.couchdbtest.track.TrackAttributesGenerator;


public class BasicTestUtils {
	@SuppressWarnings("static-access")
	public static void addConnectionOptions(Options options) {
		options.addOption(OptionBuilder
				.withArgName("db url")
				.hasArg()
				.withDescription("couchdb url")
			    .create("url")
		);
		options.addOption(OptionBuilder
				.withArgName("database name")
				.hasArg()
				.withDescription("database name")
			    .create("d")
		);
		options.addOption(OptionBuilder
				.withArgName("username")
				.hasArg()
				.withDescription("username which is used to connect to the db")
				.create("u")
		);
		options.addOption(OptionBuilder
				.withArgName("password")
				.hasArg()
				.withDescription("password which is used to connect to the db")
				.create("p")
		);
	}
	
	public static boolean checkConnectionOptionsPresence(CommandLine commandLine) {
		return commandLine.hasOption("url") && commandLine.hasOption("d");
	}
	
	public static TestDao connectWithCommandLine(TestDao testMode, CommandLine commandLine) {
		testMode.connect(
				commandLine.getOptionValue("url"),
				commandLine.getOptionValue("d"), 
				commandLine.getOptionValue("u"),
				commandLine.getOptionValue("p")
		);
		return testMode;
	}
	
	public static List<TrackAttributesGenerator> parseGenerators(String trackType) {
		ArrayList<TrackAttributesGenerator> generators = new ArrayList<TrackAttributesGenerator>();
		generators.add(new CommontAttributeGenerator());
		String[] types = trackType.split(",");
		for (String type : types) {
			type = type.trim().toLowerCase();
			if (type.equals("2d")) {
				generators.add(new Track2DGenerator());
			}
			if (type.equals("3d")) {
				generators.add(new Track3DGenerator());
			}
			if (type.equals("time")) {
				generators.add(new TimeStampedTrackGenerator());
			}
		}
		return generators;

	}

}
