package com.yuri.couchdbtest;

import static com.yuri.couchdbtest.BasicTestUtils.addConnectionOptions;
import static com.yuri.couchdbtest.BasicTestUtils.checkConnectionOptionsPresence;
import static com.yuri.couchdbtest.BasicTestUtils.connectWithCommandLine;
import static com.yuri.couchdbtest.utils.MapUtils.putToMetadata;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.codehaus.jackson.JsonNode;

import com.yuri.couchdbtest.dao.EktorpDao;
import com.yuri.couchdbtest.dao.TestDao;
import com.yuri.couchdbtest.track.CommontAttributeGenerator;
import com.yuri.couchdbtest.track.Track2DGenerator;
import com.yuri.couchdbtest.track.Track3DGenerator;
import com.yuri.couchdbtest.track.TimeStampedTrackGenerator;
import com.yuri.couchdbtest.track.TrackGenerator;

public class HierarchialSchemaTest {
	
	private static Options createOptions() {
		Options options = new Options();
		addConnectionOptions(options);
		return options;
	}
	
	private static void printHelp(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("java -jar hierarchialschema.jar <options>", options);
		System.out.println("Examples: ");
		System.out.println(" java -jar hierarchialschema.jar -url http://localhost:5984/ -d temp -u admin -p admin");
	}	
	
	private static void printMode(String url, String database) {
		System.out.println("Test parameters:");
		System.out.println(" Url       : " + url);
		System.out.println(" Database  : " + database);
		System.out.println("---------------------------");
	}
	
	private static void doTest(TestDao mode) {
		mode.cleanTrackClasses();
		
		CommontAttributeGenerator commonsGenerator = new CommontAttributeGenerator();
		// generate three 2d+time tracks
		TrackGenerator generator = new TrackGenerator(commonsGenerator, new Track2DGenerator(), new TimeStampedTrackGenerator());
		mode.save(putToMetadata(generator.generate(2), "sensor", "sensor 1"));
		mode.save(putToMetadata(generator.generate(3),  "sensor", "sensor 2"));
		mode.save(putToMetadata(generator.generate(12),  "sensor", "sensor 3"));
		//generate one time track
		generator = new TrackGenerator(commonsGenerator, new TimeStampedTrackGenerator());
		mode.save(putToMetadata(generator.generate(20),  "sensor", "sensor 1"));
		// generate two 3d+time tracks
		generator = new TrackGenerator(commonsGenerator, new TimeStampedTrackGenerator(), new Track3DGenerator());
		mode.save(putToMetadata(generator.generate(10),  "sensor", "sensor 2"));
		mode.save(putToMetadata(generator.generate(15),  "sensor", "sensor 1"));
		
		Set<String> expected = new HashSet<String>(Arrays.asList("Track 1", "Track 2", "Track 3", "Track 5", "Track 6"));
		System.out.println("Searching for all 2d tracks");
		List<JsonNode> documents = mode.getAll2DWithTimeTracks(null);
		if (expected.size() != documents.size()) {
			throw new RuntimeException("5 2d+time tracks can't be found, but found: " + documents);
		}
		for (JsonNode document : documents) {
			if (! expected.contains(document.get("name").asText())) {
				throw new RuntimeException(document + " is in result set but isn't expected");				
			}			
			if (document.get("z") != null) {
				throw new RuntimeException(document + ": z attribute can't be included to result track");
			}
			expected.remove(document.get("name").asText());
		}
		if (expected.size() != 0) {
			throw new RuntimeException("5 2d+time tracks can't be found, but found: " + documents);
		}
		System.out.println("Result 2d tracks: ");
		for (JsonNode document : documents) {
			System.out.println(document);
		}
		System.out.println("--------------------------------------------------");
		
		System.out.println("Searching for 2d tracks with sensor = 'sensor 1'");
		documents = mode.getAll2DWithTimeTracks("sensor 1");
		if (documents.size() != 2) {
			throw new RuntimeException("2 2d+time tracks with sensor='sensor 1' can't be found, but found: " + documents);
		}
		System.out.println("Result 2d tracks with sensor='sensor 1': ");
		for (JsonNode document : documents) {
			System.out.println(document);
		}
		System.out.println("--------------------------------------------------");
	}
	
	public static void main(String[] args) {
		Options options = createOptions();
		CommandLineParser parser = new GnuParser();
		TestDao mode = null;
		try {
			CommandLine commandLine = parser.parse(options, args);
			if (! checkConnectionOptionsPresence(commandLine)) {
				printHelp(options);
				return;
			}
			mode = connectWithCommandLine(new EktorpDao(), commandLine);
			printMode(commandLine.getOptionValue("url"), commandLine.getOptionValue("d"));
			doTest(mode);
		} catch (ParseException ex) {
			printHelp(options);
		} finally {
			if (mode != null && mode.isConnected()) {
				mode.cleanTrackClasses();
				mode.close();
			}			
		}
	}

}
