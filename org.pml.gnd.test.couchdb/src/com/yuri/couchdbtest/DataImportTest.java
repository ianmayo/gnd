package com.yuri.couchdbtest;

import static com.yuri.couchdbtest.BasicTestUtils.addConnectionOptions;
import static com.yuri.couchdbtest.BasicTestUtils.checkConnectionOptionsPresence;
import static com.yuri.couchdbtest.BasicTestUtils.connectWithCommandLine;
import static com.yuri.couchdbtest.BasicTestUtils.parseGenerators;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.yuri.couchdbtest.dao.EktorpDao;
import com.yuri.couchdbtest.dao.PostGISDao;
import com.yuri.couchdbtest.dao.TestDao;
import com.yuri.couchdbtest.track.TrackAttributesGenerator;
import com.yuri.couchdbtest.track.TrackGenerator;
import com.yuri.couchdbtest.utils.TimeProxy;

public class DataImportTest
{

	@SuppressWarnings("static-access")
	private static Options createOptions()
	{
		Options options = new Options();
		addConnectionOptions(options);
		options
				.addOption(OptionBuilder
						.withArgName("strategy")
						.hasArg()
						.withDescription(
								"generatefirst | generateandsave (default: generateandsave, in generatefirst "
										+ "strategy the system generates tracks into list and computes pure saving time)")
						.create("s"));
		options.addOption(OptionBuilder.withArgName("number of tracks").hasArg()
				.create("tn"));
		options.addOption(OptionBuilder.withArgName("number of points in track")
				.hasArg().create("tp"));
		options.addOption(OptionBuilder
				.withArgName("track type")
				.hasArg()
				.withDescription(
						"can be composition of following types: '2d', '3d', 'time' ")
				.create("tt"));
		options.addOption(OptionBuilder
				.withArgName("database type")
				.hasArg()
				.withDescription(
						"can be composition of following types: 'ektorp', 'postgres'")
				.create("db"));
		options.addOption(OptionBuilder.withDescription(
				"disable validation on tracks import").create("dv"));
		options.addOption(OptionBuilder.withDescription(
				"perform bulk-save operation").create("bs"));
		return options;
	}

	private static void printHelp(Options options)
	{
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("java -jar testimport.jar <options>", options);
		System.out.println("Examples: ");
		System.out
				.println(" java -jar testimport.jar -url http://127.0.0.1:5984 -d tracks -u admin -p admin -tn 1000 -tp 1000 -tt 2d");
		System.out
				.println(" java -jar testimport.jar -url http://127.0.0.1:5984 -d temp -u admin -p admin -tn 100 -tp 10000 -tt 3d,time");
		System.out
				.println(" java -jar testimport.jar -url http://127.0.0.1:5984 -d temp -u admin -p admin -tn 100 -s generatefirst -tp 5000 -tt 2d,time");
	}

	private static void printMode(ImportStrategy strategy, String url,
			String backend, String database, int tracksNumber, int pointsNumber,
			String trackType, boolean validation, boolean bulkSave)
	{
		System.out.println("Test parameters:");
		System.out.println(" Strategy   : " + strategy.getName());
		System.out.println(" Url        : " + url);
		System.out.println(" Backend    : " + backend);
		System.out.println(" Database   : " + database);
		System.out.println(" Track type : " + trackType);
		System.out.println(" Tracks num : " + tracksNumber);
		System.out.println(" Points num : " + pointsNumber);
		System.out.println(" Validation : " + validation);
		System.out.println(" Bulk-Save  : " + bulkSave);
		System.out.println("---------------------------");
	}

	public static void main(String[] args) throws Exception
	{
		Options options = createOptions();
		CommandLineParser parser = new GnuParser();
		try
		{
			CommandLine commandLine = parser.parse(options, args);
			if (!checkConnectionOptionsPresence(commandLine)
					|| !commandLine.hasOption("tn") || !commandLine.hasOption("tp")
					|| !commandLine.hasOption("tt"))
			{
				printHelp(options);
				return;
			}
			TestDao testDao;
			if (commandLine.hasOption("db") && commandLine.getOptionValue("db").toLowerCase().equals("postgres"))
			{
				testDao = new PostGISDao();
			}
			else
			{
				testDao = new EktorpDao();
			}

			ImportStrategy strategy;
			if (commandLine.hasOption("s")
					&& commandLine.getOptionValue("s").toLowerCase()
							.equals("generatefirst"))
			{
				strategy = new GenerateFirstStrategy();
			}
			else
			{
				strategy = new GenerateInSaveLoopStrategy();
			}

			final boolean bulkSave;
			if (commandLine.hasOption("bs"))
			{
				bulkSave = true;
			}
			else
			{
				bulkSave = false;
			}

			List<TrackAttributesGenerator> trackAttributesGenerator = parseGenerators(commandLine
					.getOptionValue("tt"));
			int tracksNumber = Integer.parseInt(commandLine.getOptionValue("tn"));
			int pointsNumber = Integer.parseInt(commandLine.getOptionValue("tp"));
			printMode(strategy, commandLine.getOptionValue("url"),
					commandLine.getOptionValue("db"), commandLine.getOptionValue("d"),
					tracksNumber, pointsNumber, commandLine.getOptionValue("tt"),
					!commandLine.hasOption("dv"), bulkSave);

			testDao = TimeProxy.timeProxy(TestDao.class, testDao,
					new HashSet<String>(Arrays.asList("connect", "close")));
			strategy = TimeProxy.timeProxy(ImportStrategy.class, strategy);
			long timeMark = System.currentTimeMillis();
			connectWithCommandLine(testDao, commandLine);
			if (commandLine.hasOption("dv"))
			{
				testDao.disableValidation();
			}
			strategy.doTest(testDao, trackAttributesGenerator, tracksNumber,
					pointsNumber, bulkSave);
			testDao.close();
			System.out.println("Overall test takes: "
					+ (System.currentTimeMillis() - timeMark));
		}
		catch (ParseException ex)
		{
			printHelp(options);
		}
	}

	public static interface ImportStrategy
	{

		void doTest(TestDao dao,
				List<TrackAttributesGenerator> trackAttributesGenerator,
				int tracksCount, int pointsInTrack, boolean bulkSave);

		String getName();
	}

	public static class GenerateFirstStrategy implements ImportStrategy
	{

		@Override
		public String getName()
		{
			return "Generate tracks in first loop and save them in second loop";
		}

		@Override
		public void doTest(TestDao dao,
				List<TrackAttributesGenerator> trackAttributesGenerator,
				int tracksCount, int pointsInTrack, boolean bulkSave)
		{
			DecimalFormat format = new DecimalFormat("0.###");
			TrackGenerator generator = new TrackGenerator(trackAttributesGenerator);
			ArrayList<Map<String, Object>> tracks = new ArrayList<Map<String, Object>>(
					tracksCount);

			long timeMark = System.currentTimeMillis();
			for (int i = 0; i < tracksCount; i++)
			{
				tracks.add(generator.generate(pointsInTrack));
			}
			System.out.println("Generation takes: "
					+ (System.currentTimeMillis() - timeMark));

			timeMark = System.currentTimeMillis();
			if (bulkSave)
			{
				dao.bulkSave(tracks);
			}
			else
			{
				for (Map<String, Object> document : tracks)
				{
					dao.save(document);
				}
			}
			long timeResult = System.currentTimeMillis() - timeMark;
			double timePerTrack = timeResult / (double) tracksCount;
			double timePerPoint = timePerTrack / (double) pointsInTrack;
			System.out.println("Operation takes: " + timeResult);
			System.out.println("Time per track: " + Math.round(timePerTrack));
			System.out.println("Time per point: " + format.format(timePerPoint));
			System.out.println("");
		}
	}

	public static class GenerateInSaveLoopStrategy implements ImportStrategy
	{

		@Override
		public String getName()
		{
			return "Generate tracks and save them in one loop";
		}

		@Override
		public void doTest(TestDao dao,
				List<TrackAttributesGenerator> trackAttributesGenerator,
				int tracksCount, int pointsInTrack, boolean bulkSave)
		{
			DecimalFormat format = new DecimalFormat("0.###");
			TrackGenerator generator = new TrackGenerator(trackAttributesGenerator);
			long timeMark = System.currentTimeMillis();
			for (int i = 0; i < tracksCount; i++)
			{
				Map<String, Object> document = generator.generate(pointsInTrack);
				dao.save(document);
			}
			long timeResult = System.currentTimeMillis() - timeMark;
			double timePerTrack = timeResult / (double) tracksCount;
			double timePerPoint = timePerTrack / (double) pointsInTrack;
			if (bulkSave)
			{
				System.err
						.println("Warning. Bulk-save not possible for this generator strategy: "
								+ timeResult);
			}
			System.out.println("Operation takes: " + timeResult);
			System.out.println("Time per track: " + Math.round(timePerTrack));
			System.out.println("Time per point: " + format.format(timePerPoint));
			System.out.println("");
		}
	}
}
