package com.yuri.couchdbtest;

import static com.yuri.couchdbtest.BasicTestUtils.addConnectionOptions;
import static com.yuri.couchdbtest.BasicTestUtils.checkConnectionOptionsPresence;
import static com.yuri.couchdbtest.BasicTestUtils.connectWithCommandLine;

import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.codehaus.jackson.JsonNode;

import com.yuri.couchdbtest.dao.EktorpDao;
import com.yuri.couchdbtest.dao.TestDao;
import com.yuri.couchdbtest.track.CommontAttributeGenerator;
import com.yuri.couchdbtest.track.TimeStampedTrackGenerator;
import com.yuri.couchdbtest.track.Track2DGenerator;
import com.yuri.couchdbtest.track.Track3DGenerator;
import com.yuri.couchdbtest.track.TrackGenerator;

public class TransformationTest {
	
	private static Options createOptions() {
		Options options = new Options();
		addConnectionOptions(options);
		return options;
	}
	
	private static void printHelp(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("java -jar validation.jar <options>", options);
		System.out.println("Examples: ");
		System.out.println(" java -jar validation.jar -url http://localhost:5984/ -d temp -u admin -p admin");
	}	
	
	private static void printMode(String url, String database) {
		System.out.println("Test parameters:");
		System.out.println(" Url               : " + url);
		System.out.println(" Database          : " + database);
		System.out.println("---------------------------");
	}
	
	private static void doTest(TestDao mode) {
		mode.cleanTrackClasses();
		CommontAttributeGenerator common = new CommontAttributeGenerator();
		// generate 2d track
		TrackGenerator generator = new TrackGenerator(new Track2DGenerator(), common);
		mode.save(generator.generate(10));
		
		// generate 2d+time track
		generator = new TrackGenerator(new Track2DGenerator(), new TimeStampedTrackGenerator(), common);
		mode.save(generator.generate(5));
		
		// generate 3d track
		generator = new TrackGenerator(new Track3DGenerator(), common);
		mode.save(generator.generate(2));
		
		// generate 3d+time track
		generator = new TrackGenerator(new Track3DGenerator(), new TimeStampedTrackGenerator(), common);
		mode.save(generator.generate(7));
		
		// generate only timed track. we won't create geo json for it
		generator = new TrackGenerator(new TimeStampedTrackGenerator(), common);
		mode.save(generator.generate(15));
		
		List<JsonNode> result = mode.getInGeoJson();
		if (result.size() != 4) {
			throw new RuntimeException("we must have 4 track in GeoJson, but have: " + result);
		}
		for (JsonNode node : result) {
			JsonNode coords = node.get("geometry").get("coordinates");
			String trackName = node.get("properties").get("name").asText();
			if ("Track 1".equals(trackName)) {
				if (node.get("properties").has("time")) {
					throw new RuntimeException("time shouldn't exist in 2d only track");
				}				
				if (! (coords.get(0).has(0) && coords.get(0).has(1) && ! coords.get(0).has(2))) {
					throw new RuntimeException("2d only track must have only two points in coords");
				}
				System.out.println("2d only track: ");
				System.out.println(node);
				System.out.println();
			}
			if ("Track 2".equals(trackName)) {
				if (! node.get("properties").has("time")) {
					throw new RuntimeException("time must exist in 2d+time track");
				}				
				if (! (coords.get(0).has(0) && coords.get(0).has(1) && ! coords.get(0).has(2))) {
					throw new RuntimeException("2d+time track must have only two points in coords");
				}
				System.out.println("2d+time track: ");
				System.out.println(node);
				System.out.println();
			}
			if ("Track 3".equals(trackName)) {
				if (node.get("properties").has("time")) {
					throw new RuntimeException("3d only track must not have time field");
				}				
				if (! (coords.get(0).has(0) && coords.get(0).has(1) && coords.get(0).has(2))) {
					throw new RuntimeException("3d track must have three points in coords");
				}
			 	System.out.println("3d only track: ");
				System.out.println(node);
				System.out.println();
			}
			if ("Track 4".equals(trackName)) {
				if (! node.get("properties").has("time")) {
					throw new RuntimeException("3d+time track must have time field");
				}				
				if (! (coords.get(0).has(0) && coords.get(0).has(1) && coords.get(0).has(2))) {
					throw new RuntimeException("3d track must have three points in coords");
				}
			 	System.out.println("3d+time track: ");
				System.out.println(node);
				System.out.println();
			}
		}
	}
	
	public static void main(String[] args) {
		Options options = createOptions();
		CommandLineParser parser = new GnuParser();
		TestDao mode = null;
		try {
			CommandLine commandLine = parser.parse(options, args);
			if (! checkConnectionOptionsPresence(commandLine)) {
				printHelp(options);
				return;
			}
			mode = connectWithCommandLine(new EktorpDao(), commandLine);
			printMode(commandLine.getOptionValue("url"), commandLine.getOptionValue("d"));
			doTest(mode);
		} catch (ParseException ex) {
			printHelp(options);
		} finally {
			if (mode != null && mode.isConnected()) {
				mode.cleanTrackClasses();
				mode.close();
			}			
		}	
	}

}
