package org.pml.gnd.gwt.client.mvp.browse;

import java.util.ArrayList;
import java.util.List;

import org.pml.gnd.gwt.client.mvp.browse.SearchView.Listener;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;

public class BrowseViewImpl extends Composite implements BrowseView
{

	private SimplePanel centrePanel;
	private Presenter _myListener;
	private SearchViewImpl _searchPanel;

	public BrowseViewImpl()
	{

		DockPanel dockPanel = new DockPanel();
		initWidget(dockPanel);
		dockPanel.setSize("100%", "100%");

		CaptionPanel cptnpnlNewPanel = new CaptionPanel("Filter");
		dockPanel.add(cptnpnlNewPanel, DockPanel.WEST);

		FlowPanel flowPanel = new FlowPanel();
		cptnpnlNewPanel.setContentWidget(flowPanel);
		flowPanel.setSize("100%", "100%");

		_searchPanel = new SearchViewImpl();
		flowPanel.add(_searchPanel);

		CaptionPanel cptnpnlNewPanel_1 = new CaptionPanel("Results");
		dockPanel.add(cptnpnlNewPanel_1, DockPanel.CENTER);
		cptnpnlNewPanel_1.setSize("100%", "100%");

		centrePanel = new SimplePanel();
		cptnpnlNewPanel_1.setContentWidget(centrePanel);
		centrePanel.setSize("100%", "100%");

		CaptionPanel cptnpnlNewPanel_2 = new CaptionPanel("Basket");
		dockPanel.add(cptnpnlNewPanel_2, DockPanel.EAST);

		FlowPanel flowPanel_1 = new FlowPanel();
		cptnpnlNewPanel_2.setContentWidget(flowPanel_1);
		flowPanel_1.setSize("100%", "100%");

		CellList<String> cellList = new CellList<String>(new AbstractCell<String>()
		{
			@Override
			public void render(Context context, String value, SafeHtmlBuilder sb)
			{
				sb.appendEscaped(value);
			}
		});
		flowPanel_1.add(cellList);

		// populate with some sample data
		List<String> values = new ArrayList<String>();
		values.add("Track 23");
		values.add("Track 23a");
		values.add("Track 23v");
		values.add("Track 252");
		cellList.setRowData(values);

		Button operateBtn = new Button("Operate");
		flowPanel_1.add(operateBtn);
		operateBtn.addClickHandler(new ClickHandler()
		{
			@Override
			public void onClick(ClickEvent event)
			{
				if (_myListener != null)
					_myListener.operateBasket();
			}
		});

	}

	@Override
	public void setPresenter(Presenter listener)
	{
		_myListener = listener;

		_searchPanel.setListener(new Listener()
		{

			@Override
			public void search()
			{
				_myListener.search(_searchPanel);
			}

			@Override
			public void reset()
			{
				_myListener.reset();
			}
		});
	}
	
	@Override
	public SearchView getSearchView()
	{
		return _searchPanel;
	}

	@Override
	public void setCentre(IsWidget panel)
	{
		centrePanel.setWidget(panel);
	}

}
