package org.pml.gnd.gwt.client.mvp.browse.info;

import org.pml.gnd.gwt.client.to.Dataset;

import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class LocalInfoModelImpl implements InfoModel
{

	@Override
	public void getDataset(String id, final AsyncCallback<Dataset> callback)
	{
		RequestCallback request = new RequestCallback()
		{
			public void onError(Request request, Throwable exception)
			{
			}

			public void onResponseReceived(Request request, Response response)
			{
				if (200 == response.getStatusCode())
				{
					// ok, now we have to process them...
					final Dataset result = JsonUtils
							.unsafeEval(response.getText());
					
					callback.onSuccess(result);

				}
				else
				{
					callback.onFailure(new Throwable("Couldn't retrieve JSON (" + response.getStatusText()
							+ ")"));
				}
			}
		};		
		// is this a fancy id that includes an underscore version number?
		if(id.contains("_"))
			id = id.split("_")[0];
		
		String url = "org.pml.gnd.gwt.Gnd/test_data/full_" + id + ".js";
		// Send request to server and catch any errors.
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, url);

		try
		{
			builder.sendRequest(null, request);
		}
		catch (RequestException e)
		{
			System.err.println("ERROR:" + e.getMessage());
		}
	}

}
