package org.pml.gnd.gwt.client.mvp.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

//public class HelloPlace extends ActivityPlace<HelloActivity>
public class WelcomePlace extends Place
{
	public WelcomePlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<WelcomePlace>
	{
		final static String NAME="WELCOME";

		@Override
		public String getToken(WelcomePlace place)
		{
			return NAME;
		}

		@Override
		public WelcomePlace getPlace(String token)
		{
			return new WelcomePlace();
		}

	}
}
