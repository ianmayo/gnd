/**
 * 
 */
package org.pml.gnd.gwt.client;

import org.pml.gnd.gwt.client.mvp.AppActivityMapper;
import org.pml.gnd.gwt.client.mvp.AppPlaceHistoryMapper;
import org.pml.gnd.gwt.client.mvp.ClientFactory;
import org.pml.gnd.gwt.client.mvp.ClientFactoryImpl;
import org.pml.gnd.gwt.client.mvp.place.WelcomePlace;
import org.pml.gnd.gwt.client.ui.HomePageUI;

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.web.bindery.event.shared.EventBus;

/**
 * @author ian
 * 
 */
public class BrowseMVP implements EntryPoint
{
	private Place defaultPlace = new WelcomePlace();

	private static boolean USE_REMOTE = true;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.core.client.EntryPoint#onModuleLoad()
	 */
	@Override
	public void onModuleLoad()
	{
		// Create ClientFactory using deferred binding so we can replace with
		// different
		// impls in gwt.xml
		ClientFactory clientFactory = new ClientFactoryImpl(USE_REMOTE);
		EventBus eventBus = clientFactory.getEventBus();
		final PlaceController placeController = clientFactory.getPlaceController();

		// create the home
		HomePageUI homeUI = new HomePageUI();

		// Start ActivityManager for the main widget with our ActivityMapper
		ActivityMapper activityMapper = new AppActivityMapper(clientFactory);
		ActivityManager activityManager = new ActivityManager(activityMapper,
				eventBus);
		activityManager.setDisplay(homeUI.getCentre());

		// Start PlaceHistoryHandler with our PlaceHistoryMapper
		AppPlaceHistoryMapper historyMapper = GWT
				.create(AppPlaceHistoryMapper.class);
		PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(historyMapper);
		historyHandler.register(placeController, eventBus, defaultPlace);

		homeUI.addHomeHandler(new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event)
			{
				placeController.goTo(defaultPlace);
			}
		});

		// put our UI in the root
		RootPanel.get().add(homeUI);

		// Goes to place represented on URL or default place
		historyHandler.handleCurrentHistory();

	}

}
