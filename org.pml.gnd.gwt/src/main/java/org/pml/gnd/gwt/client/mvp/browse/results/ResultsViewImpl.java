package org.pml.gnd.gwt.client.mvp.browse.results;

import java.util.ArrayList;

import org.pml.gnd.gwt.client.to.ElasticMatch;
import org.pml.gnd.gwt.client.to.ElasticResults;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.DockPanel;

public class ResultsViewImpl extends Composite implements ResultsView
{

	private Label msgResults;
	private Label msgHits;
	private CellTable<ElasticMatch> matchTable;
	private Presenter _myPresenter;

	public ResultsViewImpl()
	{

		FlowPanel flowPanel = new FlowPanel();
		initWidget(flowPanel);
		flowPanel.setSize("100%", "100%");

		DockPanel dockPanel = new DockPanel();
		flowPanel.add(dockPanel);
		dockPanel.setSize("100%", "100%");

		FlowPanel flowPanel_1 = new FlowPanel();
		dockPanel.add(flowPanel_1, DockPanel.NORTH);

		msgHits = new Label("#hits pending");
		flowPanel_1.add(msgHits);

		msgResults = new Label("results pending");
		flowPanel_1.add(msgResults);

		ScrollPanel scroll = new ScrollPanel();
		scroll.setAlwaysShowScrollBars(true);
		dockPanel.add(scroll, DockPanel.CENTER);
		scroll.setSize("100%", "100%");

		matchTable = new CellTable<ElasticMatch>();
		scroll.add(matchTable);
		matchTable.setSize("100%", "100%");
		TextColumn<ElasticMatch> nameColumn = new TextColumn<ElasticMatch>()
		{
			@Override
			public String getValue(ElasticMatch match)
			{
				return match.getName();
			}
		};
		TextColumn<ElasticMatch> platformColumn = new TextColumn<ElasticMatch>()
		{
			@Override
			public String getValue(ElasticMatch match)
			{
				return match.getPlatform();
			}
		};
		TextColumn<ElasticMatch> platformTypeColumn = new TextColumn<ElasticMatch>()
		{
			@Override
			public String getValue(ElasticMatch match)
			{
				return match.getPlatformType();
			}
		};
		TextColumn<ElasticMatch> sensorColumn = new TextColumn<ElasticMatch>()
		{
			@Override
			public String getValue(ElasticMatch match)
			{
				return match.getSensor();
			}
		};
		TextColumn<ElasticMatch> sensorTypeColumn = new TextColumn<ElasticMatch>()
		{
			@Override
			public String getValue(ElasticMatch match)
			{
				return match.getSensorType();
			}
		};
		TextColumn<ElasticMatch> trialColumn = new TextColumn<ElasticMatch>()
		{
			@Override
			public String getValue(ElasticMatch match)
			{
				return match.getTrial();
			}
		};

		matchTable.addColumn(nameColumn, "Name");
		matchTable.addColumn(platformColumn, "Platform");
		matchTable.addColumn(platformTypeColumn, "Platform Type");
		matchTable.addColumn(trialColumn, "Trial");
		matchTable.addColumn(sensorColumn, "Sensor");
		matchTable.addColumn(sensorTypeColumn, "SensorType");

		// what do we do if one gets clicked?
		ActionCell.Delegate<ElasticMatch> clicker = new ActionCell.Delegate<ElasticMatch>()
		{
			public void execute(ElasticMatch record)
			{
				viewIt(record.getId());
			}
		};

		// define the click view more column
		Column<ElasticMatch, ElasticMatch> click3 = new Column<ElasticMatch, ElasticMatch>(
				new ActionCell<ElasticMatch>("...", clicker))
		{
			public ElasticMatch getValue(ElasticMatch object)
			{
				return object;
			}
		};
		matchTable.addColumn(click3, "View");

	}

	/**
	 * view the specified dataset
	 * 
	 * @param id
	 */
	protected void viewIt(String id)
	{
		if (_myPresenter != null)
			_myPresenter.viewIt(id);
	}

	@Override
	public void setPresenter(Presenter listener)
	{
		_myPresenter = listener;
	}

	@Override
	public void setResults(ElasticResults results)
	{
		if (results != null)
		{
			int numHits = results.getNumHits();
			msgHits.setText(numHits + " hits" + " after "
					+ results.getElapsedMillis() + " millis");
			ArrayList<ElasticMatch> matches = new ArrayList<ElasticMatch>();
			for (int i = 0; i < numHits; i++)
			{
				ElasticMatch hit = results.getHit(i);
				matches.add(hit);
			}
			showMatches(matches);
		}
		else
		{
			msgHits.setText("num hits pending");
			msgResults.setText("pending");
			matchTable.setRowCount(0);
		}
	}

	private void showMatches(ArrayList<ElasticMatch> matches)
	{
		matchTable.setRowData(0, matches);

	}

}
