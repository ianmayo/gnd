package org.pml.gnd.gwt.client.mvp.browse.info;

import org.pml.gnd.gwt.client.to.Dataset;

import ca.nanometrics.gflot.client.DataPoint;
import ca.nanometrics.gflot.client.PlotModel;
import ca.nanometrics.gflot.client.SeriesHandler;
import ca.nanometrics.gflot.client.SimplePlot;
import ca.nanometrics.gflot.client.options.GridOptions;
import ca.nanometrics.gflot.client.options.PlotOptions;

import com.google.gwt.core.client.JsArrayMixed;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;

public class InfoChartPanel extends Composite
{

	private SimplePanel flowPanel;
	private Dataset _myDataset;

	public InfoChartPanel()
	{
		flowPanel = new SimplePanel();
		initWidget(flowPanel);
	}

	public void setDataset(Dataset dataset)
	{
		_myDataset = dataset;
	}

	public void load()
	{
		// ditch any existing sets
		flowPanel.clear();

		// create a new plot
		PlotModel model = new PlotModel();
		PlotOptions plotOptions = new PlotOptions();

		plotOptions.setGridOptions(new GridOptions().setAboveData(true));

		// create a series
		SeriesHandler handler = model.addSeries("Chart plot for "
				+ _myDataset.getMetadata().getName(), "blue");

		// have a go at lat vs long
		JsArrayString types = _myDataset.getMetadata().getDataTypes();
		if (types != null)
		{
			String typesStr = types.join();
			if (typesStr.contains("lat") && typesStr.contains("lon"))
			{
				JsArrayMixed latArr = _myDataset.getDataArray("lat");
				JsArrayMixed lonArr = _myDataset.getDataArray("lon");
				for (int i = 0; i < latArr.length(); i++)
				{
					handler.add(new DataPoint(lonArr.getNumber(i), latArr.getNumber(i)));
				}
			}
		}

		// create the plot
		SimplePlot plot = new SimplePlot(model, plotOptions);

		flowPanel.setWidget(plot);
	}

}
