package org.pml.gnd.gwt.client.mvp.browse;

import java.util.List;

import com.google.gwt.user.client.ui.HasValue;

public interface SearchView
{

	public ListProvider getPlatforms();
	public ListProvider getPlatformTypes();
	public ListProvider getTrials();
	public ListProvider getSensors();
	public ListProvider getSensorTypes();
  public HasValue<String> getFreeText();
  
  public void setListener(Listener listener);
	
	/** convenience class for managing the facet lists
	 * 
	 * @author ian
	 *
	 */
	public static interface ListProvider
	{
		/** find the current selection(s)
		 * 
		 * @return
		 */
		public List<String> getSelections();

		/** set the items in the list
		 * 
		 * @param theItems
		 */
		public void populateList(List<String> theItems, boolean keepSelection);

		/** clear any selections
		 * 
		 */
		public void clearSelections();

		/** mark the available options
		 * 
		 * @param parseFacet
		 */
		public void showSelected(List<String> parseFacet);
	}

	public interface Listener
	{
		/** do a search with the current search selections
		 * 
		 */
		public void search();

		/** reset the search criteria
		 * 
		 */
		public void reset();
	}
	
}
