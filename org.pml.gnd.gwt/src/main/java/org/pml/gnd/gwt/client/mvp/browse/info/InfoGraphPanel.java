package org.pml.gnd.gwt.client.mvp.browse.info;

import org.pml.gnd.gwt.client.to.Dataset;

import ca.nanometrics.gflot.client.DataPoint;
import ca.nanometrics.gflot.client.PlotModelStrategy;
import ca.nanometrics.gflot.client.PlotWithOverview;
import ca.nanometrics.gflot.client.PlotWithOverviewModel;
import ca.nanometrics.gflot.client.SeriesHandler;
import ca.nanometrics.gflot.client.options.GridOptions;
import ca.nanometrics.gflot.client.options.PlotOptions;

import com.google.gwt.core.client.JsArrayMixed;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;

public class InfoGraphPanel extends Composite
{

	private SimplePanel chartHolder;
	private ListBox xList;
	private ListBox yList;
	private Dataset _myDataset;

	public InfoGraphPanel()
	{
		FlowPanel flowPanel = new FlowPanel();
		initWidget(flowPanel);

		// put in the axis selectors
		HorizontalPanel axisHolders = new HorizontalPanel();
		flowPanel.add(axisHolders);

		Label lblNewLabel = new Label("X-Axis");
		axisHolders.add(lblNewLabel);

		ChangeHandler blur = new ChangeHandler()
		{

			@Override
			public void onChange(ChangeEvent event)
			{
				if ((xList.getSelectedIndex() != -1)
						&& (yList.getSelectedIndex() != -1))
				{
					String xAxis = xList.getValue(xList.getSelectedIndex());
					String yAxis = yList.getValue(yList.getSelectedIndex());
					displayPlot(xAxis, yAxis);
				}
			}
		};

		xList = new ListBox();
		axisHolders.add(xList);
		xList.setEnabled(false);
		xList.addChangeHandler(blur);

		Label lblNewLabel_1 = new Label("Y-Axis");
		axisHolders.add(lblNewLabel_1);

		yList = new ListBox();
		axisHolders.add(yList);
		yList.setEnabled(false);
		yList.addChangeHandler(blur);

		chartHolder = new SimplePanel();
		flowPanel.add(chartHolder);
	}

	public void displayPlot(String xAxis, String yAxis)
	{

		// ditch any existing sets
		chartHolder.clear();

		// create a new plot
		PlotWithOverviewModel model = new PlotWithOverviewModel(
				PlotModelStrategy.defaultStrategy());
		PlotOptions plotOptions = new PlotOptions();
		plotOptions.setGridOptions(new GridOptions().setAboveData(true));

		// create a series
		SeriesHandler handler = model.addSeries("Showing " + xAxis + " vs " + yAxis
				+ " for " + _myDataset.getMetadata().getName(), "blue");

		// have a go at lat vs long
		JsArrayString types = _myDataset.getMetadata().getDataTypes();
		String typesStr = types.join();
		if (typesStr.contains(xAxis) && typesStr.contains(yAxis))
		{
			JsArrayMixed xArr = _myDataset.getDataArray(xAxis);
			JsArrayMixed yArr = _myDataset.getDataArray(yAxis);
			for (int i = 0; i < xArr.length(); i++)
			{
				final double xVal;
				if (xAxis.equals("time"))
					xVal = i;
				else
					xVal = xArr.getNumber(i);
				final double yVal;
				if (yAxis.equals("time"))
					yVal = i;
				else
					yVal = yArr.getNumber(i);

				handler.add(new DataPoint(xVal, yVal));
			}
		}

		// create the plot
		final PlotWithOverview plot = new PlotWithOverview(model, plotOptions);
		plot.setLinearSelection(150, 199);

		chartHolder.setWidget(plot);
	}

	public void setDataset(Dataset dataset)
	{
		_myDataset = dataset;
	}

	public void load()
	{
		// clear the lists
		xList.clear();
		yList.clear();
		
		// clear the graph
		chartHolder.clear();

		// set the axes
		JsArrayString cols = _myDataset.getMetadata().getDataTypes();
		for (int i = 0; i < cols.length(); i++)
		{
			xList.addItem(cols.get(i));
			yList.addItem(cols.get(i));
		}

		xList.setEnabled(true);
		yList.setEnabled(true);

	}

}
