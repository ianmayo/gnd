package org.pml.gnd.gwt.client.mvp.browse.results;

import org.pml.gnd.gwt.client.to.ElasticResults;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * View interface. Extends IsWidget so a view impl can easily provide its
 * container widget.
 * 
 * @author drfibonacci
 */
public interface ResultsView extends IsWidget
{
	void setPresenter(Presenter listener);

	void setResults(ElasticResults results);

	public interface Presenter
	{

		/**
		 * display the specified dataset
		 * 
		 * @param id
		 */
		void viewIt(String id);
	}

}