package org.pml.gnd.gwt.client.mvp.welcome;

import org.pml.gnd.gwt.client.to.ElasticCount;
import org.pml.gnd.gwt.client.to.WelcomeItem;
import org.pml.gnd.gwt.client.to.WelcomeResults;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.CaptionPanel;

public class WelcomeViewImpl extends Composite implements WelcomeView
{

	private Presenter _myListener;
	private FlowPanel _itemListing;
	private Label _countStr;

	public WelcomeViewImpl()
	{

		HorizontalPanel mainHolder = new HorizontalPanel();
		initWidget(mainHolder);
		mainHolder.setSize("100%", "100%");

		CaptionPanel navVert = new CaptionPanel("Welcome");
		mainHolder.add(navVert);

		VerticalPanel verticalPanel = new VerticalPanel();
		navVert.setContentWidget(verticalPanel);
		verticalPanel.setSize("5cm", "3cm");

		HorizontalPanel item1 = new HorizontalPanel();
		verticalPanel.add(item1);

		Button btnBrowse = new Button("Browse");
		item1.add(btnBrowse);
		btnBrowse.addClickHandler(new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event)
			{
				if (_myListener != null)
					_myListener.browse();
			}
		});

		_countStr = new Label("count pending");
		item1.add(_countStr);

		Button btnAdd = new Button("Add");
		verticalPanel.add(btnAdd);
		btnAdd.addClickHandler(new ClickHandler()
		{
			@Override
			public void onClick(ClickEvent event)
			{
				if (_myListener != null)
					_myListener.add();
			}
		});

		Button btnMaintain = new Button("Maintain");
		verticalPanel.add(btnMaintain);
		btnMaintain.addClickHandler(new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event)
			{
				if (_myListener != null)
					_myListener.maintain();
			}
		});

		CaptionPanel dataFlow = new CaptionPanel("New datasets");
		mainHolder.add(dataFlow);
		dataFlow.setSize("100%", "100%");

		_itemListing = new FlowPanel();
		dataFlow.setContentWidget(_itemListing);
		_itemListing.setSize("100%", "100%");

	}

	@Override
	public void setPresenter(Presenter listener)
	{
		_myListener = listener;
	}

	@Override
	public void setRecentItems(WelcomeResults items)
	{
		if (items == null)
		{
			_itemListing.clear();

			Hyperlink hprlnkDataset_2 = new Hyperlink("new this month", false,
					"BrowsePlace:F={new this month}");
			_itemListing.add(hprlnkDataset_2);
		}
		else

			for (int i = 0; i < items.getNumItems(); i++)
			{
				WelcomeItem item = items.getHit(i);
				Hyperlink hprlnkDataset = new Hyperlink(item.getValue(), false,
						"BrowsePlace:I=" + item.getId());
				_itemListing.add(hprlnkDataset);
			}

	}

	@Override
	public void setCount(ElasticCount result)
	{
		_countStr.setText("(" + result.getCount() + " records)");
	}
}
