package org.pml.gnd.gwt.client.util;

import java.util.Map;
import java.util.Map.Entry;

import com.google.gwt.http.client.URL;

public class URLUtils {
	
	public static String ensureSlash(String str) {
		if (! str.endsWith("/")) {
			str += "/";
		}
		return str; 
	}
	
	public static String joinUrls(String... parts) {
		return joinUrls(true, parts);
	}
	
	public static String joinUrls(boolean encode, String... parts) {
		if (parts == null || parts.length == 0) {
			return "";
		}
		StringBuilder builder = new StringBuilder(ensureSlash(parts[0]));
		for (int i = 1; i < parts.length; i++) {
			String part = parts[i];
			if (part.startsWith("/")) {
				part = part.substring(1);
			}
			builder.append(ensureSlash(part));
		}
		String url = builder.substring(0, builder.length() - 1);
		return encode ? URL.encode(url) : url;
	}
	
	public static String generateQueryString(Map<String, Object> parameters) {
		if (parameters == null || parameters.isEmpty()) {
			return "";
		}
		StringBuilder builder = new StringBuilder();
		for (Entry<String, Object> entry : parameters.entrySet()) {
			builder.append(URL.encodeQueryString(entry.getKey()));
			builder.append('=');
			builder.append(URL.encodeQueryString(entry.getValue().toString()));
			builder.append('&');
		}
		return builder.substring(0, builder.length() - 1);
	}
	
	public static String joinQueryString(String url, String queryString) {
		if (queryString == null || queryString.isEmpty()) {
			return url;
		}
		return url + "?" + queryString;
	}
	
	public static String getHostName(String url) {
		boolean http = url.startsWith("http://");
		boolean https = url.startsWith("https://");
		if (! http && ! https) {
			return url;
		}
		int first = http ? 7 : 8;
		int index = url.indexOf('/', first);
		if (index == -1) {
			index = url.length();
		}
		return url.substring(first, index);
	}

}
