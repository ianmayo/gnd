package org.pml.gnd.gwt.client.mvp;

import org.pml.gnd.gwt.client.mvp.add.AddView;
import org.pml.gnd.gwt.client.mvp.browse.BrowseView;
import org.pml.gnd.gwt.client.mvp.browse.SearchModel;
import org.pml.gnd.gwt.client.mvp.browse.info.InfoModel;
import org.pml.gnd.gwt.client.mvp.browse.info.InfoView;
import org.pml.gnd.gwt.client.mvp.browse.results.ResultsView;
import org.pml.gnd.gwt.client.mvp.maintain.MaintainView;
import org.pml.gnd.gwt.client.mvp.operate.OperateView;
import org.pml.gnd.gwt.client.mvp.welcome.WelcomeModel;
import org.pml.gnd.gwt.client.mvp.welcome.WelcomeView;

import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

public interface ClientFactory
{
	EventBus getEventBus();
	PlaceController getPlaceController();
	WelcomeView getWelcomeView();
	BrowseView getBrowseView();
	AddView getAddView();
	MaintainView getMaintainView();
	InfoView getInfoView();
	ResultsView getFilterView();
	SearchModel getSearchModel();
	InfoModel getInfoModel();
	OperateView getOperateView();
	WelcomeModel getWelcomeModel();
}
