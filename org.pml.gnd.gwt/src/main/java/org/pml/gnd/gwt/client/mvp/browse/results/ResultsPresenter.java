package org.pml.gnd.gwt.client.mvp.browse.results;

import org.pml.gnd.gwt.client.to.ElasticResults;

public class ResultsPresenter
{
	private ResultsView _myView;

	public ResultsPresenter(ResultsView view)
	{
		_myView = view;
	}

	public void setResults(ElasticResults result)
	{
		_myView.setResults(result);
	}
}
