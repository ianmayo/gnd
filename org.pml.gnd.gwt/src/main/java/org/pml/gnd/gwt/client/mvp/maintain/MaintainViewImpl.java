package org.pml.gnd.gwt.client.mvp.maintain;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class MaintainViewImpl extends Composite implements MaintainView
{

	public MaintainViewImpl()
	{
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		initWidget(horizontalPanel);
		
		VerticalPanel verticalPanel = new VerticalPanel();
		horizontalPanel.add(verticalPanel);
		
		Label lblWelcoem = new Label("maintain stuff");
		verticalPanel.add(lblWelcoem);
		
	}

	@Override
	public void setPresenter(Presenter listener)
	{
	}

}
