package org.pml.gnd.gwt.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.Constants;

public interface GndConstants extends Constants {
	public static GndConstants INSTANCE = GWT.create(GndConstants.class);	
	
	String couchDbUrl();	
}
