package org.pml.gnd.gwt.client.dao;

import org.pml.gnd.gwt.client.dao.builders.QueryBuilder;
import org.pml.gnd.gwt.client.dao.transport.Transport;

import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class CouchDbConnector {
	
	private Transport transport;

	public CouchDbConnector(Transport transport) {
		assert transport != null : "CouchDB can't be instanciated without transport";
		this.transport = transport;
	}
	
	public void execute(QueryBuilder<?> builder, AsyncCallback<JSONValue> callback) {
		transport.get(builder.generateUrl(), callback);
	}
	
}
