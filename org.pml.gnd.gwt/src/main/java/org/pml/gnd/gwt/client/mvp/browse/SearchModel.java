package org.pml.gnd.gwt.client.mvp.browse;

import org.pml.gnd.gwt.client.to.ElasticResults;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface SearchModel
{
	public void getMatches(String query, AsyncCallback<ElasticResults> callback);

	public void getAll(AsyncCallback<ElasticResults> callback);
}
