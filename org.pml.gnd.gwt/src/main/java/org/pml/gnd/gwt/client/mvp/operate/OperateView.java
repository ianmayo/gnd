package org.pml.gnd.gwt.client.mvp.operate;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * View interface. Extends IsWidget so a view impl can easily provide
 * its container widget.
 *
 * @author drfibonacci
 */
public interface OperateView extends IsWidget
{
	void setPresenter(Presenter listener);

	public interface Presenter
	{
	}
}