package org.pml.gnd.gwt.client.to;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Dataset TO object
 * 
 * { "_type" : "terms", "missing" : 0, "total" : 5, "other" : 0, "terms" : [ {
 * "term" : "roger", "count" : 3 }, { "term" : "tomato", "count" : 1 }, { "term"
 * : "jolly", "count" : 1 } ] }
 * 
 * @author Yuri
 * 
 */
public class ElasticFacet extends JavaScriptObject
{

	protected ElasticFacet()
	{
	}

	public final native int getNumTerms() /*-{
		if (this.terms == null)
			return 0;
		else
			return this.terms.length;
	}-*/;

	public final native String getTermName(int index) /*-{
		return this.terms[index].term;
	}-*/;

	public final native int getTermCount(int index) /*-{
		return this.terms[index].count;
	}-*/;

}
