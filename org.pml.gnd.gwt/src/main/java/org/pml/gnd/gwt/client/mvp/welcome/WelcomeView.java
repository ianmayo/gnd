package org.pml.gnd.gwt.client.mvp.welcome;

import org.pml.gnd.gwt.client.to.ElasticCount;
import org.pml.gnd.gwt.client.to.WelcomeResults;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * View interface. Extends IsWidget so a view impl can easily provide
 * its container widget.
 *
 * @author drfibonacci
 */
public interface WelcomeView extends IsWidget
{
	void setPresenter(Presenter listener);
	void setCount(ElasticCount result);
	void setRecentItems(WelcomeResults items);
	
	public interface Presenter
	{
		void browse();
		void view(String datasetId);
		void add();
		void maintain();
	}

}