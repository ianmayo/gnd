package org.pml.gnd.gwt.client.mvp.add;

import org.pml.gnd.gwt.client.mvp.ClientFactory;
import org.pml.gnd.gwt.client.mvp.CoreActivity;
import org.pml.gnd.gwt.client.mvp.place.AddPlace;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

public class AddActivity extends CoreActivity implements
		AddView.Presenter {
	// Name that will be appended to "Hello,"

	public AddActivity(AddPlace place, ClientFactory clientFactory) {
		super(clientFactory);
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		AddView browseView = getFactory().getAddView();
		browseView.setPresenter(this);
		containerWidget.setWidget(browseView.asWidget());
	}

}
