package org.pml.gnd.gwt.client.mvp.welcome;

import org.pml.gnd.gwt.client.to.ElasticCount;
import org.pml.gnd.gwt.client.to.WelcomeResults;

import com.google.gwt.jsonp.client.JsonpRequestBuilder;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class RemoteWelcomeModelImpl implements WelcomeModel
{

	@Override
	public void getRecentDatasets(AsyncCallback<WelcomeResults> callback)
	{
		String url = "http://localhost:5984/tracks/_design/tracks/_view/track_listing?limit=10";
		// Send request to server and catch any errors.
		JsonpRequestBuilder builder = new JsonpRequestBuilder();
		builder.requestObject(url, callback);
	}

	@Override
	public void getDatasetCount(AsyncCallback<ElasticCount> callback)
	{
		String url = "http://localhost:9200/gnd/_count";
		// Send request to server and catch any errors.
		JsonpRequestBuilder builder = new JsonpRequestBuilder();
		builder.requestObject(url, callback);

	}
}
