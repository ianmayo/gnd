package org.pml.gnd.gwt.client.mvp;

import org.pml.gnd.gwt.client.mvp.add.AddActivity;
import org.pml.gnd.gwt.client.mvp.browse.BrowseActivity;
import org.pml.gnd.gwt.client.mvp.maintain.MaintainActivity;
import org.pml.gnd.gwt.client.mvp.operate.OperateActivity;
import org.pml.gnd.gwt.client.mvp.place.AddPlace;
import org.pml.gnd.gwt.client.mvp.place.BrowsePlace;
import org.pml.gnd.gwt.client.mvp.place.MaintainPlace;
import org.pml.gnd.gwt.client.mvp.place.OperatePlace;
import org.pml.gnd.gwt.client.mvp.place.WelcomePlace;
import org.pml.gnd.gwt.client.mvp.welcome.WelcomeActivity;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

public class AppActivityMapper implements ActivityMapper {

	private ClientFactory clientFactory;

	/**
	 * AppActivityMapper associates each Place with its corresponding
	 * {@link Activity}
	 * 
	 * @param clientFactory
	 *            Factory to be passed to activities
	 */
	public AppActivityMapper(ClientFactory clientFactory) {
		super();
		this.clientFactory = clientFactory;
	}

	/**
	 * Map each Place to its corresponding Activity. This would be a great use
	 * for GIN.
	 */
	@Override
	public Activity getActivity(Place place) {
		// This is begging for GIN
		if (place instanceof WelcomePlace)
			return new WelcomeActivity((WelcomePlace) place, clientFactory);
		else if (place instanceof BrowsePlace)
			return new BrowseActivity((BrowsePlace) place, clientFactory);
		else if (place instanceof AddPlace)
			return new AddActivity((AddPlace) place, clientFactory);
		else if (place instanceof OperatePlace)
			return new OperateActivity((OperatePlace) place, clientFactory);
		else if (place instanceof MaintainPlace)
			return new MaintainActivity((MaintainPlace) place, clientFactory);

		return null;
	}

}
