package org.pml.gnd.gwt.client.mvp;

import org.pml.gnd.gwt.client.mvp.place.AddPlace;
import org.pml.gnd.gwt.client.mvp.place.BrowsePlace;
import org.pml.gnd.gwt.client.mvp.place.MaintainPlace;
import org.pml.gnd.gwt.client.mvp.place.OperatePlace;
import org.pml.gnd.gwt.client.mvp.place.WelcomePlace;

import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;

/**
 * PlaceHistoryMapper interface is used to attach all places which the
 * PlaceHistoryHandler should be aware of. This is done via the @WithTokenizers
 * annotation or by extending PlaceHistoryMapperWithFactory and creating a
 * separate TokenizerFactory.
 */
@WithTokenizers( { WelcomePlace.Tokenizer.class, BrowsePlace.Tokenizer.class, OperatePlace.Tokenizer.class, AddPlace.Tokenizer.class, MaintainPlace.Tokenizer.class})
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper {
}
