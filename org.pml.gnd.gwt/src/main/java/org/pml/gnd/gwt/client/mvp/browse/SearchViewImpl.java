package org.pml.gnd.gwt.client.mvp.browse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SubmitButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;

public class SearchViewImpl extends Composite implements SearchView,
		ClickHandler
{
	private EasyBox platform_types;
	private EasyBox trials;
	private EasyBox platforms;
	private TextBox freeText;
	private Listener _myListener;
	private EasyBox sensors;
	private EasyBox sensorTypes;

	public SearchViewImpl()
	{

		FlowPanel flowPanel = new FlowPanel();
		initWidget(flowPanel);
		flowPanel.setHeight("100%");

		FlexTable grid = new FlexTable();
		flowPanel.add(grid);
		
		int thisRow = 0;

		Label lblPlatform = new Label("Platform");
		grid.setWidget(thisRow, 0, lblPlatform);

		platforms = new EasyBox(this);
		grid.setWidget(thisRow, 1, platforms);
		platforms.setVisibleItemCount(5);

		Label lblPlatformType = new Label("Platform type");
		grid.setWidget(++thisRow, 0, lblPlatformType);

		platform_types = new EasyBox(this);
		grid.setWidget(thisRow, 1, platform_types);
		platform_types.setVisibleItemCount(5);

		Label lblTrial = new Label("Trial");
		grid.setWidget(++thisRow, 0, lblTrial);

		trials = new EasyBox(this);
		grid.setWidget(thisRow, 1, trials);
		trials.setVisibleItemCount(5);

		Label lblSensor = new Label("Sensor");
		grid.setWidget(++thisRow, 0, lblSensor);

		sensors = new EasyBox(this);
		grid.setWidget(thisRow, 1, sensors);
		sensors.setVisibleItemCount(5);

		Label lblSensorType = new Label("SensorType");
		grid.setWidget(++thisRow, 0, lblSensorType);

		sensorTypes = new EasyBox(this);
		grid.setWidget(thisRow, 1, sensorTypes);
		sensorTypes.setVisibleItemCount(5);


		Label lblOthers = new Label("others...");
		grid.setWidget(++thisRow, 0, lblOthers);

		Label lblAfter = new Label("After");
		grid.setWidget(++thisRow, 0, lblAfter);

		DateBox dateBox = new DateBox();
		grid.setWidget(thisRow, 1, dateBox);

		Label lblFreeText = new Label("Free text");
		grid.setWidget(++thisRow, 0, lblFreeText);

		freeText = new TextBox();
		grid.setWidget(thisRow, 1, freeText);

		Button btnSearch = new SubmitButton("Search");
		grid.setWidget(++thisRow, 1, btnSearch);
		btnSearch.addClickHandler(new ClickHandler()
		{
			@Override
			public void onClick(ClickEvent event)
			{
				if (_myListener != null)
					_myListener.search();
			}
		});

		Button btnReset = new Button("Reset");
		grid.setWidget(++thisRow, 1, btnReset);
		btnReset.addClickHandler(new ClickHandler()
		{
			@Override
			public void onClick(ClickEvent event)
			{
				if (_myListener != null)
					_myListener.reset();
			}
		});

	}

	@Override
	public ListProvider getPlatforms()
	{
		return platforms;
	}

	@Override
	public ListProvider getPlatformTypes()
	{
		return platform_types;
	}

	@Override
	public ListProvider getSensors()
	{
		return sensors;
	}

	@Override
	public ListProvider getSensorTypes()
	{
		return sensorTypes;
	}

	@Override
	public ListProvider getTrials()
	{
		return trials;
	}

	@Override
	public HasValue<java.lang.String> getFreeText()
	{
		return freeText;
	}

	private static class EasyBox extends ListBox implements ListProvider
	{

		public EasyBox(ClickHandler handler)
		{
			super(true);
			// this.addClickHandler(handler);
		}

		@Override
		public void populateList(List<String> theItems, boolean keepSelection)
		{
			// remember the selections
			List<String> sels = getSelections();

			// clear the list
			clear();
			for (Iterator<String> iterator = theItems.iterator(); iterator.hasNext();)
			{
				String string = iterator.next();
				addItem(string);
				
				// are we highlighting?
				if(keepSelection)
				{
					if(sels.contains(trimSelection(string)))
						this.setItemSelected(this.getItemCount()-1, true);
				}
			}
		}

		@Override
		public List<String> getSelections()
		{
			List<String> items = new ArrayList<String>();
			for (int i = 0; i < this.getItemCount(); i++)
			{
				if (this.isItemSelected(i))
				{
					String thisItem = trimSelection(this.getItemText(i));

					items.add(thisItem);
				}
			}
			return items;
		}

		private String trimSelection(String txt)
		{
			// trim off any round brackets
			String thisItem = txt.split("\\(")[0];

			// and white space
			thisItem = thisItem.trim();
			return thisItem;
		}

		@Override
		public void clearSelections()
		{
			for (int i = 0; i < this.getItemCount(); i++)
			{
				this.setItemSelected(i, false);
			}
		}

		@Override
		public void showSelected(List<String> parseFacet)
		{
			for (int i = 0; i < this.getItemCount(); i++)
			{
				String thisItem = trimSelection(this.getItemText(i));
				if (parseFacet.contains(thisItem))
					this.setItemSelected(i, true);
				else
					this.setItemSelected(i, false);
			}
		}

	}

	@Override
	public void setListener(Listener listener)
	{
		_myListener = listener;
	}

	@Override
	public void onClick(ClickEvent event)
	{
		if (_myListener != null)
			_myListener.search();
	}

}
