package org.pml.gnd.gwt.client.mvp.welcome;

import org.pml.gnd.gwt.client.to.ElasticCount;
import org.pml.gnd.gwt.client.to.WelcomeResults;

import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class LocalWelcomeModelImpl implements WelcomeModel
{
	public void getRecentDatasets(RequestCallback callback)
	{

		String url = "org.pml.gnd.gwt.Gnd/test_data/recent.js";
		// Send request to server and catch any errors.
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, url);

		try
		{
			builder.sendRequest(null, callback);
		}
		catch (RequestException e)
		{
			System.err.println("ERROR:" + e.getMessage());
		}
	}

	@Override
	public void getRecentDatasets(final AsyncCallback<WelcomeResults> callback)
	{
		RequestCallback rq = new RequestCallback()
		{

			@Override
			public void onResponseReceived(Request request, Response response)
			{
				if (200 == response.getStatusCode())
				{
					// ok, now we have to process them...
					final WelcomeResults result = JsonUtils
							.unsafeEval(response.getText());

					callback.onSuccess(result);
				}
				else
					callback.onFailure(new Throwable("Response status:" + response.getStatusText()));
			}

			@Override
			public void onError(Request request, Throwable exception)
			{
				callback.onFailure(exception);
			}
		};
		
		getRecentDatasets(rq);
	}

	@Override
	public void getDatasetCount(AsyncCallback<ElasticCount> callback)
	{
		// ignore...
	}

}
