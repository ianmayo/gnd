package org.pml.gnd.gwt.client.to;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Dataset TO object
 * 
 * {"id":"b945b353f8d8d71120e350c8a225aa12","key":"b945b353f8d8d71120e350c8a225aa12","value":"236597000 - AASLI (42) pts"},
 *
 * @author Isn
 * 
 */
public class WelcomeItem extends JavaScriptObject
{

	protected WelcomeItem()
	{
	}

	public final native String getId() /*-{
		return this.id;
	}-*/;
	

	public final native String getValue() /*-{
		return this.value;
	}-*/;

}
