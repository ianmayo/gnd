{
  "took" : 6,
  "timed_out" : false,
  "_shards" : {
    "total" : 5,
    "successful" : 5,
    "failed" : 0
  },
  "hits" : {
    "total" : 5,
    "max_score" : 2.1410832,
    "hits" : [ {
      "_index" : "gnd",
      "_type" : "datasets",
      "_id" : "72b6efa2c581e7615bb765208312fd17_1",
      "_score" : 2.1410832, "_source" : {"sensor":"Garmin-G400","platform":"plat_a","sensor_type":"speed","data_type":["time","lon","lat"],"platform_type":"van","time_bounds":{"start":"2012-03-15T03:46:37+0000","end":"2012-03-15T16:35:58+0000"},"name":"Track 30","type":"track","geo_bounds":{"tl":[-7.784170959924891,57.944529113991734],"br":[-5.019334420312834,49.109100753386016]},"trial":"trial_d"}
    }, {
      "_index" : "gnd",
      "_type" : "datasets",
      "_id" : "72b6efa2c581e7615bb765208312a0c9_1",
      "_score" : 1.8481886, "_source" : {"sensor":"Garmin-G400","platform":"plat_a","sensor_type":"speed","data_type":["time","lon","lat"],"platform_type":"car","time_bounds":{"start":"2012-03-14T13:07:18+0000","end":"2012-03-15T02:55:34+0000"},"name":"Track 17","type":"track","geo_bounds":{"tl":[-7.9712073675533,57.80955669601785],"br":[-5.000857121521625,49.04946998127201]},"trial":"trial_b"}
    }, {
      "_index" : "gnd",
      "_type" : "datasets",
      "_id" : "72b6efa2c581e7615bb765208313c1b7_1",
      "_score" : 1.8129525, "_source" : {"sensor":"Garmin-G300","platform":"plat_a","sensor_type":"temp","data_type":["time","lon","lat"],"platform_type":"car","time_bounds":{"start":"2012-03-15T02:33:17+0000","end":"2012-03-15T15:11:59+0000"},"name":"Track 51","type":"track","geo_bounds":{"tl":[-7.977247425786304,56.13135083208631],"br":[-5.014104533173364,49.08996876846529]},"trial":"trial_c"}
    }, {
      "_index" : "gnd",
      "_type" : "datasets",
      "_id" : "72b6efa2c581e7615bb765208312aac6_1",
      "_score" : 1.7725551, "_source" : {"sensor":"Garmin-G100","platform":"plat_a","sensor_type":"speed","data_type":["time","lon","lat"],"platform_type":"car","time_bounds":{"start":"2012-03-14T17:14:59+0000","end":"2012-03-15T05:25:17+0000"},"name":"Track 19","type":"track","geo_bounds":{"tl":[-7.893884247635603,57.52730661161165],"br":[-5.005948273775663,49.00207246888446]},"trial":"trial_a"}
    }, {
      "_index" : "gnd",
      "_type" : "datasets",
      "_id" : "72b6efa2c581e7615bb765208314cea8_1",
      "_score" : 1.2990458, "_source" : {"sensor":"Garmin-G300","platform":"plat_a","sensor_type":"temp","data_type":["time","lon","lat"],"platform_type":"car","time_bounds":{"start":"2012-03-15T08:43:44+0000","end":"2012-03-15T21:35:22+0000"},"name":"Track 78","type":"track","geo_bounds":{"tl":[-7.999818696022181,56.83950338807612],"br":[-5.028319063940317,49.04018144733467]},"trial":"trial_b"}
    } ]
  }
}