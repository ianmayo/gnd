package org.pml.gnd.gwt.client.mvp.browse.info;

import org.pml.gnd.gwt.client.to.Dataset;

import com.google.gwt.user.client.rpc.AsyncCallback;

public class InfoPresenter
{
	private InfoView _myView;
	private InfoModel _myModel;

	public InfoPresenter(InfoView view, InfoModel model)
	{
		_myView = view;
		_myModel = model;
	}

	public void setDataset(final String id)
	{

		// clear the UI first
		_myView.setDataset(null);

		_myModel.getDataset(id, new AsyncCallback<Dataset>()
		{

			@Override
			public void onSuccess(Dataset result)
			{

				// ok, put the results in to the view
				_myView.setDataset(result);
			}

			@Override
			public void onFailure(Throwable caught)
			{
				displayError("Couldn't retrieve JSON (" + caught.getMessage() + ")");
			}
		});

	}

	protected void displayError(String string)
	{
		System.err.println("Browse error" + string);
	}
}
