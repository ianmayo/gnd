package org.pml.gnd.gwt.client.dao.builders;

import org.pml.gnd.gwt.client.util.URLUtils;

public class SimpleQueryBuilder extends QueryBuilder<SimpleQueryBuilder> {
	
	protected String _documentId;
	
	protected String _showFunction;

	public String getDocumentId() {
		return _documentId;
	}

	public SimpleQueryBuilder documentId(String _documentId) {
		this._documentId = _documentId;
		return this;
	}

	public String getShowFunction() {
		return _showFunction;
	}

	public SimpleQueryBuilder showFunction(String _showFunction) {
		this._showFunction = _showFunction;
		return this;
	}

	@Override
	public String generateUrl() {
		assert _documentId != null : "documentid must be specified for simple queries";
		String url;
		if (_showFunction != null) {
			assert _designDocId != null : "show function wtihout design doc";
			url = URLUtils.joinUrls(_designDocId, "_show", _showFunction, _documentId);
		} else {
			url = _documentId;
		}
		return URLUtils.joinQueryString(url, URLUtils.generateQueryString(urlParameters));
	}
}
