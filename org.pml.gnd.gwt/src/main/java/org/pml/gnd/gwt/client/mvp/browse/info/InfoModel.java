package org.pml.gnd.gwt.client.mvp.browse.info;

import org.pml.gnd.gwt.client.to.Dataset;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface InfoModel
{
	/** get the specified dataset
	 * 
	 * @param id
	 * @param callback
	 */
	public void getDataset(String id, AsyncCallback<Dataset> callback);
}
