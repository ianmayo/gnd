package org.pml.gnd.gwt.client.mvp.browse.info;

import org.pml.gnd.gwt.client.to.Dataset;

import com.google.gwt.event.logical.shared.BeforeSelectionEvent;
import com.google.gwt.event.logical.shared.BeforeSelectionHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TabPanel;

public class InfoViewImpl extends Composite implements InfoView
{

	private Label msgLabel;
	private InfoInfoPanel infoTab;
	private InfoTablePanel tableTab;
	private InfoGraphPanel graphTab;
	private InfoChartPanel chartTab;
	private TabPanel tabPanel;

	public InfoViewImpl()
	{
		DockPanel doc = new DockPanel();
		initWidget(doc);
		doc.setSize("100%", "100%");

		FlowPanel header = new FlowPanel();

		msgLabel = new Label("pending");
		header.add(msgLabel);

		doc.add(header, DockPanel.NORTH);

		tabPanel = new TabPanel();
		infoTab = new InfoInfoPanel();
		tabPanel.add(infoTab, "Info");

		tableTab = new InfoTablePanel();
		tabPanel.add(tableTab, "Table");

		graphTab = new InfoGraphPanel();
		tabPanel.add(graphTab, "Graph");

		chartTab = new InfoChartPanel();
		tabPanel.add(chartTab, "Chart");

		doc.add(tabPanel, DockPanel.CENTER);
		
		// show the first tab
		tabPanel.selectTab(0);

		// listen for the other tabs
		BeforeSelectionHandler<Integer> handler = new BeforeSelectionHandler<Integer>(){

			@Override
			public void onBeforeSelection(BeforeSelectionEvent<Integer> event)
			{
				int index = event.getItem();
				switch(index)
				{
				case 0:
					break;
				case 1:
					tableTab.load();
					break;
				case 2:
					graphTab.load();
					break;
				case 3:
					chartTab.load();
					break;
				}
			}};
		tabPanel.addBeforeSelectionHandler(handler);
		
	}

	@Override
	public void setPresenter(Presenter listener)
	{
	}

	@Override
	public void setDataset(Dataset dataset)
	{
		if (dataset != null)
		{
			msgLabel.setText("showing " + dataset.getMetadata().getName());
			infoTab.setDataset(dataset);
			tableTab.setDataset(dataset);
			graphTab.setDataset(dataset);
			chartTab.setDataset(dataset);
		}
	}

	@Override
	public void showFirstTab()
	{
		tabPanel.selectTab(0);
	}

}
