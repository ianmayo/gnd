package org.pml.gnd.gwt.client.mvp.welcome;

import org.pml.gnd.gwt.client.to.ElasticCount;
import org.pml.gnd.gwt.client.to.ElasticResults;
import org.pml.gnd.gwt.client.to.WelcomeResults;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface WelcomeModel
{
	/** get a list of recent datasets
	 * 
	 * @param callback
	 */
	public void getRecentDatasets(AsyncCallback<WelcomeResults> callback);

	/** find out how many datasets there are
	 * 
	 * @param callback
	 */
	void getDatasetCount(AsyncCallback<ElasticCount> callback);
}
