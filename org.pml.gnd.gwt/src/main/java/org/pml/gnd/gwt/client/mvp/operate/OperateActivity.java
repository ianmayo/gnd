package org.pml.gnd.gwt.client.mvp.operate;

import org.pml.gnd.gwt.client.mvp.ClientFactory;
import org.pml.gnd.gwt.client.mvp.CoreActivity;
import org.pml.gnd.gwt.client.mvp.place.OperatePlace;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

public class OperateActivity extends CoreActivity implements
		OperateView.Presenter {
	// Name that will be appended to "Hello,"

	public OperateActivity(OperatePlace place, ClientFactory clientFactory) {
		super(clientFactory);
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		OperateView browseView = getFactory().getOperateView();
		browseView.setPresenter(this);
		containerWidget.setWidget(browseView.asWidget());
	}
}
