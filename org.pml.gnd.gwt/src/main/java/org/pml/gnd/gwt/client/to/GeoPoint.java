package org.pml.gnd.gwt.client.to;

public class GeoPoint {
	
	private double lat;
	
	private double lon;
	
	public GeoPoint() {
	}

	public GeoPoint(double lat, double lon) {
		this.lat = lat;
		this.lon = lon;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	@Override
	public String toString() {
		return "GeoPoint [lat=" + lat + ", lon=" + lon + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) ((int) lat ^ ((int) lat >>> 32));
		result = prime * result + (int) ((int) lon ^ ((int) lon >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if ((obj != null) && (obj instanceof GeoPoint)) {
			GeoPoint other = (GeoPoint) obj;
			return lat == other.lat && lon == other.lon;
		}
		return false;
	}
	
	
}
