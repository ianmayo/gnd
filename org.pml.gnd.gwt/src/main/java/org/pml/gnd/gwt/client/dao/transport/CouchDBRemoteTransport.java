package org.pml.gnd.gwt.client.dao.transport;

import org.pml.gnd.gwt.client.util.URLUtils;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.http.client.URL;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.jsonp.client.JsonpRequestBuilder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;

public class CouchDBRemoteTransport implements Transport {

	private String couchDbUrl;

	public CouchDBRemoteTransport(String couchDbUrl) {
		assert couchDbUrl != null : "couchdb url must be specified";
		this.couchDbUrl = URL.encode(URLUtils.ensureSlash(couchDbUrl));
	}	
	
	@Override
	public void get(String relativeUrl, final AsyncCallback<JSONValue> callback) {
		assert callback != null : "response must be specified";
		assert relativeUrl != null : "request url must be specified";
		
		JsonpRequestBuilder builder = new JsonpRequestBuilder();
		builder.requestObject(URLUtils.joinUrls(false, couchDbUrl, relativeUrl), new AsyncCallback<JavaScriptObject>() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess(JavaScriptObject result) {
				callback.onSuccess(new JSONObject(result));				
			}
		});
	}

	@Override
	public void post(String relativeUrl, String responseBody, AsyncCallback<JSONValue> callback) {
		callback.onFailure(new StatusCodeException(5000, "not available with remote couch db"));
	}

	@Override
	public void post(String relativeUrl, AsyncCallback<JSONValue> callback) {
		post(relativeUrl, callback);
	}

	@Override
	public boolean isPostSupported() {
		return false;
	}

}
