package org.pml.gnd.gwt.client;

import org.pml.gnd.gwt.client.dao.CouchDbConnector;
import org.pml.gnd.gwt.client.dao.builders.SimpleQueryBuilder;
import org.pml.gnd.gwt.client.dao.transport.CouchDBLocalTransport;
import org.pml.gnd.gwt.client.dao.transport.CouchDBRemoteTransport;
import org.pml.gnd.gwt.client.dao.transport.Transport;
import org.pml.gnd.gwt.client.to.Dataset;
import org.pml.gnd.gwt.client.util.URLUtils;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class Gnd implements EntryPoint {
	
	private CouchDbConnector connector;
	
	private void createConnector() {
		String couchDbUrl = GndConstants.INSTANCE.couchDbUrl();
		String moduleHost = URLUtils.getHostName(GWT.getModuleBaseURL());
		String couchHost = URLUtils.getHostName(couchDbUrl);
		Transport transport = moduleHost.equals(couchHost) ? new CouchDBLocalTransport(couchDbUrl) : new CouchDBRemoteTransport(couchDbUrl);
		connector = new CouchDbConnector(transport);
	}

	@Override
	public void onModuleLoad() {
		createConnector();
		AsyncCallback<JSONValue> callback = new AsyncCallback<JSONValue>() {
			
			@Override
			public void onSuccess(JSONValue result) {				
				Dataset data = result.isObject().getJavaScriptObject().cast();
				GWT.log("platform=" + data.getMetadata().getPlatform());
				GWT.log("platformType=" + data.getMetadata().getPlatformType());
				GWT.log("sensor=" + data.getMetadata().getSensor());
				GWT.log("sensorType=" + data.getMetadata().getSensorType());
				GWT.log("trial=" + data.getMetadata().getTrial());
				GWT.log("tl=" + data.getMetadata().getTl());
				GWT.log("br=" + data.getMetadata().getBr());
				GWT.log("startTime=" + data.getMetadata().getStartDate());
				GWT.log("endTime=" + data.getMetadata().getEndDate());
				GWT.log("has lat=" + data.hasDatatype("lat"));
				GWT.log("has lon=" + data.hasDatatype("lon"));
				GWT.log("has z=" + data.hasDatatype("z"));
				GWT.log("has time=" + data.hasDatatype("time"));
				GWT.log("time=" + data.getDataArray("time"));
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		};
		connector.execute(new SimpleQueryBuilder()
			.documentId("1d3b0d1737024687089ac307150bbfc8")			
		, callback);
	}	
}
