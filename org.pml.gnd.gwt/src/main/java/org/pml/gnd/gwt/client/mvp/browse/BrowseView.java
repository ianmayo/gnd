package org.pml.gnd.gwt.client.mvp.browse;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * View interface. Extends IsWidget so a view impl can easily provide
 * its container widget.
 *
 * @author drfibonacci
 */
public interface BrowseView extends IsWidget
{
	void setPresenter(Presenter listener);
	
	void setCentre(IsWidget panel);
	
	SearchView getSearchView();

	public interface Presenter
	{
		/** run the specified search
		 * 
		 * @param text
		 */
		void search(SearchView selections);

		/** open the operate page, the user wants to do something with the basket
		 * 
		 */
		void operateBasket();

		/** reset the search criteria
		 * 
		 * @param _searchPanel
		 */
		void reset();
	}


}