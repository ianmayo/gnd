package org.pml.gnd.gwt.client.mvp.operate;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class OperateViewImpl extends Composite implements OperateView
{

	public OperateViewImpl()
	{
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		initWidget(horizontalPanel);
		
		VerticalPanel verticalPanel = new VerticalPanel();
		horizontalPanel.add(verticalPanel);
		
		Label lblWelcoem = new Label("DO THE OPERATION IN HERE");
		verticalPanel.add(lblWelcoem);
		
	}

	@Override
	public void setPresenter(Presenter listener)
	{
	}

}
