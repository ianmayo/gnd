package org.pml.gnd.gwt.client.mvp.browse.info;

import java.util.Arrays;
import java.util.List;

import org.pml.gnd.gwt.client.to.Dataset;
import org.pml.gnd.gwt.client.to.HistoryItem;

import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;

public class InfoInfoPanel extends Composite
{

	private Label platformLabel;
	private Label platformTypeLabel;
	private Label idLabel;

	public InfoInfoPanel()
	{
		
		FlowPanel flowPanel = new FlowPanel();
		initWidget(flowPanel);
		
		Label lblNewLabel = new Label("Id:");
		flowPanel.add(lblNewLabel);
		lblNewLabel.setWidth("50");
		
		idLabel = new Label("[Pending]");
		flowPanel.add(idLabel);
		idLabel.setWidth("200");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		flowPanel.add(horizontalPanel);
		
		CaptionPanel cptnpnlMetdata = new CaptionPanel("Metadata");
		horizontalPanel.add(cptnpnlMetdata);
		
		FlowPanel flowPanel_1 = new FlowPanel();
		cptnpnlMetdata.setContentWidget(flowPanel_1);
		flowPanel_1.setSize("5cm", "3cm");
		
		Label lblNewLabel_2 = new Label("Platform:");
		flowPanel_1.add(lblNewLabel_2);
		
		 platformLabel = new Label("[pending]");
		flowPanel_1.add(platformLabel);
		
		Label lblNewLabel_3 = new Label("Platform Type:");
		flowPanel_1.add(lblNewLabel_3);
		
		 platformTypeLabel = new Label("[pending]");
		flowPanel_1.add(platformTypeLabel);
		
		CaptionPanel cptnpnlHistory = new CaptionPanel("History");
		horizontalPanel.add(cptnpnlHistory);
		
		CellTable<HistoryItem> cellTable = new CellTable<HistoryItem>();
		cptnpnlHistory.setContentWidget(cellTable);
		cellTable.setSize("5cm", "3cm");
		
		cellTable.addColumn(new TextColumn<HistoryItem>(){
			@Override
			public String getValue(HistoryItem object)
			{
				return object.getDate().toString();
			}}, "Date");
		cellTable.addColumn(new TextColumn<HistoryItem>(){
			@Override
			public String getValue(HistoryItem object)
			{
				return object.getReason();
			}}, "Reason");
		
		
		// sort out some histrory
	  final List<HistoryItem> history = Arrays.asList(
	      new HistoryItem(),
	      new HistoryItem(),
	      new HistoryItem());	
	  
	  cellTable.setRowData(history);
	}

	public void setDataset(Dataset dataset)
	{
		platformLabel.setText(dataset.getMetadata().getPlatform());
		platformTypeLabel.setText(dataset.getMetadata().getPlatformType());
		idLabel.setText(dataset.getId());
	}

	
}
