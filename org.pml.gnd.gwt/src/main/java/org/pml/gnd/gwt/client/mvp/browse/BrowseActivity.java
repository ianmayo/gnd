package org.pml.gnd.gwt.client.mvp.browse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.pml.gnd.gwt.client.mvp.ClientFactory;
import org.pml.gnd.gwt.client.mvp.CoreActivity;
import org.pml.gnd.gwt.client.mvp.browse.info.InfoModel;
import org.pml.gnd.gwt.client.mvp.browse.info.InfoPresenter;
import org.pml.gnd.gwt.client.mvp.browse.info.InfoView;
import org.pml.gnd.gwt.client.mvp.browse.results.ResultsPresenter;
import org.pml.gnd.gwt.client.mvp.browse.results.ResultsView;
import org.pml.gnd.gwt.client.mvp.place.BrowsePlace;
import org.pml.gnd.gwt.client.mvp.place.OperatePlace;
import org.pml.gnd.gwt.client.to.ElasticFacet;
import org.pml.gnd.gwt.client.to.ElasticResults;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;

public class BrowseActivity extends CoreActivity implements
		BrowseView.Presenter, ResultsView.Presenter
{
	private BrowsePlace _myPlace;

	// Name that will be appended to "Hello,"

	public BrowseActivity(BrowsePlace place, ClientFactory clientFactory)
	{
		super(clientFactory);
		_myPlace = place;
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus)
	{
		final BrowseView browseView = getFactory().getBrowseView();
		browseView.setPresenter(this);
		containerWidget.setWidget(browseView.asWidget());

		// ok, put in the message
		final IsWidget theView;
		if (_myPlace.getId() != null)
		{
			InfoView iView = getFactory().getInfoView();
			InfoModel iModel = getFactory().getInfoModel();
			InfoPresenter presenter = new InfoPresenter(iView, iModel);
			presenter.setDataset(_myPlace.getId());
			iView.showFirstTab();

			theView = iView;
		}
		else
		{
			final ResultsView iView = getFactory().getFilterView();
			iView.setPresenter(this);
			SearchModel iModel = getFactory().getSearchModel();

			ResultsPresenter presenter = new ResultsPresenter(iView);
			presenter.setResults(null);

			// we'll run the query, since we want to use any facets
			// in the filter pane

			if (_myPlace.getFilter() != null)
			{
				AsyncCallback<ElasticResults> handler = new SearchHandler(true, iView, browseView);
				iModel.getMatches(_myPlace.getFilter(), handler);
			}
			else
			{
				AsyncCallback<ElasticResults> handler = new SearchHandler(false, iView, browseView);
				browseView.getSearchView().getFreeText().setValue("");
				browseView.getSearchView().getPlatforms().clearSelections();
				browseView.getSearchView().getPlatformTypes().clearSelections();
				browseView.getSearchView().getTrials().clearSelections();
				iModel.getAll(handler);
			}

			theView = iView;
		}

		browseView.setCentre(theView);

	}
	
	private static class SearchHandler implements AsyncCallback<ElasticResults>
	{
		private boolean _keepSelections;
		private ResultsView _results;
		private BrowseView _browse;

		private SearchHandler(boolean keepSelections, ResultsView results, BrowseView browse )
		{
			_keepSelections = keepSelections;
			_results = results;
			_browse = browse;
		}

		@Override
		public void onSuccess(ElasticResults result)
		{
			// ok, put the results in to the view
			_results.setResults(result);

			// we also wish to put the facets in the search window
			_browse.getSearchView().getPlatforms()
					.populateList(parseAndAnnotateFacet(result.getFacet("platform")), _keepSelections);
			_browse.getSearchView().getPlatformTypes()
					.populateList(parseAndAnnotateFacet(result.getFacet("platform_type")), _keepSelections);
			_browse.getSearchView().getTrials()
					.populateList(parseAndAnnotateFacet(result.getFacet("trial")), _keepSelections);
			_browse.getSearchView().getSensors()
			.populateList(parseAndAnnotateFacet(result.getFacet("sensor")), _keepSelections);
			_browse.getSearchView().getSensorTypes()
			.populateList(parseAndAnnotateFacet(result.getFacet("sensor_type")), _keepSelections);
		}

		@Override
		public void onFailure(Throwable caught)
		{
			caught.printStackTrace();
		}
	};

	private static List<String> parseAndAnnotateFacet(ElasticFacet facet)
	{
		List<String> pList = new ArrayList<String>();

		int numP = facet.getNumTerms();
		for (int i = 0; i < numP; i++)
		{
			pList.add(facet.getTermName(i) + " (" + facet.getTermCount(i) + ")");
		}
		Collections.sort(pList);
		return pList;
	}

	@Override
	public void search(SearchView search)
	{
		JSONArray facets = new JSONArray();
		// ok, get collating the items.
		addThis(facets, search.getPlatforms().getSelections(), "platform");
		addThis(facets, search.getPlatformTypes().getSelections(), "platform_type");
		addThis(facets, search.getTrials().getSelections(), "trial");
		addThis(facets, search.getSensors().getSelections(), "sensor");
		addThis(facets, search.getSensorTypes().getSelections(), "sensor_type");

		// we also have to do the free search
		String freeText = search.getFreeText().getValue();
		if (freeText.length() > 0)
		{
			JSONObject field = new JSONObject();
			JSONObject term = new JSONObject();
			term.put("query", new JSONString(freeText));
			field.put("query_string", term);
			facets.set(facets.size(), field);
		}

		JSONObject mustHolder = new JSONObject();
		mustHolder.put("must", facets);
		JSONObject boolHolder = new JSONObject();
		boolHolder.put("bool", mustHolder);

		super.goTo(new BrowsePlace("{" + boolHolder.toString() + "}"));
	}

	private void addThis(JSONArray facets, List<String> items, String tag_name)
	{
		if (items.size() > 0)
		{
			JSONObject holder = new JSONObject();
			JSONObject terms = new JSONObject();
			JSONArray matches = new JSONArray();
			for (Iterator<String> iterator = items.iterator(); iterator.hasNext();)
			{
				String thisItem = (String) iterator.next();

				// and store it
				matches.set(matches.size(), new JSONString(thisItem));
			}
			terms.put(tag_name, matches);
			terms.put("minimum_match", new JSONNumber(1));
			holder.put("terms", terms);
			facets.set(facets.size(), holder);
		}
	}

	@Override
	public void viewIt(String id)
	{
		super.goTo(new BrowsePlace(id));
	}

	@Override
	public void operateBasket()
	{
		super.goTo(new OperatePlace());
	}

	@Override
	public void reset()
	{
		super.goTo(new BrowsePlace());
	}

}
