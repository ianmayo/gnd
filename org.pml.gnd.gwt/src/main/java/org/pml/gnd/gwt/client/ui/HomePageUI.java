package org.pml.gnd.gwt.client.ui;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class HomePageUI extends Composite
{

	private SimplePanel _simplePanel;
	private ClickHandler _homeListener;

	public HomePageUI()
	{

		DockPanel dockLayoutPanel = new DockPanel();
		initWidget(dockLayoutPanel);

		HorizontalPanel horizontalPanel = new HorizontalPanel();
		dockLayoutPanel.add(horizontalPanel, DockPanel.NORTH);

		Button btnNewButton = new Button("Home");
		horizontalPanel.add(btnNewButton);
		btnNewButton.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event)
			{
				if(_homeListener != null)
					_homeListener.onClick(event);
			}});

		Label lblNewLabel = new Label("GeoSpatial Network Database");
		horizontalPanel.add(lblNewLabel);

		_simplePanel = new SimplePanel()
		{

			@Override
			public void setWidget(IsWidget w)
			{
				// TODO Auto-generated method stub
				super.setWidget(w);
			}

			@Override
			public void setWidget(Widget w)
			{
				// TODO Auto-generated method stub
				super.setWidget(w);
			}

		};
		dockLayoutPanel.add(_simplePanel, DockPanel.CENTER);

	}

	public AcceptsOneWidget getCentre()
	{
		return _simplePanel;
	}

	public void addHomeHandler(ClickHandler clickHandler)
	{
		_homeListener = clickHandler;
	}

}
