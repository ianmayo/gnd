package org.pml.gnd.gwt.client.mvp;

import com.google.gwt.user.client.rpc.AsyncCallback;

public abstract class CoreCallback<T> implements AsyncCallback<T>
{

	@Override
	public void onFailure(Throwable caught)
	{
		System.err.println("failed:" + caught);
	}

	@Override
	public void onSuccess(T result)
	{
		done(result);
	}
	
	abstract public void done(T result);

}
