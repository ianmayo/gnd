package org.pml.gnd.gwt.client.dao.builders;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unchecked")
public abstract class QueryBuilder<T extends QueryBuilder<?>> {
	
	protected String _designDocId;
	protected Map<String, Object> urlParameters;

	public String getDesignDocId() {
		return _designDocId;
	}

	public T designDocId(String designDocId) {
		this._designDocId = designDocId;
		return (T) this;
	}

	public <F> F getUlrParameter(String parameter) {
		return (F) urlParameters.get(parameter);
	}
	
	public <F> T setUrlParameter(String parameter, F value) {
		if (urlParameters == null) {
			urlParameters = new HashMap<String, Object>();
		}
		urlParameters.put(parameter, value);
		return (T) this;
	}
	
	public abstract String generateUrl();		
}
