package org.pml.gnd.gwt.client.dao.transport;

import org.pml.gnd.gwt.client.util.URLUtils;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;

public class CouchDBLocalTransport implements Transport {
	
	private String couchDbUrl;

	public CouchDBLocalTransport(String couchDbUrl) {
		assert couchDbUrl != null : "couchdb url must be specified";
		this.couchDbUrl = URL.encode(URLUtils.ensureSlash(couchDbUrl));
	}
	
	private void doRequest(RequestBuilder builder, String body, final AsyncCallback<JSONValue> callback) {
		try {
			builder.sendRequest(body, new RequestCallback() {
				public void onError(Request request, Throwable exception) {
					callback.onFailure(exception);
				}

				public void onResponseReceived(Request request,	Response response) {
					if (response.getStatusCode() <= 300) {
						JSONValue value = JSONParser.parseStrict(response.getText());
						callback.onSuccess(value);
					} else {
						callback.onFailure(new StatusCodeException(response.getStatusCode(), response.getText()));
					}
				}
			});
		} catch (RequestException e) {
			callback.onFailure(e);
		}		
	}
	
	@Override
	public void get(String relativeUrl, AsyncCallback<JSONValue> callback) {
		assert callback != null : "response must be specified";
		assert relativeUrl != null : "request url must be specified";

		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET,
				URLUtils.joinUrls(false, couchDbUrl, relativeUrl));
		doRequest(builder, null, callback);
	}

	@Override
	public void post(String relativeUrl, String responseBody, AsyncCallback<JSONValue> callback) {
		assert callback == null : "response must be specified";
		assert relativeUrl == null : "request url must be specified";

		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST,
				URLUtils.joinUrls(false, couchDbUrl, relativeUrl));
		doRequest(builder, responseBody, callback);
	}

	@Override
	public void post(String relativeUrl, AsyncCallback<JSONValue> callback) {
		post(relativeUrl, callback);
	}

	@Override
	public boolean isPostSupported() {
		return true;
	}
}
