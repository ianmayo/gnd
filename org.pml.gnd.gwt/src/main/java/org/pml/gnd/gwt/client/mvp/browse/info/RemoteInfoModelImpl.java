package org.pml.gnd.gwt.client.mvp.browse.info;

import org.pml.gnd.gwt.client.to.Dataset;

import com.google.gwt.jsonp.client.JsonpRequestBuilder;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class RemoteInfoModelImpl implements InfoModel
{

	@Override
	public void getDataset(String id, AsyncCallback<Dataset> callback)
	{
		String url = "http://localhost:5984/tracks/" + id;
		// Send request to server and catch any errors.
		JsonpRequestBuilder builder = new JsonpRequestBuilder();
		builder.requestObject(url, callback);
	}

}
