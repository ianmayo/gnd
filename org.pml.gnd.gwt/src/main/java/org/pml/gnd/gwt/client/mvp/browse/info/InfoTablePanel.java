package org.pml.gnd.gwt.client.mvp.browse.info;

import org.pml.gnd.gwt.client.to.Dataset;

import com.google.gwt.core.client.JsArrayMixed;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;

public class InfoTablePanel extends Composite
{
	private FlexTable _cellTable;
	private Dataset _myDataset;

	public InfoTablePanel()
	{
		FlowPanel flowPanel = new FlowPanel();
		initWidget(flowPanel);

		_cellTable = new FlexTable();
		flowPanel.add(_cellTable);

	}

	public void setDataset(Dataset dataset)
	{
		_myDataset = dataset;
	}

	public void load()
	{

		// ditch any existing columns
		_cellTable.removeAllRows();

		// ok, look at the columns
		JsArrayString cols = _myDataset.getMetadata().getDataTypes();

		for (int i = 0; i < cols.length(); i++)
		{
			String thisCol = cols.get(i);

			// stick in the header
			_cellTable.setText(0, i, thisCol);

			// sort out this datatype
			JsArrayMixed data = _myDataset.getDataArray(thisCol);
			for (int j = 0; j < data.length(); j++)
			{
				_cellTable.setText(j + 1, i, data.getString(j));
			}
		}
	}

}
