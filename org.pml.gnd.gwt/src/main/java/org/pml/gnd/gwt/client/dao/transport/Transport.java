package org.pml.gnd.gwt.client.dao.transport;

import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface Transport {
	
	void get(String relativeUrl, AsyncCallback<JSONValue> callback);
	
	void post(String relativeUrl, String responseBody, AsyncCallback<JSONValue> callback);
	
	void post(String relativeUrl, AsyncCallback<JSONValue> callback);
	
	boolean isPostSupported();
}
