package org.pml.gnd.gwt.client.to;

import java.util.Date;

public class HistoryItem
{
	private final Date _date;
	private final String _reason;
	private final String[] _contributors;

	private final String[] _singleOperations = new String[]
	{ "Smoothed", "Refined", "Relaxed", "Improved" };
	private final String[] _multipleOperations = new String[]
	{ "Updated from", "Merged with", "Replaced with" };

	public HistoryItem()
	{
		long dateOffset = (long) (Math.random() * 1000 * 60 * 60 * 24 * 60);
		_date = new Date(new Date().getTime() - dateOffset);

		if (Math.random() > 0.5)
		{
			_contributors = null;
			_reason = _singleOperations[(int) (Math.random() * _singleOperations.length)];
		}
		else
		{
			int numC = (int) (Math.random() * 5);
			_contributors = new String[numC];
			_reason = _multipleOperations[(int) (Math.random() * _multipleOperations.length)];
		}
	}

	public Date getDate()
	{
		return _date;
	}

	public String getReason()
	{
		String res = _reason;
		if (_contributors != null)
		{
			res += " " + _contributors.length + " other datasets";
		}
		return res;
	}
}
