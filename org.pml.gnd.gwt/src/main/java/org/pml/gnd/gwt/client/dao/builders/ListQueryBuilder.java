package org.pml.gnd.gwt.client.dao.builders;

import java.util.Arrays;

import org.pml.gnd.gwt.client.util.URLUtils;

import com.google.gwt.http.client.URL;

public class ListQueryBuilder extends QueryBuilder<ListQueryBuilder> {
	
	protected String _viewName;
	protected String _listFunction;
	
	protected String[] _keys;
	
	public String getViewName() {
		return _viewName;
	}
	
	public ListQueryBuilder viewName(String _viewName) {
		this._viewName = _viewName;
		return this;
	}
	
	public String getKey() {
		return getUlrParameter("key");
	}
	
	public ListQueryBuilder key(String _key) {		
		return setUrlParameter("key", _key);
	}
	
	public String[] getKeys() {
		return _keys;
	}
	
	public ListQueryBuilder keys(String... _keys) {
		this._keys = _keys;
		return this;
	}
	
	public String getStartKey() {
		return getUlrParameter("startkey");
	}
	
	public ListQueryBuilder startKey(String _startKey) {
		return setUrlParameter("startkey", _startKey);
	}
	
	public String getEndKey() {
		return getUlrParameter("endkey");
	}
	
	public ListQueryBuilder endKey(String _endKey) {
		return setUrlParameter("endkey", _endKey);
	}
	
	public String getStartDocId() {
		return getUlrParameter("startkey_docid");
	}
	
	public ListQueryBuilder startDocId(String _startDocId) {
		return setUrlParameter("startkey_docid", _startDocId);
	}
	
	public String getEndDocId() {
		return getUlrParameter("endkey_docid");
	}
	
	public ListQueryBuilder endDocId(String _endDocId) {
		return setUrlParameter("endkey_docid", _endDocId);
	}
	
	public int getLimit() {
		return getUlrParameter("limit");
	}
	
	public ListQueryBuilder limit(int _limit) {
		return setUrlParameter("limit", _limit);
	}
	
	public int getSkip() {
		return getUlrParameter("skip");
	}
	
	public ListQueryBuilder skip(int _skip) {
		return setUrlParameter("skip", _skip);
	}
	
	public boolean isDescending() {
		return getUlrParameter("descending");
	}
	
	public ListQueryBuilder descending(boolean _desceding) {
		return setUrlParameter("descending", _desceding);
	}
	
	public String getListFunction() {
		return _listFunction;
	}

	public void set_listFunction(String _listFunction) {
		this._listFunction = _listFunction;
	}

	@Override
	public String generateUrl() {
		assert _designDocId != null : "design doc must be specified";
		assert _viewName != null : "view name must be specified";
		String url;
		if (_listFunction != null) {
			url = URLUtils.joinUrls(_designDocId, "_list", _listFunction, _viewName);
		} else {
			url = URLUtils.joinUrls(_designDocId, "_view", _viewName);
		}
		String queryString = URLUtils.generateQueryString(urlParameters);
		String keysParameter = "";
		if (_keys != null) {
			keysParameter = Arrays.toString(_keys);
			keysParameter = "keys=" + URL.encodeQueryString(keysParameter);
			if (queryString.isEmpty()) {
				queryString = keysParameter;
			} else {
				queryString += "&" + keysParameter;
			}
			/*StringBuilder builder = new StringBuilder("[");
			boolean first = true;
			for (String key : _keys) {
				if (! first) {
					keysParameter.append(", ");
				}
				keysParameter.append(key);
				first = false;
			}*/
			
		}
		return URLUtils.joinQueryString(url, queryString);
	}
	
	
}
