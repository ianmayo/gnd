package org.pml.gnd.gwt.client.mvp.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

//public class HelloPlace extends ActivityPlace<HelloActivity>
public class OperatePlace extends Place
{
	public OperatePlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<OperatePlace>
	{

		@Override
		public String getToken(OperatePlace place)
		{
			return null;
		}

		@Override
		public OperatePlace getPlace(String token)
		{
			return new OperatePlace();
		}

	}
}
