package org.pml.gnd.gwt.client.mvp.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

//public class HelloPlace extends ActivityPlace<HelloActivity>
public class AddPlace extends Place
{
	public AddPlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<AddPlace>
	{
		final static String NAME="Add";

		@Override
		public String getToken(AddPlace place)
		{
			return NAME;
		}

		@Override
		public AddPlace getPlace(String token)
		{
			return new AddPlace();
		}

	}
}
