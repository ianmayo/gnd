package org.pml.gnd.gwt.client.mvp.browse.info;

import org.pml.gnd.gwt.client.to.Dataset;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * View interface. Extends IsWidget so a view impl can easily provide
 * its container widget.
 *
 * @author drfibonacci
 */
public interface InfoView extends IsWidget
{
	void setPresenter(Presenter listener);
	void setDataset(Dataset result);
	void showFirstTab();
	
	public interface Presenter
	{
	}



}