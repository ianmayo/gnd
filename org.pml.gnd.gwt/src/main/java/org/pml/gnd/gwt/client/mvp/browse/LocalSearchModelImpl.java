package org.pml.gnd.gwt.client.mvp.browse;

import org.pml.gnd.gwt.client.to.ElasticResults;

import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class LocalSearchModelImpl implements SearchModel
{
	@Override
	public void getMatches(String query,
			final AsyncCallback<ElasticResults> callback)
	{
		RequestCallback rc = new RequestCallback()
		{

			@Override
			public void onResponseReceived(Request request, Response response)
			{
				// ok, now we have to process them...
				final ElasticResults result = JsonUtils.unsafeEval(response.getText());

				callback.onSuccess(result);
			}

			@Override
			public void onError(Request request, Throwable exception)
			{
				callback.onFailure(exception);
			}
		};

		String url = "org.pml.gnd.gwt.Gnd/test_data/search_res1.js";
		// Send request to server and catch any errors.
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, url);

		try
		{
			builder.sendRequest(null, rc);
		}
		catch (RequestException e)
		{
			System.err.println("ERROR:" + e.getMessage());
		}

	}

	@Override
	public void getAll(AsyncCallback<ElasticResults> callback)
	{
		getMatches("plat_a", callback);
	}

}
