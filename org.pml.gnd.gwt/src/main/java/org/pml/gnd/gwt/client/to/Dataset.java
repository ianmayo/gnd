package org.pml.gnd.gwt.client.to;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayMixed;

/**
 * Dataset TO object
 * 
 * {
 *   "lat"      : [50.1, 50.2, 50.2],
 *   "lon"      : [-2.3, -2.4, -2.1],
 *   "time"     : [ "2012-02-27T14:46:02+0000","2012-02-27T14:50:10+0000","2012-02-27T14:55:00+0000"],
 *   "metadata" :
 *    {
 *      "data_type"     : ["lat", "lon", "time"],
 *      "platform"      : "vehicle_1232",
 *      "platform_type" : "bike",
 *      "trial"         : "2012/Feb/1433",
 *      "sensor"        : "GARMIN-GO300",
 *      "sensor_type"   : "GPS",
 *      "geo_bounds"    : { "tl":[50.3, -2.4],"br":[50.1, -2.1] },
 *      "time_bounds"   : [ "2012-02-27T14:46:02+0000", "2012-02-27T14:55:00+0000"]
 *    }
 * }
 * @author Yuri
 *
 */
public class Dataset extends JavaScriptObject {

	protected Dataset() {
	}
	
	
	public final native String getId() /*-{
		return this._id;
	}-*/;
	
	
	public final native DatasetMetadata getMetadata() /*-{
		return this.metadata;
	}-*/;
	
	private final native JsArrayMixed getDataArrayNative(String name) /*-{
		if (this[name] != null) {
			return this[name];
		}
	}-*/;
	
	public final boolean hasDatatype(String datatype) {
		if (getMetadata() != null) {
			return getMetadata().hasDatatype(datatype);
		}
		return false;
	}
	
	public final JsArrayMixed getDataArray(String name) {
		if (hasDatatype(name)) {
			return getDataArrayNative(name);
		}
		return null;
	}
}
