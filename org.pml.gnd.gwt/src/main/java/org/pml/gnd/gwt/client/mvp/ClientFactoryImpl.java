package org.pml.gnd.gwt.client.mvp;

import org.pml.gnd.gwt.client.mvp.add.AddView;
import org.pml.gnd.gwt.client.mvp.add.AddViewImpl;
import org.pml.gnd.gwt.client.mvp.browse.BrowseView;
import org.pml.gnd.gwt.client.mvp.browse.BrowseViewImpl;
import org.pml.gnd.gwt.client.mvp.browse.SearchModel;
import org.pml.gnd.gwt.client.mvp.browse.LocalSearchModelImpl;
import org.pml.gnd.gwt.client.mvp.browse.RemoteSearchModelImpl;
import org.pml.gnd.gwt.client.mvp.browse.info.InfoModel;
import org.pml.gnd.gwt.client.mvp.browse.info.LocalInfoModelImpl;
import org.pml.gnd.gwt.client.mvp.browse.info.InfoView;
import org.pml.gnd.gwt.client.mvp.browse.info.InfoViewImpl;
import org.pml.gnd.gwt.client.mvp.browse.info.RemoteInfoModelImpl;
import org.pml.gnd.gwt.client.mvp.browse.results.ResultsView;
import org.pml.gnd.gwt.client.mvp.browse.results.ResultsViewImpl;
import org.pml.gnd.gwt.client.mvp.maintain.MaintainView;
import org.pml.gnd.gwt.client.mvp.maintain.MaintainViewImpl;
import org.pml.gnd.gwt.client.mvp.operate.OperateView;
import org.pml.gnd.gwt.client.mvp.operate.OperateViewImpl;
import org.pml.gnd.gwt.client.mvp.welcome.LocalWelcomeModelImpl;
import org.pml.gnd.gwt.client.mvp.welcome.RemoteWelcomeModelImpl;
import org.pml.gnd.gwt.client.mvp.welcome.WelcomeModel;
import org.pml.gnd.gwt.client.mvp.welcome.WelcomeView;
import org.pml.gnd.gwt.client.mvp.welcome.WelcomeViewImpl;

import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

public class ClientFactoryImpl implements ClientFactory
{
	private final boolean _useRemote;

	private static final EventBus eventBus = new SimpleEventBus();
	private static final PlaceController placeController = new PlaceController(
			eventBus);
	private static final WelcomeView helloView = new WelcomeViewImpl();
	private BrowseViewImpl _browseView;
	private AddViewImpl _addView;
	private MaintainViewImpl _maintainView;
	private InfoView _infoView;
	private ResultsView _filterView;
	private SearchModel _filterModel;
	private OperateView _operateView;
	private InfoModel _infoModel;
	private WelcomeModel _welcomeModel;

	public ClientFactoryImpl(boolean useRemote)
	{
		_useRemote = useRemote;
	}

	@Override
	public EventBus getEventBus()
	{
		return eventBus;
	}

	@Override
	public WelcomeView getWelcomeView()
	{
		return helloView;
	}

	@Override
	public PlaceController getPlaceController()
	{
		return placeController;
	}

	@Override
	public BrowseView getBrowseView()
	{
		if (_browseView == null)
			_browseView = new BrowseViewImpl();
		return _browseView;
	}

	@Override
	public AddView getAddView()
	{
		if (_addView == null)
			_addView = new AddViewImpl();
		return _addView;
	}

	@Override
	public MaintainView getMaintainView()
	{
		if (_maintainView == null)
			_maintainView = new MaintainViewImpl();
		return _maintainView;
	}

	@Override
	public InfoView getInfoView()
	{
		if (_infoView == null)
			_infoView = new InfoViewImpl();
		return _infoView;
	}

	@Override
	public ResultsView getFilterView()
	{
		if (_filterView == null)
			_filterView = new ResultsViewImpl();
		return _filterView;
	}

	@Override
	public SearchModel getSearchModel()
	{
		if (_filterModel == null)
			if (_useRemote)
				_filterModel = new RemoteSearchModelImpl();
			else
				_filterModel = new LocalSearchModelImpl();
		return _filterModel;
	}

	@Override
	public InfoModel getInfoModel()
	{
		if (_infoModel == null)
			if (_useRemote)
				_infoModel = new RemoteInfoModelImpl();
			else
				_infoModel = new LocalInfoModelImpl();
		return _infoModel;
	}

	@Override
	public OperateView getOperateView()
	{
		if (_operateView == null)
			_operateView = new OperateViewImpl();
		return _operateView;
	}

	@Override
	public WelcomeModel getWelcomeModel()
	{
		if (_welcomeModel == null)
			if (_useRemote)
				_welcomeModel = new RemoteWelcomeModelImpl();
			else
				_welcomeModel = new LocalWelcomeModelImpl();
		return _welcomeModel;
	}

}
