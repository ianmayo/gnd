package org.pml.gnd.gwt.client.mvp.maintain;

import org.pml.gnd.gwt.client.mvp.ClientFactory;
import org.pml.gnd.gwt.client.mvp.CoreActivity;
import org.pml.gnd.gwt.client.mvp.place.MaintainPlace;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

public class MaintainActivity extends CoreActivity implements
		MaintainView.Presenter {
	// Name that will be appended to "Hello,"

	public MaintainActivity(MaintainPlace place, ClientFactory clientFactory) {
		super(clientFactory);
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		MaintainView maintainView = getFactory().getMaintainView();
		maintainView.setPresenter(this);
		containerWidget.setWidget(maintainView.asWidget());
	}

	@Override
	public void home()
	{
		// TODO Auto-generated method stub
	}



}
