package org.pml.gnd.gwt.client.mvp.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

//public class HelloPlace extends ActivityPlace<HelloActivity>
public class BrowsePlace extends Place
{
	private final String _myId;
	private final String _myFilter;

	public BrowsePlace()
	{
		this(null);
	}

	public String getId()
	{
		return _myId;
	}

	public String getFilter()
	{
		return _myFilter;
	}

	public BrowsePlace(String datasetId)
	{
		// do quick check, is this a dataset?
		if (datasetId == null)
		{
			_myFilter = null;
			_myId = null;
		}
		else if (datasetId.contains("{"))
		{
			_myFilter = datasetId;
			_myId = null;
		}
		else
		{
			_myId = datasetId;
			_myFilter = null;
		}
	}

	public static class Tokenizer implements PlaceTokenizer<BrowsePlace>
	{

		@Override
		public String getToken(BrowsePlace place)
		{
			final String res;
			if (place.getId() != null)
				res = "I=" + place.getId();
			else if (place.getFilter() != null)
				res = "F=" + place.getFilter();
			else
				res = null;

			return res;
		}

		@Override
		public BrowsePlace getPlace(String token)
		{
			final BrowsePlace res;
			// see what's in the token
			if (token.contains("="))
			{
				// see what kind it is
				String [] parts = token.split("=");
				String typeStr = parts[0];
				String dataStr = parts[1];
				final String marker;
				if (typeStr.equals("I"))
					marker = dataStr;
				else if (typeStr.equals("F"))
					marker = dataStr;
				else
					marker = null;

				res = new BrowsePlace(marker);
			}
			else
				res = new BrowsePlace();
			return res;
		}

	}
}
