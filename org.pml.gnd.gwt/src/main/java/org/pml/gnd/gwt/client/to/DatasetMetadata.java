package org.pml.gnd.gwt.client.to;

import java.util.Date;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.i18n.client.DateTimeFormat;

/**
 * Dataset metadata:
 *  
 *  {
 *     "data_type"     : ["lat", "lon", "time"],
 *     "platform"      : "vehicle_1232",
 *     "platform_type" : "bike",     
 *     "trial"         : "2012/Feb/1433",
 *     "sensor"        : "GARMIN-GO300",
 *     "sensor_type"   : "GPS",
 *     "geo_bounds"    : { "tl":[50.3, -2.4],"br":[50.1, -2.1] },
 *     "time_bounds"   : [ "2012-02-27T14:46:02+0000", "2012-02-27T14:55:00+0000"]
 *   }
 *
 * @author Yuri M.
 *
 */
public class DatasetMetadata extends JavaScriptObject {
	private static final DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("yyyy-MM-dd'T'HH:mm:ssZ");	
	
	protected DatasetMetadata() {
	}
	

	public final native String getName() /*-{
		return this.name;
	}-*/;

	public final native String getPlatform() /*-{
		return this.platform;
	}-*/;
	
	public final native String getPlatformType() /*-{
		return this.platform_type;
	}-*/;	

	public final native String getTrial() /*-{
		return this.trial;
	}-*/;

	public final native String getSensor() /*-{
		return this.sensor;
	}-*/;

	public final native String getSensorType() /*-{
		return this.sensor_type;
	}-*/;
	
	private final native GeoPoint getGeoPoint(String name) /*-{
		if (this.geo_bounds != null && this.geo_bounds[name] != null) {
			var lat = this.geo_bounds[name][0] == null ? 0 : this.geo_bounds[name][0];
			var lon = this.geo_bounds[name][1] == null ? 0 : this.geo_bounds[name][1];
			return @org.pml.gnd.gwt.client.to.GeoPoint::new(DD)(lat, lon)
		}
		return null;
	}-*/;

	public final GeoPoint getTl() {
		return getGeoPoint("tl");	
	}
	
	public final GeoPoint getBr() {
		return getGeoPoint("br");	
	}	
	
	private final native Date getDate(int i) /*-{
		if (this.time_bounds != null && this.time_bounds[i] != null) {
			var formatter = @org.pml.gnd.gwt.client.to.DatasetMetadata::dateTimeFormat;
			return formatter.@com.google.gwt.i18n.client.DateTimeFormat::parse(Ljava/lang/String;)(this.time_bounds[i]);
		}
		return null;
	}-*/;
	
	public final native Date getStartDate() /*-{
		if (this.time_bounds != null && this.time_bounds.start != null) {
			var formatter = @org.pml.gnd.gwt.client.to.DatasetMetadata::dateTimeFormat;
			return formatter.@com.google.gwt.i18n.client.DateTimeFormat::parse(Ljava/lang/String;)(this.time_bounds.start);
		}
		return null;
	}-*/;
	
	
	public final native Date getEndDate()/*-{
		if (this.time_bounds != null && this.time_bounds.end != null) {
			var formatter = @org.pml.gnd.gwt.client.to.DatasetMetadata::dateTimeFormat;
			return formatter.@com.google.gwt.i18n.client.DateTimeFormat::parse(Ljava/lang/String;)(this.time_bounds.end);
		}
		return null;
	}-*/;
	
	public final native JsArrayString getDataTypes() /*-{
		return this.data_type;
	}-*/; 
	
	public final boolean hasDatatype(String datatype) {
		JsArrayString array = getDataTypes();
		int size = array.length();
		for (int i = 0; i < size; i++) {
			if (datatype.equals(array.get(i))) {
				return true;
			}
		}
		return false;
	}	
}
