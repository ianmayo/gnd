package org.pml.gnd.gwt.client.mvp.add;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class AddViewImpl extends Composite implements AddView
{

	public AddViewImpl()
	{
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		initWidget(horizontalPanel);
		
		VerticalPanel verticalPanel = new VerticalPanel();
		horizontalPanel.add(verticalPanel);
		
		Label lblWelcoem = new Label("add stuff");
		verticalPanel.add(lblWelcoem);
		
	}

	@Override
	public void setPresenter(Presenter listener)
	{
	}

}
