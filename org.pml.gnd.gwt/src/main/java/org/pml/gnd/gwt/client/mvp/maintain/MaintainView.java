package org.pml.gnd.gwt.client.mvp.maintain;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * View interface. Extends IsWidget so a view impl can easily provide
 * its container widget.
 *
 * @author drfibonacci
 */
public interface MaintainView extends IsWidget
{
	void setPresenter(Presenter listener);

	public interface Presenter
	{
		void home();
	}
}