package org.pml.gnd.gwt.client.mvp.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

//public class HelloPlace extends ActivityPlace<HelloActivity>
public class MaintainPlace extends Place
{
	public MaintainPlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<MaintainPlace>
	{
		@Override
		public String getToken(MaintainPlace place)
		{
			return null;
		}

		@Override
		public MaintainPlace getPlace(String token)
		{
			return new MaintainPlace();
		}

	}
}
